#include <neural_network.h>

using namespace std;

#define ALPHA 0.5

Network::Network(int n_args, ...)
{
	va_list vl;

	va_start(vl, n_args);
	vector<double> layers;
	for (int i = 1; i <= n_args; ++i) {
		layers.push_back(va_arg(vl, int));
	}
	va_end(vl);

  data.size = layers.size();
  srand(time(NULL)); 

  double r;
  vector<Node*> aux;

  for (int i = 0; i < data.size -1; i++)
  {
    data.weights.push_back({});
    for (int j = 0; j < layers[i]; j++)
    {

      data.weights[i].push_back({});

      for (int k = 0; k < layers[i + 1]; k++)
      {
        r = rand() % 200 - 100;
				r = r / 100;
        data.weights[i][j].push_back(r);
      }
    }
  }

  vector<Node> nodeVector;
  for (int i = 0; i < data.size; i++)
  {
    nodeVector.clear();
    for (int j = 0; j < layers[i]; j++)
    {
      Node newNode;
      newNode.input = j;
      newNode.output = j;
      newNode.delta = j;
      nodeVector.push_back(move(newNode));
    }
    data.neural_network.push_back(nodeVector);
  }
}

Network::~Network() {};

void Network::Init(int n_args, ...)
{
	va_list vl;

	va_start(vl, n_args);
	vector<double> layers;
	for (int i = 1; i <= n_args; ++i) {
		layers.push_back(va_arg(vl, int));
	}
	va_end(vl);

	data.size = layers.size();

	data.size = layers.size();
	srand(time(NULL));

	double r;
	vector<Node*> aux;

	for (int i = 0; i < data.size - 1; i++)
	{
		data.weights.push_back({});
		for (int j = 0; j < layers[i]; j++)
		{

			data.weights[i].push_back({});

			for (int k = 0; k < layers[i + 1]; k++)
			{
				r = rand() % 200 - 100;
				r = r / 100;
				data.weights[i][j].push_back(r);
			}
		}
	}

	vector<Node> nodeVector;
	for (int i = 0; i < data.size; i++)
	{
		nodeVector.clear();
		for (int j = 0; j < layers[i]; j++)
		{
			Node newNode;
			newNode.input = j;
			newNode.output = j;
			newNode.delta = j;
			nodeVector.push_back(move(newNode));
		}
		data.neural_network.push_back(nodeVector);
	}
}

double Network::sigmoide(double x)
{

  return (1.0 / (1 + exp(-1 * x)));

}

double Network::sigmoide_p(double x)
{

  return (exp(x) / ((exp(x) + 1)*(exp(x) + 1)));

}

vector<double> Network::CData(int n_args, ...) {
	va_list vl;

	va_start(vl, n_args);
	vector<double> values;
	for (int i = 1; i <= n_args; ++i) {
		values.push_back(va_arg(vl, double));
	}
	va_end(vl);

	return values;
}

vector<double> Network::FeedForwardOutput(vector<double>& input)
{

  vector<double> outputs;
  for (unsigned int i = 0; i < data.neural_network[0].size(); i++)
  {
		data.neural_network[0][i].output = input[i];
  }

  for (unsigned int i = 1; i < data.neural_network.size(); i++)
  {
    for (unsigned int j = 0; j < data.neural_network[i].size(); j++)
    {
			data.neural_network[i][j].input = 0;
      for (unsigned int k = 0; k < data.neural_network[i - 1].size(); k++)
      {
				data.neural_network[i][j].input += data.weights[i - 1][k][j] * data.neural_network[i - 1][k].output;
      }
			data.neural_network[i][j].output = sigmoide(data.neural_network[i][j].input);
    }
  }

  for (unsigned int i = 0; i < data.neural_network[data.neural_network.size() - 1].size(); i++)
  {
		outputs.push_back(data.neural_network[data.neural_network.size() - 1][i].output);
  }
  return outputs;

}

void Network::BackPropagation(vector< vector < double > >& inputs, vector < vector < double > >& outputs)
{

  double suma = 0.0f;
  int iteration = 0;
  double ERROR = 200;
  double error = 0.0f;

  while (ERROR > data.offset_error && iteration < data.maxIterations)
  {
    ERROR = 0;
    for (unsigned int e = 0; e < inputs.size(); e++)
    {
      FeedForwardOutput(inputs[e]);

      for (unsigned int i = 0; i < data.neural_network[data.size - 1].size(); i++)
      {
        error = (outputs[e][i] - data.neural_network[data.neural_network.size() - 1][i].output);
				data.neural_network[data.neural_network.size() - 1][i].delta = sigmoide_p(data.neural_network[data.neural_network.size() - 1][i].input)*error;
        ERROR += error * error;
      }

      for (unsigned int i = data.size - 2; i >= 0; i--)
      {
        for (unsigned int j = 0; j < data.neural_network[i].size(); j++)
        {
          suma = 0;
          for (unsigned int k = 0; k < data.weights[i][j].size(); k++)
          {
            suma = suma + data.weights[i][j][k] * data.neural_network[i + 1][k].delta;
          }
					data.neural_network[i][j].delta = sigmoide_p(data.neural_network[i][j].input)*suma;
          for (unsigned int k = 0; k < data.neural_network[i + 1].size(); k++)
          {
						data.weights[i][j][k] = data.weights[i][j][k] + ALPHA * data.neural_network[i][j].output * data.neural_network[i + 1][k].delta;
          }
        }
      }
    }
    ERROR *= 0.5*(1.0 / inputs.size());
    
    iteration++;
  }
  //cout << "ERROR = " << ERROR << endl;
	data.final_error = ERROR;
  //cout << "ITERATIONS = " << iteration<< endl;
	data.final_iterations = iteration;

}

void Network::RL_BackPropagation(vector< vector < double > >& inputs, vector < vector < double > >& outputs)
{

	double suma = 0.0f;
	double ERROR = 200;
	double error = 0.0f;


	ERROR = 0;
	for (unsigned int e = 0; e < inputs.size(); e++)
	{
		FeedForwardOutput(inputs[e]);

		for (unsigned int i = 0; i < data.neural_network[data.size - 1].size(); i++)
		{
			error = (outputs[e][i] - data.neural_network[data.neural_network.size() - 1][i].output);
			data.neural_network[data.neural_network.size() - 1][i].delta = sigmoide_p(data.neural_network[data.neural_network.size() - 1][i].input)*error;
			ERROR += error * error;
		}

		for (unsigned int i = data.size - 2; i >= 0; i--)
		{
			for (unsigned int j = 0; j < data.neural_network[i].size(); j++)
			{
				suma = 0;
				for (unsigned int k = 0; k < data.weights[i][j].size(); k++)
				{
					suma = suma + data.weights[i][j][k] * data.neural_network[i + 1][k].delta;
				}
				data.neural_network[i][j].delta = sigmoide_p(data.neural_network[i][j].input)*suma;
				for (unsigned int k = 0; k < data.neural_network[i + 1].size(); k++)
				{
					data.weights[i][j][k] = data.weights[i][j][k] + ALPHA * data.neural_network[i][j].output * data.neural_network[i + 1][k].delta;
				}
			}
		}
		ERROR *= 0.5*(1.0 / inputs.size());
	}

}

void Network::SetErrorOffset(double offset) 
{
	data.offset_error = offset;
}

void Network::SetMaxIterations(int iterations)
{
	data.maxIterations = iterations;
}

int Network::Size() 
{
  return data.neural_network.size();
}

void Network::PrintWeights()
{
  cout << endl << "###############################" << endl;
  cout << "Weights" << endl;
  cout << "###############################" << endl << endl;
  for (unsigned int k = 0; k < data.weights.size(); k++)
  {
    cout << endl << "(" << k << ")   ";
    for (unsigned int j = 0; j < data.weights[k].size(); j++)
    {
      cout << "[" << j << "](";
      for (unsigned int i = 0; i < data.weights[k][j].size() - 1; i++)
      {
        cout << data.weights[k][j][i] << ", ";
      }
      cout << data.weights[k][j][data.weights[k][j].size() - 1] << ")    ";
    }
  }
  cout << endl;
}

void Network::PrintOutput()
{
  cout << endl << "###############################" << endl;
  cout << "Output" << endl;
  cout << "###############################" << endl << endl;
  for (unsigned int k = 0; k < data.neural_network.size(); k++)
  {
    cout << endl << "(" << k << ")   ";
    for (unsigned int j = 0; j < data.neural_network[k].size(); j++)
    {
      if (k != data.neural_network.size() - 1)
        cout << "[" << j << "](" << data.neural_network[k][j].output << ")    ";
      else
        cout << "[" << j << "](" << data.neural_network[k][j].output << ")";
    }
  }
  cout << endl;
}

void Network::PrintGlobalInfo() 
{
  cout << endl << "###############################" << endl;
  cout << "Neural Network Information" << endl;
  cout << "###############################" << endl << endl;
  cout << "ERROR = " << data.final_error << endl;
  cout << "ITERATIONS = " << data.final_iterations << endl;
  
  PrintWeights();

  PrintOutput();
}