#include <log.h>
#include <neural_network.h>


int main(int argc, char**argv) {

  vector< vector< double > > trainer1 = {
  {0},
  {2},
  {1}};

  vector< vector< double > > trainer2 = {
    {0.5},
    {1},
    {0}};

  Network* net = new Network(4, 1, 3, 3, 1);

	net->BackPropagation(trainer1, trainer2);



	return 0;
}