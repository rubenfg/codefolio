#include <vector>

using namespace std;

struct Node {
	double input = 0.0f;
	double output = 0.0f;
	double delta = 0.0f;

	Node::Node() {}
};

struct Data {
	int size = 0;
	int final_iterations = 0;
	int maxIterations = 10000;
	double final_error = 0;
	double offset_error = 0.0001;
	vector<vector<Node>> neural_network;
	vector< vector < vector <double> > > weights;

	Data::Data() {};
};