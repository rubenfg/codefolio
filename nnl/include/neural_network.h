#include <iostream>
#include <vector>
#include <math.h>
#include <random>
#include <fstream>
#include <time.h>
#include <data.h>
#include <stdarg.h>

using namespace std;

class Network
{
public:
	Network(int n_args, ...);
  ~Network();

  void PrintWeights();
  void PrintOutput();
  void PrintGlobalInfo();;

	vector<double> CData(int n_args, ...);

	void Init(int n_args, ...);

  void BackPropagation(vector< vector < double > >& inputs, vector < vector < double > >& outputs);
	void RL_BackPropagation(vector< vector < double > >& inputs, vector < vector < double > >& outputs);
  vector<double> FeedForwardOutput(vector < double >& inputs);
  double sigmoide(double z);
  double sigmoide_p(double x);

  void SetErrorOffset(double offset);
  void SetMaxIterations(int iterations);
  int Size();
	Data data;
private:

};