#include <iostream>
#include <fstream>
#include <vector>

class Log {

public :

  Log();
  ~Log();

  void Add(const char* msg);

  void Write();

  std::ofstream file;

  std::vector<const char*> vMsg;
};