// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Resource.generated.h"

UCLASS()
class NNL_RTS_API AResource : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AResource();

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
    float quantity = 500.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
    UStaticMeshComponent* static_mesh = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
    UParticleSystem* particle_hit = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

  UFUNCTION()
    float Extract(float quantity_extracted);
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
