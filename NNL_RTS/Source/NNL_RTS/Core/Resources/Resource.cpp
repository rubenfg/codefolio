// Fill out your copyright notice in the Description page of Project Settings.

#include "Resource.h"


// Sets default values
AResource::AResource()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

  RootComponent = static_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ResourceStaticMesh"));

}

// Called when the game starts or when spawned
void AResource::BeginPlay()
{
	Super::BeginPlay();
	
}

float AResource::Extract(float quantity_extracted)
{
  if (quantity <= 0.0f) return 0.0f;

  quantity -= quantity_extracted;
  
  return quantity_extracted;
}

// Called every frame
void AResource::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

