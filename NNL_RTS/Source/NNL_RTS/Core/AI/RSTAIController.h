// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "NNL_RTS/Core/Characters/Soldier.h"
#include "NNL_RTS/Core/Characters/Worker.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeComponent.h"
#include "RSTAIController.generated.h"

/**
 * 
 */
UCLASS()
class NNL_RTS_API ARSTAIController : public AAIController
{
	GENERATED_BODY()
public:
	ARSTAIController();

	UPROPERTY()
		UBehaviorTreeComponent* behaviorComp_;

	UPROPERTY()
		UBlackboardComponent* blackboardComp_;

	UFUNCTION()
		void Possess(class APawn* InPawn) override;

	UFUNCTION()
		void UnPossess() override;
	
	
};
