// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_Reach.generated.h"

/**
 * 
 */
UCLASS()
class NNL_RTS_API UBTT_Reach : public UBTTaskNode
{
	GENERATED_BODY()
	
	
	/** Execute task ONCE */
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp,
		uint8* NodeMemory) override;

	UPROPERTY(EditAnywhere)
		bool onlyUpdate;
	
};
