// Fill out your copyright notice in the Description page of Project Settings.

#include "BTT_SetTarget.h"
#include "NNL_RTS/Core/Characters/Soldier.h"
#include "NNL_RTS/Core/Characters/Worker.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "AIController.h"
#include "BehaviorTree/BehaviorTree.h"

EBTNodeResult::Type UBTT_SetTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) {

	AAIController* MyController = OwnerComp.GetAIOwner();
	auto BBComp = OwnerComp.GetBlackboardComponent();


	ASoldier* soldier = Cast<ASoldier>(MyController->GetPawn());
	if (soldier)
	{
		BBComp->SetValueAsVector("Target", soldier->target);
		soldier->Reached = false;
		soldier->CanMove = true;
		return EBTNodeResult::Succeeded;
	}
	AWorker* worker = Cast<AWorker>(MyController->GetPawn());
	if (worker)
	{
		BBComp->SetValueAsVector("Target", worker->target);
		worker->Reached = false;
		return EBTNodeResult::Succeeded;
	}
	return EBTNodeResult::Failed;
}




