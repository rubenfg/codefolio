// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTS_Soldier.generated.h"

/**
 * 
*/
UCLASS()
class NNL_RTS_API UBTS_Soldier : public UBTService
{
	GENERATED_BODY()
	
	
public:
	void TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds) override;
	
};
