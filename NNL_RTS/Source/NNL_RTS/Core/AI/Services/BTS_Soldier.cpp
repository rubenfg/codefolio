// Fill out your copyright notice in the Description page of Project Settings.

#include "BTS_Soldier.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "NNL_RTS/Core/Characters/Soldier.h"
#include "NNL_RTS/Core/Characters/Worker.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "AIController.h"
#include "BehaviorTree/BehaviorTree.h"



void UBTS_Soldier::TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds) {
	auto BBComp = OwnerComp.GetBlackboardComponent();

	if (!BBComp) return;

	auto soldier = Cast<ASoldier>(OwnerComp.GetAIOwner()->GetPawn());
	if (soldier) 
	{
		BBComp->SetValueAsBool("CanMove", soldier->CanMove);
		BBComp->SetValueAsVector("Target", soldier->target);
		BBComp->GetBlackboardAsset()->UpdateParentKeys();
	}

	auto worker = Cast<AWorker>(OwnerComp.GetAIOwner()->GetPawn());
	if (worker)
	{
		BBComp->SetValueAsBool("CanMove", worker->CanMove);
		BBComp->SetValueAsVector("Target", worker->target);
		BBComp->GetBlackboardAsset()->UpdateParentKeys();
	}

}

