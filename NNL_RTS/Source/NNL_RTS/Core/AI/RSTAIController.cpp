// Fill out your copyright notice in the Description page of Project Settings.

#include "RSTAIController.h"

ARSTAIController::ARSTAIController() {
	behaviorComp_ = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	blackboardComp_ = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));
}


void ARSTAIController::Possess(class APawn* InPawn) {
	Super::Possess(InPawn);
	ASoldier* agent = Cast<ASoldier>(InPawn);
	AWorker* agent2 = Cast<AWorker>(InPawn);
	if (agent && agent->BehTree) { //Check if the Pawn/Character has a reference to one
																			 // NOTE: Set the perception component to use when possessing the agent here!!
		//SetPerceptionComponent(*agent->perceptionComponent);
		if (agent->BehTree->BlackboardAsset) {
			{ //Check if the Pawn/Character has a reference to one
				blackboardComp_->InitializeBlackboard(*agent->BehTree->BlackboardAsset);
				behaviorComp_->StartTree(*agent->BehTree);
			}
		}
		
	}

	if (agent2 && agent2->BehTree) { //Check if the Pawn/Character has a reference to one
																			 // NOTE: Set the perception component to use when possessing the agent here!!
		//SetPerceptionComponent(*agent->perceptionComponent);
		if (agent2->BehTree->BlackboardAsset) {
			{ //Check if the Pawn/Character has a reference to one
				blackboardComp_->InitializeBlackboard(*agent2->BehTree->BlackboardAsset);
				behaviorComp_->StartTree(*agent2->BehTree);
			}
		}

	}
}

void ARSTAIController::UnPossess() {
	behaviorComp_->StopTree();
}

