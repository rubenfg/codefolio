// Fill out your copyright notice in the Description page of Project Settings.

#include "Soldier.h"
#include "NNL_RTS/Core/RTS_Manager.h"
#include "NNL_RTS/Core/Characters/Lord.h"
#include "NNL_RTS/Core/AI/RSTAIController.h"

// Sets default values
ASoldier::ASoldier()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

  neural_network = CreateDefaultSubobject<UNNL_Component>(TEXT("NeuralComponent"));

  //static_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
  //static_mesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ASoldier::BeginPlay()
{
	Super::BeginPlay();

  normal_scale = GetActorScale3D();

  if (lord) {
    TArray<AActor*> FoundActors;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), ALord::StaticClass(), FoundActors);
    for (int i = 0; i < FoundActors.Num(); i++)
    {
      ALord* lordToCheck = Cast<ALord>(FoundActors[i]);
      if (lordToCheck)
      {
        if (lord->id != lordToCheck->id)
        {
          enemy_lord = lordToCheck;
        }
      }
    }
  }

	SpawnDefaultController();
	AIContrn = Cast<ARSTAIController>(GetController());
	if (AIContrn)
	{
		AIContrn->RunBehaviorTree(BehTree);
	}


	this->CanMove = false;
	this->target = GetActorLocation();
	this->leader = false;
}

void ASoldier::BeginDestroy() 
{
	Super::BeginDestroy();

}

void ASoldier::OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void ASoldier::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}

void ASoldier::AttackOnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
  ASoldier* enemy = Cast<ASoldier>(OtherActor);
  if (enemy && (enemy->GetId() != GetId()))
  {
    enemy->ReceiveDamage(damage);
  }
}

void ASoldier::AttackTower(int index)
{

	CanMove = true;
//	if (FVector::Dist(enemy_lord->GetActorLocation(), GetActorLocation()) > 600.0f)
//	{
	
//	}
	
	//target = enemy_lord->attackT_position[FMath::RandRange(0, enemy_lord->attackT_position.Num() - 1)];
	target = enemy_lord->attackT_position[index];
	if (FVector::Dist(target, GetActorLocation()) < 500.0f)
	{
		DoDamageToTower(enemy_lord);
	}
}

void ASoldier::AttackEnemy(int index)
{
	int obj = 0;
	switch (AE_Step)
	{
	case 0:
		if ((enemy_lord->leaders.Num() > index) && (index >= 0))
		{
			if (FVector::Dist(enemy_lord->leaders[index]->GetActorLocation(), GetActorLocation()) > radius_detection)
			{
				target = enemy_lord->leaders[index]->GetActorLocation();
				enemy_target = enemy_lord->leaders[index];
				CanMove = true;
			}
			else
			{
				enemy_target = nullptr;
				//CanMove = false;
				AE_Step++;
			}
		}
		break;

	case 1:

		if (!enemy_target || enemy_target->dead)
		{
			if (enemy_lord) {
				obj = lord->GetRandomObjective(index);
			}
			if (obj < 0)
			{
				return;
			}
			if ((enemy_lord->followers.Num() > index) && (index >= 0))
			{
				if (obj >= enemy_lord->followers[index].values.Num())
				{
					target = enemy_lord->leaders[index]->GetActorLocation();
					enemy_target = enemy_lord->leaders[index];
				}
				else
				{
					target = enemy_lord->followers[index].values[obj]->GetActorLocation();
					enemy_target = enemy_lord->followers[index].values[obj];
				}
			}
			else 
			{
				target = enemy_lord->leaders[index]->GetActorLocation();
				enemy_target = enemy_lord->leaders[index];
			}

		}
		if (enemy_target)
		{
			if (!enemy_target->dead)
			{
				target = enemy_target->GetActorLocation();
				CanMove = true;
			}
		}

		if (FMath::Abs(FVector::Dist(target, GetActorLocation())) < distance_offset)
		{
			ASoldier* aux = GetClosestEnemy();
			if (!aux) 
			{
				AE_Step = 0;
				return;
			}
			
			DoDamage(aux);
			if (aux->dead)
			{

				enemy_target = nullptr;
				CanMove = false;
				AE_Step = 0;
			}
			enemy_target = nullptr;
			aux = nullptr;
		}
		break;
	};
}

void ASoldier::Move(FVector position_to_move) 
{

  if (FVector::Dist(GetActorLocation(), position_to_move) > distance_offset)
  {
    SetActorLocation(FMath::VInterpConstantTo(GetActorLocation(), position_to_move, d_time, 0.5));
  }
  SetActorRotation(UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), position_to_move));
}

void ASoldier::ReceiveDamage(float damage_received)
{
	if (particle_hit)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), particle_hit, GetActorLocation(), FRotator::ZeroRotator, true);
	}

  life -= damage_received;
  if (life <= 0.09f) 
  {
	  GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
    life = 0.0f;

		this->dead = true;
  }
}

void ASoldier::DoDamage(ASoldier* enemy)
{
  if (!cd && enemy)
  {
    if (particle_attack)  UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), particle_attack, GetActorLocation(), FRotator::ZeroRotator, true);
		AnimAttack = true;
		enemy->ReceiveDamage(damage);
		FTimerHandle CDTimer;
		cd = true;
		FTimerDelegate CDDelegate = FTimerDelegate::CreateUObject(this, &ASoldier::ResetCD);
		GetWorldTimerManager().SetTimer(CDTimer, CDDelegate, 1.0f, false);
  }
}

void ASoldier::DoDamageToTower(ALord* enemy)
{
	if (!cd)
	{
		if (particle_attack)  UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), particle_attack, GetActorLocation(), FRotator::ZeroRotator, true);
		enemy->ReceiveDamage(damage);
		FTimerHandle CDTimer;
		cd = true;
		FTimerDelegate CDDelegate = FTimerDelegate::CreateUObject(this, &ASoldier::ResetCD);
		GetWorldTimerManager().SetTimer(CDTimer, CDDelegate, 1.0f, false);
	}
}

void ASoldier::UpdateMaterial(int action_id)
{
  if ((actions_materials.Num() > action_id) && (action_id >=0)) 
  {
    static_mesh->SetMaterial(0, actions_materials[action_id]);
  }
}

int ASoldier::GetId() 
{
  if (!lord) return -1;

  return lord->id;
}

void ASoldier::ResetCD()
{
  cd = false;
}

void ASoldier::ReturnToFight()
{
  SetActorScale3D(normal_scale);
  life = 1.0f;
  inmune = false;
}

// Called every frame
void ASoldier::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

  d_time += DeltaTime;

}

// Called to bind functionality to input
void ASoldier::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

FDataArray ASoldier::CreateExample(int value1, int value2, int value3)
{
  FDataArray data;
  data.values.Add(value1);
  data.values.Add(value2);
  data.values.Add(value3);

  return data;
}

FDataArray ASoldier::CreateExample4(float value1, float value2, float value3, float value4)
{
  FDataArray data;
  data.values.Add(value1);
  data.values.Add(value2);
  data.values.Add(value3);
  data.values.Add(value4);

  return data;
}

int ASoldier::GetResultGreatestValue()
{
  if (nn_result.Num() <= 0) return 0;

  int index = 0;
  for (int i = 1; i < nn_result.Num(); i++)
  {
    if (nn_result[index] < nn_result[i])
      index = i;
  }

  return index;
}

ASoldier* ASoldier::GetRandomObjective(int leader_index)
{
	ASoldier* enemy_leader = enemy_lord->leaders[leader_index];

	TArray<ASoldier*> enemy_squad;
	enemy_squad.Add(enemy_leader);

	for (int i = 0; i < enemy_lord->followers[leader_index].values.Num(); i++)
	{
		enemy_squad.Add(enemy_lord->followers[leader_index].values[i]);
	}

	int rnd = FMath::RandRange(0, enemy_squad.Num()-1);

	return enemy_squad[rnd];
}


ASoldier* ASoldier::GetClosestEnemy() 
{
	if (enemy_lord->leaders.Num() == 0) return nullptr;

	ASoldier* aux = nullptr;

	float distance = FVector::Dist(GetActorLocation(), enemy_lord->leaders[0]->GetActorLocation());

	for (int i = 0; i < enemy_lord->leaders.Num(); i++)
	{
		float new_distance = FVector::Dist(GetActorLocation(), enemy_lord->leaders[i]->GetActorLocation());
		if (distance >= new_distance)
		{
			distance = new_distance;
			aux = enemy_lord->leaders[i];
		}
		for (int i = 0; i < enemy_lord->followers.Num(); i++)
		{
			for (int j = 0; j < enemy_lord->followers[i].values.Num(); j++)
			{
				float f_distance = FVector::Dist(GetActorLocation(), enemy_lord->followers[i].values[j]->GetActorLocation());
				if (distance >= f_distance)
				{
					distance = f_distance;
					aux = enemy_lord->followers[i].values[j];
				}
			}
		}
	}

  return aux;
}

void ASoldier::SetMaterial(UMaterialInstance* material)
{
	//static_mesh->SetMaterial(0, material);
}