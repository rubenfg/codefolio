// Fill out your copyright notice in the Description page of Project Settings.

#include "Worker.h"
#include "NNL_RTS/Core/Characters/Lord.h"
#include "NNL_RTS/Core/RTS_Manager.h"
#include "NNL_RTS/Core/AI/RSTAIController.h"

AWorker::AWorker()
{
  // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;

  neural_network = CreateDefaultSubobject<UNNL_Component>(TEXT("NeuralComponent"));

//  static_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
//  static_mesh->SetupAttachment(RootComponent);

  attack_box = CreateDefaultSubobject<UBoxComponent>(TEXT("AttackCollision"));
  attack_box->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWorker::BeginPlay()
{
  Super::BeginPlay();

	SpawnDefaultController();
	AIContrn = Cast<ARSTAIController>(GetController());
	if (AIContrn)
	{
		AIContrn->RunBehaviorTree(BehTree);
	}
}

void AWorker::OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void AWorker::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}

void AWorker::AttackOnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void AWorker::Mind()
{

}

void AWorker::Action()
{
}

void AWorker::Work()
{

}

void AWorker::Flee()
{

}

void AWorker::Move(FVector position_to_move)
{

}

void AWorker::ReceiveDamage(float damage_received)
{
  if (particle_hit)  UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), particle_hit, GetActorLocation(), FRotator::ZeroRotator, true);
  life -= damage;
  if (life <= 0.0f)
  {
    life = 0.0f;

    Destroy();
  }
}

void AWorker::DoWork()
{
  if(!cd)
  {
    d_time = 0.0f;
    cd = true;
		if (resource_load < MAX_resource_load)
		{
			resource_load += resource_target->Extract(q_extraction);
		}
    FTimerHandle CDTimer;
    FTimerDelegate CDDelegate = FTimerDelegate::CreateUObject(this, &AWorker::ResetCD);
    GetWorldTimerManager().SetTimer(CDTimer, CDDelegate, 1.0f, false);
  }
}

void AWorker::UpdateMaterial(int action_id)
{
  if (actions_materials.Num() == 3)
  {
   // static_mesh->SetMaterial(0, actions_materials[action_id]);
  }
}

void AWorker::CheckEnemiesInRadius()
{
  enemy_soldier_in_range.Empty();
  TArray<AActor*> FoundActors;
  UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASoldier::StaticClass(), FoundActors);
  TArray<ASoldier*> all_enemies;
  for (int i = 0; i < FoundActors.Num(); i++)
  {
    ASoldier* enemy = Cast<ASoldier>(FoundActors[i]);
    if (enemy && (enemy->GetId() != GetId()))
    {
      if (FVector::Dist(GetActorLocation(), enemy->GetActorLocation()) < radius_detection)
      {
        enemy_soldier_in_range.Add(std::move(enemy));
      }
    }
  }
}

int AWorker::GetId()
{
  if (!lord) return -1;

  return lord->id;
}

void AWorker::ResetCD()
{
  cd = false;
}

// Called every frame
void AWorker::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);

  d_time += DeltaTime;

  CheckEnemiesInRadius();
}

// Called to bind functionality to input
void AWorker::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
  Super::SetupPlayerInputComponent(PlayerInputComponent);

}

FDataArray AWorker::CreateExample(int value1, int value2, int value3)
{
  FDataArray data;
  data.values.Add(value1);
  data.values.Add(value2);
  data.values.Add(value3);

  return data;
}

FDataArray AWorker::CreateExample5(float value1, float value2, float value3, float value4, float value5)
{
  FDataArray data;
  data.values.Add(value1);
  data.values.Add(value2);
  data.values.Add(value3);
  data.values.Add(value4);
  data.values.Add(value5);

  return data;
}

int AWorker::GetResultGreatestValue()
{
  if (nn_result.Num() <= 0) return 0;

  int index = 0;
  for (int i = 1; i < nn_result.Num(); i++)
  {
    if (nn_result[index] < nn_result[i])
      index = i;
  }

  return index;
}

ASoldier* AWorker::GetClosestEnemy()
{
  if (enemy_soldier_in_range.Num() <= 0) return nullptr;

  float distance = FVector::Dist(GetActorLocation(), enemy_soldier_in_range[0]->GetActorLocation());
  int index = 0;

  for (int i = 1; i < enemy_soldier_in_range.Num(); i++)
  {
    float new_distance = FVector::Dist(GetActorLocation(), enemy_soldier_in_range[i]->GetActorLocation());
    if (distance > new_distance)
    {
      distance = new_distance;
      index = i;
    }
  }
  return enemy_soldier_in_range[index];
}

void AWorker::SetMaterial(UMaterialInstance* material)
{
	//static_mesh->SetMaterial(0, material);
}
