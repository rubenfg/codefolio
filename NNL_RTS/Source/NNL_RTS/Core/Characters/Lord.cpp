// Fill out your copyright notice in the Description page of Project Settings.

#include "Lord.h"
#include "NNL_RTS/Core/RTS_Manager.h"

// Sets default values
ALord::ALord()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	LordMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("lord_mesh"));
	RootComponent = LordMesh;
	
  SpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("spawn_point"));
  SpawnPoint->SetupAttachment(LordMesh);
	
  
  nn_soldiers = CreateDefaultSubobject<UNNL_Component>(TEXT("SoldiersNeuralComponent"));
  nn_workers = CreateDefaultSubobject<UNNL_Component>(TEXT("WorkersNeuralComponent"));
	main_nn = CreateDefaultSubobject<UNNL_Component>(TEXT("MainNNComponent"));
	attack_nn = CreateDefaultSubobject<UNNL_Component>(TEXT("AttackNNComponent"));
	wait_nn = CreateDefaultSubobject<UNNL_Component>(TEXT("WaitNNComponent"));
	resources_nn = CreateDefaultSubobject<UNNL_Component>(TEXT("ResourcesNNComponent"));
	test_data = CreateDefaultSubobject<UFileData>(TEXT("FileDataTest"));
	
  ResourcesText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("ResourcesText"));
  ResourcesText->SetupAttachment(LordMesh);
}

// Worker cost: 3
// Soldir cost: 5
//

//******************************************************************************
//  Input 1: Resources
//  Input 2: Number of soldiers
//  Input 3: Number of workers
// ----------------------------------------------------------------------------
//  Output 1: Wait
//  Output 2: Create Soldier
//  Output 3: Create Worker
//******************************************************************************

// Called when the game starts or when spawned
void ALord::BeginPlay()
{
	Super::BeginPlay();


	TArray<int> main_nn_layers = { 9, 81, 81, 3};

	TArray<int> attack_nn_layers = { 123,  175, 21 };

	TArray<int> wait_nn_layers = { 6, 36, 36, 4 };

	TArray<int> resources_nn_layers = { 8, 49, 49, 3 };

	main_nn->Init(main_nn_layers);

	attack_nn->Init(attack_nn_layers);

	wait_nn->Init(wait_nn_layers);

	resources_nn->Init(resources_nn_layers);

  TArray<int> nnw_layers = { 4, 20, 20, 20, 2 };
 
  TArray<FDataArray> nnw_it;
  nnw_it.Add(nn_workers->CData(4, 1.0, 0.3, 0, 0, 1));
	nnw_it.Add(nn_workers->CData(4, 1.0, 0.3, 0, 0, 0));
	nnw_it.Add(nn_workers->CData(4, 1.0, 0.3, 1.0, 0.3, 0));
	nnw_it.Add(nn_workers->CData(4, 1.0, 0.3, 1.0, 0.3, 1));
	nnw_it.Add(nn_workers->CData(4, 1.0, 0.3, 0.3, 0.3, 1));
	nnw_it.Add(nn_workers->CData(4, 1.0, 0.3, 0.3, 0.3, 0));
 
 
  TArray<FDataArray> nnw_ot;
	nnw_ot.Add(nn_workers->CData(2, 1, 0));
	nnw_ot.Add(nn_workers->CData(2, 0, 1));
  nnw_ot.Add(nn_workers->CData(2, 0, 1));
  nnw_ot.Add(nn_workers->CData(2, 0, 1));
  nnw_ot.Add(nn_workers->CData(2, 0, 1));
  nnw_ot.Add(nn_workers->CData(2, 0, 1));
 
  nn_workers->Init(nnw_layers);
 
  bool result = nn_workers->LoadWeightsData();
  if (!result) 
  {
	  nn_workers->BackPropagation(nnw_it, nnw_ot, useNNLFiles);
	  GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, "Data ERROR: Using Backpropagation");
  }

	double pi = 3.14159;
	for (double angle = 0; angle <= 2 * pi; angle += ((2 * pi) / 10))
	{
		formation_position.Add(FVector(formation_radius* cos(angle), formation_radius* sin(angle), 0.0f));
	}

	for (double angle = 0; angle <= 2 * pi; angle += ((2 * pi) / 20))
	{
		FVector pos = FVector(500.0f * cos(angle), 500.0f * sin(angle), 0.0f);
		pos.X += GetActorLocation().X;
		pos.Y += GetActorLocation().Y;
		attackT_position.Add(pos);
	}

  TArray<AActor*> FoundLords;
  UGameplayStatics::GetAllActorsOfClass(GetWorld(), ALord::StaticClass(), FoundLords);
  for (int i = 0; i < FoundLords.Num(); i++)
  {
		if (FoundLords[i]) {
			ALord* lordToCheck = Cast<ALord>(FoundLords[i]);
			if (lordToCheck->id != this->id)
			{
				enemy_lord = lordToCheck;
			}
		}
  }  



  TArray<AActor*> FoundActors;
  UGameplayStatics::GetAllActorsOfClass(GetWorld(), AResource::StaticClass(), FoundActors);
  for (int i = 0; i < FoundActors.Num(); i++)
  {
		if (FoundActors[i]) {
			if (FVector::Dist(FoundActors[i]->GetActorLocation(), GetActorLocation()) < 5000.0f)
			{
				Resourses.Add(std::move(Cast<AResource>(FoundActors[i])));
			}
		}
  }
  FoundActors.Empty();

	if (EmptyAllData)
	{
		if(main_nn->dt_weights)	main_nn->dt_weights->EmptyTable();
		if (main_nn->dt_RL_input)	main_nn->dt_RL_input->EmptyTable();
		if (main_nn->dt_RL_output)	main_nn->dt_RL_output->EmptyTable();

		if (attack_nn->dt_weights) attack_nn->dt_weights->EmptyTable();
		if (attack_nn->dt_RL_input)	attack_nn->dt_RL_input->EmptyTable();
		if (attack_nn->dt_RL_output)	attack_nn->dt_RL_output->EmptyTable();

		if (wait_nn->dt_weights) wait_nn->dt_weights->EmptyTable();
		if (wait_nn->dt_RL_input)	wait_nn->dt_RL_input->EmptyTable();
		if (wait_nn->dt_RL_output)	wait_nn->dt_RL_output->EmptyTable();

		resources_nn->dt_weights->EmptyTable();
	}

	main_nn->LoadWeightsData();
	attack_nn->LoadWeightsData();
	wait_nn->LoadWeightsData();
	resources_nn->LoadWeightsData();
}

void ALord::CreateSoldier()
{
  if (SoldierType) 
  {
    resources -= 5.0;

		float ran_x = 0.0f;
		float ran_y = 0.0f;
		FVector newLocation = FVector::ZeroVector;
		ASoldier* new_soldier = nullptr;

		ran_x = FMath::RandRange(-500, 500);
		ran_y = FMath::RandRange(-500, 500);

		
		newLocation.X = GetActorLocation().X + ran_x + 200.0f;
		newLocation.Y = GetActorLocation().Y + ran_y + 200.0f;
		newLocation.Z = 0.0f;

		free_soldiers.Add(GetWorld()->SpawnActor<ASoldier>(SoldierType, newLocation, FRotator::ZeroRotator));

		for (int i = 0; i < free_soldiers.Num(); i++)
		{
			if (free_soldiers[i]) {
				free_soldiers[i]->lord = this;
				free_soldiers[i]->enemy_lord = enemy_lord;
				if (faction_material)
				{
					free_soldiers[i]->SetMaterial(faction_material);
				}
			}
		}
  }
}

void ALord::CreateWorker() 
{
  if (WorkerType)
  {
    resources -= 3.0;

		float ran_x = 0.0f;
		float ran_y = 0.0f;
		FVector newLocation = FVector::ZeroVector;
		ASoldier* new_soldier = nullptr;

		ran_x = FMath::RandRange(-500, 500);
		ran_y = FMath::RandRange(-500, 500);


		newLocation.X = GetActorLocation().X + ran_x + 200.0f;
		newLocation.Y = GetActorLocation().Y + ran_y + 200.0f;
		newLocation.Z = 0.0f;

    workers.Add(std::move(GetWorld()->SpawnActor<AWorker>(WorkerType, newLocation, FRotator::ZeroRotator)));
		for (int i = 0; i < workers.Num(); i++)
		{
			if(faction_material)
			{
				workers[i]->SetMaterial(faction_material);
			}
			workers[i]->lord = this;
		}
  }
}

void ALord::SYS_Farm()
{
  for (int i = 0; i < workers.Num(); i++)
  {
    FString msg;
    FDataArray data;
    data.values.Add(workers[i]->life);
    data.values.Add(workers[i]->speed);

    ASoldier* enemy = workers[i]->GetClosestEnemy();

    float enemy_life = 0.0f;
    float enemy_speed = 0.0f;

    if (enemy)
    {
      enemy_life = enemy->life;
      enemy_speed = enemy->speed;
    }
    data.values.Add(enemy_life);
    data.values.Add(enemy_speed);

    float resource_in_range = 0.0f;

    workers[i]->resource_target = GetResource();

    if (FVector::Dist(workers[i]->GetActorLocation(), workers[i]->resource_target->GetActorLocation()) < workers[i]->radius_detection)
    {
      resource_in_range = 1.0f;
    }
    data.values.Add(resource_in_range);

    nn_result = nn_workers->FeedForwardOutput(data, false);

    int action = nn_workers->GetNodeActivated(nn_result);
    switch (action)
    {
    case 0:
      SYS_Farm_Work(workers[i]);
      break;
    case 1:
      SYS_Farm_Move(workers[i]);
      break;
    }
  }
}

void ALord::SYS_Farm_Work(AWorker* worker)
{

	if (worker->resource_load >= worker->MAX_resource_load)
	{
		if (!worker->CanMove) {
			worker->CanMove = true;
			worker->target = GetActorLocation();
		}
		if (FVector::Dist(worker->GetActorLocation(), worker->target) < 150.0f)
		{
			resources += worker->resource_load;
			worker->resource_load = 0.0f;
			worker->CanMove = false;
		}
	}
	else
	{
		if (!worker->CanMove)
		{
			worker->CanMove = true;
			worker->target = GetResource()->GetActorLocation();
		}
		if (FVector::Dist(worker->target, worker->GetActorLocation()) < 150.0f)
		{
			worker->DoWork();
			if (worker->resource_load >= worker->MAX_resource_load)
			{
				worker->CanMove = false;
			}
		}
	}
}


void ALord::SYS_Farm_Move(AWorker* worker)
{

}

void ALord::UpdateDecision()
{
	if (!CD_Decision_Update) {
		
		CD_Decision_Update = true;

		FTimerHandle DecisionTimerHandle;
		FTimerDelegate DecisionDelegate = FTimerDelegate::CreateUObject(this, &ALord::ResetCDecision);
		GetWorldTimerManager().SetTimer(DecisionTimerHandle, DecisionDelegate, Time_Update, false);

		for (int i = 0; i < leaders.Num(); i++)
		{
			FDataArray main_nn_data = main_nn->CData(
				9,
				AVG_Life(i), AVG_Speed(i), NUM_Soldiers(i), NUM_TotalSoldiers(),
				NUM_SoldiersNear(i), enemy_lord->NUM_TotalSoldiers(), life, enemy_lord->life,
				CHECK_NearTower(i)
			);

			int action = -1;
			nn_result = main_nn->FeedForwardOutput(main_nn_data, false);
			action = main_nn->GetNodeActivated(nn_result);

			for (int t = 0; t < nn_result.Num(); t++)
			{
				//FString msg = FString::SanitizeFloat(nn_result[t]);
				//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, msg);
				
			}

			leaders[i]->fAction = action;
			if ((followers.Num() > i) && (i >= 0))
			{
				for (int y = 0; y < followers[i].values.Num(); y++)
				{
					followers[i].values[y]->fAction = action;
				}
			}
			FDataArray output;
			switch (leaders[i]->fAction)
			{
			case 0:
			//	if (isRandom) {
					//output = main_nn->CData(3, 0.9, 0.0, 0.0);

					output = main_nn->CData(3, 0.5, 0.0, 0.0);
					RL_SendData(i, leaders[i]->fAction, 0, main_nn, main_nn_data, output);
			//	}
				//SYS_Attack_Attack(i);
					UpdateAttackDecision(i);
				break;
			case 1:
				//if (isRandom) {
					output = main_nn->CData(3, 0.0, 0.7, 0.0);
					RL_SendData(i, leaders[i]->fAction, 0, main_nn, main_nn_data, output);
			//	}
			//	SYS_Attack_Move(i);

				break;
			case 2:
			//	if (isRandom) {
					output = main_nn->CData(3, 0.0, 0.0, 0.7);
					RL_SendData(i, leaders[i]->fAction, 0, main_nn, main_nn_data, output);
			//	}
			//	SYS_Attack_Wait(i);
					UpdateWaitDecision(i);
				break;
			}

		//	switch (train)
		//	{
		//	case true:
		//
		//		
		//		break;
		//	case false:
		//
		//		nn_result = main_nn->FeedForwardOutput(main_nn_data, false);
		//		action = GetNodeActivated(nn_result);
		//
		//		break;
		//	}

		}

	}
}

void ALord::UpdateAttackDecision(int leader_index)
{
	bool isRandom = false;

	FDataArray attack_nn_data;
	attack_nn_data.values.Add(life);
	attack_nn_data.values.Add(enemy_lord->life);
	attack_nn_data.values.Add(NUM_SoldiersNear(leader_index));
	for (int i = 0; i < 20; i++)
	{
		if (i >= enemy_lord->leaders.Num())
		{
			attack_nn_data.values.Add(0);
			attack_nn_data.values.Add(0);
			attack_nn_data.values.Add(0);
			attack_nn_data.values.Add(0);
			attack_nn_data.values.Add(0);
			attack_nn_data.values.Add(0);
		}
		else
		{
			attack_nn_data.values.Add(enemy_lord->AVG_Life(i));
			attack_nn_data.values.Add(enemy_lord->AVG_Speed(i));
			attack_nn_data.values.Add(enemy_lord->NUM_Soldiers(i));
			attack_nn_data.values.Add(enemy_lord->NUM_EnemiesNear(i));
			attack_nn_data.values.Add(enemy_lord->CHECK_NearTowerEnemy(i));
			attack_nn_data.values.Add(enemy_lord->NUM_AlliesNearEnemy(i));
		}
	}

	int action = -1;

	nn_result = attack_nn->FeedForwardOutput(attack_nn_data, false);
	action = attack_nn->GetNodeActivated(nn_result);

//	FString msg = FString::FromInt(action);
//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, msg);

	if (action > enemy_lord->leaders.Num())
	{
		action = FMath::RandRange(1, enemy_lord->leaders.Num());
	}
	leaders[leader_index]->sAction = action;
	if ((followers.Num() > leader_index) && (leader_index >= 0))
	{
		for (int i = 0; i < followers[leader_index].values.Num(); i++)
		{
			followers[leader_index].values[i]->sAction = action;
		}
	}
	FDataArray output;
	for (int i = 0; i < 21; i++)
	{
		output.values.Add(0.0);
		//output.values.Add(nn_result[i]);
	}

	if (leaders[leader_index]->sAction <= 0)
	{
		output.values[0] = 0.8f;
		//if (isRandom)
		//{
			RL_SendData(leader_index, leaders[leader_index]->sAction, 1, attack_nn, attack_nn_data, output);
		//}
		//AttackTower(leader_index);
	}
	else
	{
		output.values[leaders[leader_index]->sAction] = 0.8f;
		if (train)
		{
			//if (isRandom) {
			//  enemy_lord->leaders[leaders[leader_index]->sAction] 
			if (enemy_lord->leaders.Num() > leaders[leader_index]->sAction) {
				if (FVector::Dist(enemy_lord->leaders[leaders[leader_index]->sAction]->GetActorLocation(), GetActorLocation()))
				{
					output.values[leaders[leader_index]->sAction] = 1.0f;
					RL_SendData(leader_index, leaders[leader_index]->sAction, 1, attack_nn, attack_nn_data, output);
					main_nn->RL_SaveData(leaders[leader_index]->RL_data_index[0][0], leaders[leader_index]->RL_data_index[0][1]);
					attack_nn->RL_SaveData(leaders[leader_index]->RL_data_index[1][0], leaders[leader_index]->RL_data_index[1][1]);
					ResetData(leader_index);
					return;
				}
				RL_SendData(leader_index, leaders[leader_index]->sAction, 1, attack_nn, attack_nn_data, output);
			}
		}
		//}
		//AttackSoldier(leader_index, action);
	}
}

void ALord::UpdateWaitDecision(int leader_index)
{
	bool isRandom = false;

	FDataArray wait_nn_data = wait_nn->CData(
		6,
		AVG_Life(leader_index), AVG_Speed(leader_index), NUM_Soldiers(leader_index),
		NUM_SoldiersNear(leader_index), NUM_FreeSoldiers(), NUM_Resources()
	);

	int action = -1;

	nn_result = wait_nn->FeedForwardOutput(wait_nn_data, false);
	action = wait_nn->GetNodeActivated(nn_result);
	leaders[leader_index]->sAction = action;
	if ((followers.Num() > leader_index) && (leader_index >= 0))
	{
		for (int i = 0; i < followers[leader_index].values.Num(); i++)
		{
			followers[leader_index].values[i]->sAction = action;
		}
	}

	FDataArray output;
	switch (leaders[leader_index]->sAction)
	{
	case 0:
		output = wait_nn->CData(4, 0.7, 0.0, 0.0, 0.0);
		RL_SendData(leader_index, leaders[leader_index]->sAction, 2, wait_nn, wait_nn_data, output);
		leaders[leader_index]->CanMove = false;
		if ((followers.Num() > leader_index) && (leader_index >= 0))
		{
			for (int i = 0; i < followers[leader_index].values.Num(); i++)
			{
				followers[leader_index].values[i]->CanMove = false;
			}
		}

		if (CHECK_State(leader_index, AVG_Life(leader_index), AVG_Speed(leader_index), NUM_Soldiers(leader_index), life))
		{
			if (train)
			{
				main_nn->RL_SaveData(leaders[leader_index]->RL_data_index[0][0], leaders[leader_index]->RL_data_index[0][1]);
				wait_nn->RL_SaveData(leaders[leader_index]->RL_data_index[2][0], leaders[leader_index]->RL_data_index[2][1]);
				ResetData(leader_index);
			}
		}
		else
		{
			if (train)
			{
				ResetData(leader_index);
			}
		}
		break;
	case 1:
	//	if (isRandom)
	//	{
			output = wait_nn->CData(4, 0.0, 1.0, 0.0, 0.0);
			RL_SendData(leader_index, leaders[leader_index]->sAction, 2, wait_nn, wait_nn_data, output);
	//	}
	//	IncreaseGroup(leader_index);

		break;
	case 2:
		//if (isRandom)
		//{
			output = wait_nn->CData(4, 0.0, 0.0, 0.9, 0.0);
			RL_SendData(leader_index, leaders[leader_index]->sAction, 2, wait_nn, wait_nn_data, output);
		//}
		//RequestSoldier(leader_index);

		break;
	case 3:
		//if (isRandom)
		//{
			output = wait_nn->CData(4, 0.0, 0.0, 0.0, 1.0);
			RL_SendData(leader_index, leaders[leader_index]->sAction, 2, wait_nn, wait_nn_data, output);
		//}
		//UpdateFormation(leader_index);

		break;
	}
}

void ALord::SYS_Attack()
{
	
		for (int i = 0; i < leaders.Num(); i++)
		{
			bool isRandom = false;

		//	FDataArray main_nn_data = main_nn->CData(
		//		9,
		//		AVG_Life(i), AVG_Speed(i), NUM_Soldiers(i), NUM_TotalSoldiers(),
		//		NUM_SoldiersNear(i), enemy_lord->NUM_TotalSoldiers(), life, enemy_lord->life,
		//		CHECK_NearTower(i)
		//	);

			//nn_result = main_nn->FeedForwardOutput(main_nn_data, false);
			//int action = GetResultGreatestValue();

			
		//	leaders[i]->CanMove = false;
		//	if ((followers.Num() > i) && (i >= 0))
		//	{
		//		if (followers.Num() > 0) {
		//			for (int j = 0; j < followers[i].values.Num(); j++)
		//			{
	////					followers[i].values[j]->CanMove = false;
		//			}
		//		}
		//	}
		//	action = 0;
			FDataArray output;
			switch (leaders[i]->fAction)
			{
			case 0:
			//	if (isRandom) {
			//		output = main_nn->CData(3, 0.9, 0.0, 0.0);
			//		RL_SendData(i, fAction, 0, main_nn, main_nn_data, output);
			//	}
				SYS_Attack_Attack(i);

				break;
			case 1:
			//	if (isRandom) {
			//		output = main_nn->CData(3, 0.0, 0.7, 0.0);
			//		RL_SendData(i, action, 0, main_nn, main_nn_data, output);
			//	}
				SYS_Attack_Move(i);

				break;
			case 2:
			//	if (isRandom) {
			//		output = main_nn->CData(3, 0.0, 0.0, 0.9);
			//		RL_SendData(i, action, 0, main_nn, main_nn_data, output);
			//	}
				SYS_Attack_Wait(i);

				break;
			}
		}
}

void ALord::SYS_Attack_Attack(int index) 
{
	//bool isRandom = false;

	//FDataArray attack_nn_data;
	//attack_nn_data.values.Add(life);
	//attack_nn_data.values.Add(enemy_lord->life);
	//attack_nn_data.values.Add(NUM_SoldiersNear(index));
	//for (int i = 0; i < 20; i++)
	//{
	//	if (i >= enemy_lord->leaders.Num()) 
	//	{
	//		attack_nn_data.values.Add(0);
	//		attack_nn_data.values.Add(0);
	//		attack_nn_data.values.Add(0);
	//		attack_nn_data.values.Add(0);
	//		attack_nn_data.values.Add(0);
	//		attack_nn_data.values.Add(0);
	//	}
	//	else 
	//	{
	//		attack_nn_data.values.Add(enemy_lord->AVG_Life(i));
	//		attack_nn_data.values.Add(enemy_lord->AVG_Speed(i));
	//		attack_nn_data.values.Add(enemy_lord->NUM_Soldiers(i));
	//		attack_nn_data.values.Add(enemy_lord->NUM_EnemiesNear(i));
	//		attack_nn_data.values.Add(enemy_lord->CHECK_NearTowerEnemy(i));
	//		attack_nn_data.values.Add(enemy_lord->NUM_AlliesNearEnemy(i));
	//	}
	//}
	//FString msg = FString::SanitizeFloat(attack_nn_data.values.Num());
	//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, msg);
	//int action = GetActionIndex(index, 1);
	//if (action < 0)
	//{
////		action = FMath::RandRange(0, 50);
	//	isRandom = true;
	//	action = FMath::RandRange(0, enemy_lord->leaders.Num());
	//}
//	int action = -1;
//
//	switch (train)
//	{
//	case true:
//
//		nn_result = attack_nn->FeedForwardOutput(attack_nn_data, false);
//		isRandom = true;
//		action = GetNodeActivated(nn_result);
//		break;
//	case false:
//
//		nn_result = attack_nn->FeedForwardOutput(attack_nn_data, false);
//		action = GetNodeActivated(nn_result);
//
//		break;
//	}

	//FDataArray output;
	//for (int i = 0; i < 21; i++)
	//{
	//	output.values.Add(0.0);
	//}

	if (leaders[index]->sAction == 0)
	{
		//output.values[action] = 0.9f;
		//if (isRandom)
		//{
		//	RL_SendData(index, action, 1, attack_nn, attack_nn_data, output);
		//}
		AttackTower(index);
	}
	else 
	{
		//output.values[action] = 0.7f;
		//if (isRandom) {
		//	RL_SendData(index, action, 1, attack_nn, attack_nn_data, output);
		//}
		AttackSoldier(index, leaders[index]->sAction);
	}
}

void ALord::SYS_Attack_Move(int index)
{
	FVector randomPosition = FVector::ZeroVector;
	if (!leaders[index]->CanMove) {
		
		randomPosition.X = FMath::RandRange(-2000, 2000) + leaders[index]->GetActorLocation().X;
		randomPosition.Y = FMath::RandRange(-2000, 2000) + leaders[index]->GetActorLocation().Y;
		leaders[index]->target = randomPosition;
		leaders[index]->CanMove = true;
	}

	if ((followers.Num() > index) && (index >= 0))
	{
		for (int i = 0; i < followers[index].values.Num(); i++)
		{
			if (!followers[index].values[i]->CanMove)
			{
				FVector position = FVector(
					randomPosition.X + formation_position[i].X,
					randomPosition.Y + formation_position[i].Y,
					0.0f
				);

				followers[index].values[i]->target = position;
				followers[index].values[i]->CanMove = true;
			}
		}
	}

	//if (CheckSquadReached(index), true)
	//{
	//	if (CHECK_State(index, AVG_Life(index), AVG_Speed(index), NUM_Soldiers(index), life))
	//	{
	//	//	main_nn->RL_SaveData(leaders[index]->RL_data_index[0][0], leaders[index]->RL_data_index[0][1]);			
	//	}
	//	ResetData(index);
	//}
}

void ALord::SYS_Attack_Wait(int index)
{

//	bool isRandom = false;
//
//	FDataArray wait_nn_data = wait_nn->CData(
//		6,
//		AVG_Life(index), AVG_Speed(index), NUM_Soldiers(index),
//		NUM_SoldiersNear(index), NUM_FreeSoldiers(), NUM_Resources()
//	);
//
//	int action = -1;
//
//	switch (train)
//	{
//	case true:
//
//		nn_result = wait_nn->FeedForwardOutput(wait_nn_data, false);
//		isRandom = true;
//		action = GetNodeActivated(nn_result);
//		break;
//	case false:
//
//		nn_result = wait_nn->FeedForwardOutput(wait_nn_data, false);
//		action = GetNodeActivated(nn_result);
//
//		break;
//	}
//
//	FDataArray output;
	switch (leaders[index]->sAction)
	{
	case 0:
		//if (isRandom)
		//{
		//	output = wait_nn->CData(4, 0.7, 0.0, 0.0, 0.0);
		//	RL_SendData(index, action, 2, wait_nn, wait_nn_data, output);
		//	leaders[index]->CanMove = false;
		//	if ((followers.Num() > index) && (index >= 0))
		//	{
		//		for (int i = 0; i < followers[index].values.Num(); i++)
		//		{
		//			followers[index].values[i]->CanMove = false;
		//		}
		//	}
		//}
		//if (CHECK_State(index, AVG_Life(index), AVG_Speed(index), NUM_Soldiers(index), life))
		//{
		//	main_nn->RL_SaveData(leaders[index]->RL_data_index[0][0], leaders[index]->RL_data_index[0][1]);
		//	wait_nn->RL_SaveData(leaders[index]->RL_data_index[2][0], leaders[index]->RL_data_index[2][1]);
		//	ResetData(index);
		//}
		//else
		//{
		//	ResetData(index);
		//}
		break;
	case 1:
		//if (isRandom)
		//{
		//	output = wait_nn->CData(4, 0.0, 1.0, 0.0, 0.0);
		//	RL_SendData(index, action, 2, wait_nn, wait_nn_data, output);
		//}
		IncreaseGroup(index);

		break;
	case 2:
		//if (isRandom)
		//{
		//	output = wait_nn->CData(4, 0.0, 0.0, 0.9, 0.0);
		//	RL_SendData(index, action, 2, wait_nn, wait_nn_data, output);
		//}
		RequestSoldier(index);

		break;
	case 3:
		//if (isRandom)
		//{
		//	output = wait_nn->CData(4, 0.0, 0.0, 0.0, 1.0);
		//	RL_SendData(index, action, 2, wait_nn, wait_nn_data, output);
		//}
		UpdateFormation(index);

		break;
	}

}

AResource* ALord::GetResource()
{
	TArray<int> index;

  for (int i = 0; i < Resourses.Num(); i++)
  {
    if (Resourses[i]->quantity > 0.0f)
    {
			index.Add(i);
    }
  }
  return Resourses[FMath::RandRange(0, index.Num() - 1)];
}

void ALord::ChangeLeader(ASoldier* old, ASoldier* new_leader)
{
  int index = leaders.Find(old);
  if (index != INDEX_NONE) {
    leaders.Add(new_leader);
  }
  else 
  {
    leaders.Add(new_leader);
  }
}

ASoldier* ALord::GetNearestRequest(ASoldier* leader_ignore)
{
  TArray<ASoldier*> aux_reqlist;
  ASoldier* leader_request = nullptr;

  for (int i = 0; i < request_help_list.Num(); i++)
  {
    aux_reqlist.Add(request_help_list[i]);
  }

  aux_reqlist.Remove(leader_ignore);
	if (aux_reqlist.Num() > 0 && leader_ignore)
	{
		int index = 0;
		float distance = 100000.0f;
		if (request_help_list[0]) {
			distance = FVector::Dist(leader_ignore->GetActorLocation(), request_help_list[0]->GetActorLocation());
		}
    for (int i = 1; i < aux_reqlist.Num(); i++)
    {
			if (aux_reqlist[i]) {
				float new_distance = FVector::Dist(leader_ignore->GetActorLocation(), aux_reqlist[i]->GetActorLocation());
				if (distance > new_distance)
				{
					distance = new_distance;
					index = i;
					leader_request = aux_reqlist[i];
				}
			}
    }
  }

  request_help_list.Remove(leader_request);

  return leader_request;
}

void ALord::Update()
{
	ResourcesText->SetText(FText::FromString(FString::SanitizeFloat(resources)));
	if (enemy_lord)
	{

		if (counterTime > 1.0f)
		{
			resources += 0.5;
			counterTime = 0.0f;
		}
		SYS_ResourceManager();
		UpdateDecision();
		SYS_Attack();
		SYS_Farm();
		LeadersWololo();
		AnswerRequest();
	}
}

// Called every frame
void ALord::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	counterTime += DeltaTime;
}

int ALord::GetResultGreatestValue() 
{
  if (nn_result.Num() <= 0) return 0;

  int index = 0;
  for (int i = 1; i < nn_result.Num(); i++)
  {
    if (nn_result[index] < nn_result[i])
      index = i;
  }

  return index;
}

int ALord::GreatestValueFromArray(TArray<double>& data)
{
	if (data.Num() <= 0) return 0;

	int index = 0;
	for (int i = 1; i < data.Num(); i++)
	{
		if (data[index] < data[i])
			index = i;
	}

	return index;
}

void ALord::KillSoldiers()
{
	TArray<ASoldier*> new_leaders;

	TArray<FSoldierDataArray> new_followers;

	FSoldierDataArray aux_farray;

	TArray<ASoldier*> deads;

	for (int i = 0; i < leaders.Num(); i++)
	{
		if (!leaders[i]->dead) 
		{
			new_leaders.Add(leaders[i]);
			if ((followers.Num() > i) && (i >= 0))
			{
				if (followers[i].values.Num() > 0)
				{
					for (int j = 0; j < followers[i].values.Num(); j++)
					{
						if (followers[i].values[j])
						{
							if (!followers[i].values[j]->dead)
							{
								aux_farray.values.Add(followers[i].values[j]);
							}
							else
							{
								deads.Add(followers[i].values[j]);
							}
						}
					}
				}
			}
			new_followers.Add(std::move(aux_farray));
			aux_farray.values.Empty();
		}
	}

	for (int i = 0; i < leaders.Num(); i++)
	{
		if (leaders[i]->dead)
		{
			if (followers.Num() > i)
			{
				if (followers[i].values.Num() > 0)
				{
					for (int j = 0; j < followers[i].values.Num(); j++)
					{
						if (followers[i].values[j]->dead)
						{
							deads.Add(followers[i].values[j]);
						}
						else 
						{
							aux_farray.values.Add(followers[i].values[j]);
						}
					}
					if (aux_farray.values.Num() > 0)
					{
						ASoldier* aux = aux_farray.values.Pop(true);
						aux->leader = true;
						aux->RL_data_index = leaders[i]->RL_data_index;
						aux->info_data = leaders[i]->info_data;
						aux->action_index = leaders[i]->action_index;
						aux->AE_Step = leaders[i]->AE_Step;

						new_leaders.Add(std::move(aux));
						new_followers.Add(std::move(aux_farray));
					}
					aux_farray.values.Empty();
				}
			}
			TArray<int> ig_aux = increase_group_arr;
			for (int h = 0; h < ig_aux.Num(); h++)
			{
				if (ig_aux[h] == i)
				{
					increase_group_arr.Remove(h);
				}
			}
			ig_aux.Empty();
			deads.Add(leaders[i]);
		}
	}

	for (int i = 0; i < nldr.Num(); i++)
	{
		new_leaders.Add(std::move(nldr[i]));
	}
	nldr.Empty();

	int max = deads.Num();

	leaders.Empty();
	followers.Empty();

	leaders = std::move(new_leaders);
	followers = std::move(new_followers);
	new_leaders.Empty();
	new_followers.Empty();

	for (int i = 0; i < max; i++)
	{
		deads[i]->Destroy();
	}
//	GetWorld()->ForceGarbageCollection(true);
	
	deads.Empty();
}

float ALord::AVG_Life(int leader_index)
{
	float avg_life = leaders[leader_index]->life;
	if ((followers.Num() > leader_index) && (leader_index >= 0))
	{
		for (int i = 0; i < followers[leader_index].values.Num(); i++)
		{
			avg_life += followers[leader_index].values[i]->life;
		}
		return (avg_life / (followers[leader_index].values.Num() + 1));
	}
	return (avg_life);
}

float ALord::AVG_Speed(int leader_index)
{
	float avg_speed = leaders[leader_index]->speed;

	if ((followers.Num() > leader_index) && (leader_index >= 0))
	{
		for (int i = 0; i < followers[leader_index].values.Num(); i++)
		{
			avg_speed += followers[leader_index].values[i]->speed;
		}
		return (avg_speed / (followers[leader_index].values.Num() + 1));
	}
	return (avg_speed);
}

float ALord::NUM_Soldiers(int leader_index)
{

	if ((followers.Num() > leader_index) && (leader_index >= 0)) 
	{
		return ((followers[leader_index].values.Num() + 1) / 11.0f);
	}
	return (1/10.0f);
}

float ALord::NUM_TotalSoldiers()
{
	float counter_soldiers = 0;
	for (int i = 0; i < leaders.Num(); i++)
	{
		counter_soldiers++;
		if ((followers.Num() > i) && (i >= 0))
		{
			for (int j = 0; j < followers[i].values.Num(); j++)
			{
				counter_soldiers++;
			}
		}
	}

	return (counter_soldiers/1000.0f);
}

float ALord::NUM_EnemiesNear(int leader_index)
{
	float counter_soldiers = 0;
	if ((enemy_lord->leaders.Num() > leader_index) && (leader_index >= 0))
	{
		for (int i = 0; i < enemy_lord->leaders.Num(); i++)
		{
			if (FVector::Dist(leaders[leader_index]->GetActorLocation(), enemy_lord->leaders[i]->GetActorLocation()) < soldiers_rp)
			{
				counter_soldiers += enemy_lord->NUM_Soldiers(i);
			}
		}
	}
	return (counter_soldiers / 1000.0f);
}

float ALord::NUM_AlliesNearEnemy(int leader_index)
{
	float counter_soldiers = 0;
	if ((enemy_lord->leaders.Num() > leader_index) && (leader_index >= 0))
	{
		for (int i = 0; i < leaders.Num(); i++)
		{
			if (FVector::Dist(leaders[i]->GetActorLocation(), enemy_lord->leaders[leader_index]->GetActorLocation()) < soldiers_rp)
			{
				counter_soldiers += NUM_Soldiers(i);
			}
		}
	}
	return (counter_soldiers / 1000.0f);
}

float ALord::NUM_SoldiersNear(int leader_index)
{
	float counter_soldiers = 0;
	for (int i = 0; i < leaders.Num(); i++)
	{
		if (i != leader_index) {
			if (FVector::Dist(leaders[i]->GetActorLocation(), leaders[leader_index]->GetActorLocation()) < radioproximity)
			{
				counter_soldiers++;
				if ((followers.Num() > i) && (i >= 0))
				{
					for (int j = 0; j < followers[i].values.Num(); j++)
					{
						counter_soldiers++;
					}
				}
			}
		}
	}

	return (counter_soldiers / 1000.0f);
}

float ALord::NUM_FreeSoldiers()
{
	return (free_soldiers.Num() / 10.0f);
}

float ALord::NUM_Resources()
{
	return (resources / 1000.0f);
}

float ALord::NUM_TotalWorkers()
{
	return (workers.Num() / 1000.0f);
}

float ALord::CHECK_NearTower(int leader_index)
{
	float val = 0.0f;
	if (FVector::Dist(leaders[leader_index]->GetActorLocation(), GetActorLocation()) < radioproximity)
	{
		val = 1.0f;
	}
	return val;
}

float ALord::CHECK_NearTowerEnemy(int leader_index)
{
	float val = 0.0f;
	if ((enemy_lord->leaders.Num() > leader_index) && (leader_index >= 0))
	{
		if (FVector::Dist(enemy_lord->leaders[leader_index]->GetActorLocation(), GetActorLocation()) < radioproximity)
		{
			val = 1.0f;
		}
	}
	return val;
}

bool ALord::CHECK_State(int leader_index, float al_val, float as_val, float ns_val, float tl_val)
{
	if ((leaders.Num() > leader_index) && (leader_index >= 0) && leaders[leader_index]->info_data.Num() > 0) {

		if (!CompareValues(leaders[leader_index]->info_data[0], al_val)) return false;

		if (!CompareValues(leaders[leader_index]->info_data[1], as_val)) return false;

		if (!CompareValues(leaders[leader_index]->info_data[2], ns_val)) return false;

		if (!CompareValues(leaders[leader_index]->info_data[3], tl_val)) return false;

		return true;

	}

	return false;

}

bool ALord::CHECK_EnemySquadDead(int enemy_leader_index)
{
	if (enemy_lord && enemy_lord->leaders.Num() > 0 && enemy_lord->leaders.Num() > enemy_leader_index && enemy_leader_index >= 0)
	{
		//if (!enemy_lord->leaders[enemy_leader_index]) return false;
		if (!enemy_lord->leaders[enemy_leader_index]->dead) return false;

		if ((enemy_lord->followers.Num() > enemy_leader_index) && (enemy_leader_index >= 0))
		{
			for (int i = 0; i < enemy_lord->followers[enemy_leader_index].values.Num(); i++)
			{
				if (!enemy_lord->followers[enemy_leader_index].values[i]->dead) return false;
			}
		}
	}
	return true;
}

TArray<int> ALord::GetLeadersInRange(FVector position, float radius)
{
	TArray<int> lir;

	if (enemy_lord) {
		for (int i = 0; i < enemy_lord->leaders.Num(); i++)
		{
			ASoldier* enemy = Cast<ASoldier>(enemy_lord->leaders[i]);
			if (enemy && (enemy->GetId() != id) && enemy->leader && !enemy->dead)
			{
				if (FVector::Dist(GetActorLocation(), enemy->GetActorLocation()) < radius)
				{
					lir.Add(i);
				}
			}
		}
	}

	return lir;
}

void ALord::ResetCDecision()
{
	CD_Decision_Update = false;
}

void ALord::IncreaseGroup(int leader_index)
{
	if ((followers.Num() > leader_index) && (leader_index >= 0))
	{
		if (followers[leader_index].values.Num() >= 10)
		{
			ResetData(leader_index);
			return;
		}

		increase_group_arr.Push(leader_index);
		if (train)
		{
			main_nn->RL_SaveData(leaders[leader_index]->RL_data_index[0][0], leaders[leader_index]->RL_data_index[0][1]);
			wait_nn->RL_SaveData(leaders[leader_index]->RL_data_index[2][0], leaders[leader_index]->RL_data_index[2][1]);
			ResetData(leader_index);
		}
		return;
	}
	if (train)
	{
		ResetData(leader_index);
	}
	return;
}

void ALord::RequestSoldier(int leader_index)
{

	leaders[leader_index]->CanMove = false;

	if ((followers.Num() > leader_index) && (leader_index >= 0))
	{
		for (int i = 0; i < followers[leader_index].values.Num(); i++)
		{
			followers[leader_index].values[i]->CanMove = false;
		}

		if (followers[leader_index].values.Num() >= 10)
		{
			ResetData(leader_index);
			return;
		}
	}
	request_slist.Add(leaders[leader_index], 0);

	for (auto& element : request_slist)
	{
		if (element.Key == leaders[leader_index])
		{
			element.Value++;
			if (train)
			{
				main_nn->RL_SaveData(leaders[leader_index]->RL_data_index[0][0], leaders[leader_index]->RL_data_index[0][1]);
				wait_nn->RL_SaveData(leaders[leader_index]->RL_data_index[2][0], leaders[leader_index]->RL_data_index[2][1]);
				ResetData(leader_index);
			}
			return;
		}
	}
	if (train)
	{
		ResetData(leader_index);
	}
}

void ALord::UpdateFormation(int leader_index)
{
	FVector new_target = FVector::ZeroVector;
	leaders[leader_index]->target = leaders[leader_index]->GetActorLocation();
	leaders[leader_index]->Reached = true;
	leaders[leader_index]->CanMove = false;
	if ((followers.Num() > leader_index) && (leader_index >= 0))
	{
		if(followers[leader_index].values.Num() == 0)
		{
			ResetData(leader_index);
			return;
		}

		for (int i = 0; i < followers[leader_index].values.Num(); i++)
		{
			
			new_target.X = leaders[leader_index]->GetActorLocation().X + formation_position[i].X;
			new_target.Y = leaders[leader_index]->GetActorLocation().Y + formation_position[i].Y;

			followers[leader_index].values[i]->target = new_target;
			followers[leader_index].values[i]->CanMove = true;
		}
		int farther = GetIndexFartherFollower(leader_index);
		if (farther >= 0)
		{
			leaders[leader_index]->target = followers[leader_index].values[farther]->GetActorLocation();
			leaders[leader_index]->Reached = false;
			leaders[leader_index]->CanMove = true;
		}
	}
	else 
	{
		ResetData(leader_index);
		return;
	}

	if (CheckSquadReached(leader_index, true))
	{
		if (CHECK_State(leader_index, AVG_Life(leader_index), AVG_Speed(leader_index), NUM_Soldiers(leader_index)))
		{
			if (train)
			{
				main_nn->RL_SaveData(leaders[leader_index]->RL_data_index[0][0], leaders[leader_index]->RL_data_index[0][1]);
				wait_nn->RL_SaveData(leaders[leader_index]->RL_data_index[2][0], leaders[leader_index]->RL_data_index[2][1]);
				ResetData(leader_index);
			}
		}
		else
		{
			if (train)
			{
				ResetData(leader_index);
			}
		}
	}
}

void ALord::AttackTower(int leader_index)
{
	leaders[leader_index]->AttackTower(leader_index);
	if ((followers.Num() > leader_index) && (leader_index >= 0))
	{
		for (int i = 0; i < followers[leader_index].values.Num(); i++)
		{
			followers[leader_index].values[i]->AttackTower(leader_index);
		}
		if ((followers.Num() > 5) && (!CHECK_EnemiesNearLord()))
		{
			if (FMath::RandRange(0, 100) < 20)
			{
				if (train)
				{
					main_nn->RL_SaveData(leaders[leader_index]->RL_data_index[0][0], leaders[leader_index]->RL_data_index[0][1]);
					attack_nn->RL_SaveData(leaders[leader_index]->RL_data_index[1][0], leaders[leader_index]->RL_data_index[1][1]);
				}
			}
		}
	}
	if (train)
	{
		ResetData(leader_index);
	}
}

void ALord::AttackSoldier(int leader_index, int enemy_index)
{
	enemy_index--;

	if (enemy_lord->leaders.Num() <= enemy_index)
	{
		if (train)
		{
			ResetData(leader_index);
		}
		return;
	}

	if (enemy_lord->leaders.Num() <= 0)
	{
		if (train)
		{
			ResetData(leader_index);
		}
		return;
	}

	leaders[leader_index]->AttackEnemy(enemy_index);
	if ((followers.Num() > leader_index) && (leader_index >= 0))
	{
		for (int i = 0; i < followers[leader_index].values.Num(); i++)
		{
			followers[leader_index].values[i]->AttackEnemy(enemy_index);
		}
	}
	//&& FVector::Dist(leaders[leader_index]->GetActorLocation(), GetActorLocation()) < 1000.0f
	//if (CHECK_EnemySquadDead(enemy_index) )
	//{
	if (enemy_lord->leaders.Num() > leader_index && leader_index > 0) {
		//if (leaders[leader_index]->life >= enemy_lord->leaders[enemy_index]->life)
		//{
			//if (CHECK_State(leader_index, -1, -1, -1, life))
			//{
		if (train)
		{
			main_nn->RL_SaveData(leaders[leader_index]->RL_data_index[0][0], leaders[leader_index]->RL_data_index[0][1]);
			attack_nn->RL_SaveData(leaders[leader_index]->RL_data_index[1][0], leaders[leader_index]->RL_data_index[1][1]);
			//}

			ResetData(leader_index);
		}
		//}
	}
}

int ALord::GetRandomObjective(int leader_index)
{
	TArray<int>life_index;

	if ((enemy_lord->followers.Num() > leader_index) && (leader_index >= 0))
	{
		if (enemy_lord->followers[leader_index].values.Num() > 0)
		{
			for (int i = 0; i < enemy_lord->followers[leader_index].values.Num(); i++)
			{
				if (!enemy_lord->followers[leader_index].values[i]->dead)
				{
					life_index.Add(i);
				}
			}
		}
	}
	if (enemy_lord->leaders.Num() > 0 && leader_index >= 0 && enemy_lord->leaders.Num() > leader_index)
	{
		if (enemy_lord->leaders[leader_index]) 
		{
			if (!enemy_lord->leaders[leader_index]->dead)
			{
				if ((enemy_lord->followers.Num() > leader_index) && (leader_index >= 0))
				{
					if (enemy_lord->followers.Num() > 0)
					{
						life_index.Add(enemy_lord->followers[leader_index].values.Num());
					}
				}
			}
		}
	}

	if (life_index.Num() > 0)
	{
		return life_index[FMath::RandRange(0, life_index.Num()-1)];
	}

	return -1;
}

void ALord::ReceiveDamage(float damage)
{
	//if (particle_hit)
	//{
	//	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), particle_hit, GetActorLocation(), FRotator::ZeroRotator, true);
	//}
	if (particle_hit)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), particle_hit, GetActorLocation(), FRotator::ZeroRotator, true);
	}
	life -= (damage / 100.0f);
	if (life <= 0.0f)
	{
		life = 0.0f;

		RL_SaveResults();
		enemy_lord->RL_SaveResults();

		UGameplayStatics::OpenLevel(GetWorld(), "Lvl1");
	}
}

void ALord::RL_SendData(int leader_index, int action, int data_index, UNNL_Component* nn_component, FDataArray& input, FDataArray& output)
{
	if (train)
	{
		if (leaders[leader_index]->RL_data_index[data_index][0] == -1 &&
			leaders[leader_index]->RL_data_index[data_index][1] == -1)
		{
			leaders[leader_index]->RL_data_index[data_index] = nn_component->RL_AddData(input, output);
		}

		if (leaders[leader_index]->action_index.Num() < 2)
		{
			leaders[leader_index]->action_index.Add(action);

			if (leaders[leader_index]->action_index.Num() == 2)
			{
				if (leaders[leader_index]->info_data.Num() > 0)
				{
					leaders[leader_index]->info_data.Empty();
				}

				leaders[leader_index]->info_data.Add(AVG_Life(leader_index));
				leaders[leader_index]->info_data.Add(AVG_Speed(leader_index));
				leaders[leader_index]->info_data.Add(NUM_Soldiers(leader_index));
				leaders[leader_index]->info_data.Add(this->life);
			}
		}
	}

}

void ALord::RemoveDecision(int leader_index, int index_data_1, int index_data_2, UNNL_Component* nn_1, UNNL_Component* nn_2)
{
	if (leaders[leader_index]->action_index.Num() > 1) {
		if (nn_1)
		{
			leaders[leader_index]->action_index.Empty();

			nn_1->RL_RemoveData(leaders[leader_index]->RL_data_index[index_data_1][0], leaders[leader_index]->RL_data_index[index_data_1][1]);

		}
		else 
		{
		}

		if (nn_2 && (index_data_2 >= 0))
		{
			nn_2->RL_RemoveData(leaders[leader_index]->RL_data_index[index_data_2][0], leaders[leader_index]->RL_data_index[index_data_2][1]);
		}
		else 
		{
		}
	}

}

int ALord::GetActionIndex(int leader_index, int index)
{
	if (leaders[leader_index]->action_index.Num() > 0)
	{
		if (leaders[leader_index]->action_index.Num() > index) 
		{
			return leaders[leader_index]->action_index[index];
		}
	}
	return -1;
}

void ALord::ResetData(int leader_index)
{
	if (train)
	{
		if ((leaders.Num() > leader_index) && (leader_index >= 0))
		{
			for (int i = 0; i < leaders[leader_index]->RL_data_index.Num(); i++)
			{
				for (int j = 0; j < leaders[leader_index]->RL_data_index[i].Num(); j++)
				{
					leaders[leader_index]->RL_data_index[i][j] = -1;
				}
			}
			leaders[leader_index]->action_index.Empty();
		}
	}
}

bool ALord::CheckSquadReached(int leader_index, bool useDistances)
{

	if (!useDistances)
	{
		if ((followers.Num() > leader_index) && (leader_index >= 0))
		{
			for (int i = 0; i < followers[leader_index].values.Num(); i++)
			{

				if (!followers[leader_index].values[i]->Reached) return false;

			}
		}
		return true;
	}

	if (FVector::Dist(leaders[leader_index]->GetActorLocation(), leaders[leader_index]->target) > leaders[leader_index]->distance_offset)
	{
	}
	if ((followers.Num() > leader_index) && (leader_index >= 0))
	{
		for (int i = 0; i < followers[leader_index].values.Num(); i++)
		{
			if (FVector::Dist(followers[leader_index].values[i]->GetActorLocation(), followers[leader_index].values[i]->target) > 100.0f)
			{
				return false;
			}
		}
	}

	return true;
}

bool ALord::CheckSquadReachedDist(int leader_index, bool useDistances)
{

	if (!useDistances)
	{
		if ((followers.Num() > leader_index) && (leader_index >= 0))
		{
			for (int i = 0; i < followers[leader_index].values.Num(); i++)
			{
				if (FVector::Dist(followers[leader_index].values[i]->GetActorLocation(), leaders[leader_index]->GetActorLocation()) < dist_squad_reach)
				{
					if (!followers[leader_index].values[i]->Reached)
					{
						return false;
					}
				}
			}
		}
		return true;
	}

	if (FVector::Dist(leaders[leader_index]->GetActorLocation(), leaders[leader_index]->target) > leaders[leader_index]->distance_offset)
	{
	}
	if ((followers.Num() > leader_index) && (leader_index >= 0))
	{
		for (int i = 0; i < followers[leader_index].values.Num(); i++)
		{
			if (FVector::Dist(followers[leader_index].values[i]->GetActorLocation(), leaders[leader_index]->GetActorLocation()) < dist_squad_reach)
			{
				if (FVector::Dist(followers[leader_index].values[i]->GetActorLocation(), followers[leader_index].values[i]->target) > 5.0f)
				{
					return false;
				}
			}
		}
	}

	return true;
}

int ALord::GetIndexFartherFollower(int leader_index)
{
	float f_distance = 0.0f;
	float distance = 0.0f;
	int r_index = -1;

	if ((followers.Num() > leader_index) && (leader_index >= 0))
	{
		if ((leaders.Num() > leader_index) && (leader_index >= 0))
		{
			if (followers[leader_index].values.Num() > 0)
			{
				distance = FVector::Dist(leaders[leader_index]->GetActorLocation(), followers[leader_index].values[0]->GetActorLocation());
				for (int i = 0; i < followers[leader_index].values.Num(); i++)
				{
					f_distance = FVector::Dist(
						leaders[leader_index]->GetActorLocation(),
						followers[leader_index].values[i]->GetActorLocation()
					);

					if (distance <= f_distance)
					{
						distance = f_distance;
						r_index = i;
					}
				}
			}
		}
	}
	if (distance > 500.0f)
	{
		return r_index;
	}
	else 
	{
		return -1;
	}
}

void ALord::ResetCdTowerReward(int leader_index)
{
	
	if ((leaders.Num() > leader_index) && (leader_index >= 0))
	{
		
		if (leaders.Find(leaders[leader_index]) != INDEX_NONE)
		{
			//leaders[leader_index]->CD_atReward = false;
			if (train)
			{
				main_nn->RL_SaveData(leaders[leader_index]->RL_data_index[0][0], leaders[leader_index]->RL_data_index[0][1]);
				attack_nn->RL_SaveData(leaders[leader_index]->RL_data_index[1][0], leaders[leader_index]->RL_data_index[1][1]);
			}
		}
	}
	ResetData(leader_index);

}

void ALord::SpawnSystem()
{
	FTimerHandle CDTimer;
	CD_spawnSoldier_Timer -= 0.4f;
	if (CD_spawnSoldier_Timer < 2.0f)
	{
		CD_spawnSoldier_Timer = 2.0f;
	}
	FTimerDelegate CDDelegate = FTimerDelegate::CreateUObject(this, &ALord::SpawnSystem);
	GetWorldTimerManager().SetTimer(CDTimer, CDDelegate, CD_spawnSoldier_Timer, false);
	
	if (free_soldiers.Num() < 5)
	{
		CreateSoldier();
	}
}

void ALord::Recruit()
{
	FString msg = FString::SanitizeFloat(leaders.Num());
	if (free_soldiers.Num() > 2 && leaders.Num() < 20) {
		AddLeader(free_soldiers.Pop(true));
	}
}

void ALord::AddLeader(ASoldier* new_leader)
{
	if (new_leader)
	{
		new_leader->leader = true;
		nldr.Add(std::move(new_leader));
	}
}

void ALord::LeadersWololo()
{
	if (!CD_Wololo)
	{

		if ((leaders.Num() < 20) && (free_soldiers.Num() > 0))
		{
			FTimerHandle CDTimer;
			FTimerDelegate CDDelegate = FTimerDelegate::CreateUObject(this, &ALord::ResetWololo);
			GetWorldTimerManager().SetTimer(CDTimer, CDDelegate, CD_Wololo_Timer, false);
			AddLeader(free_soldiers.Pop(true));
			CD_Wololo = true;
		}
	}
}

void ALord::SetNull(ASoldier* sld)
{
	sld = nullptr;
}

void ALord::AnswerRequest()
{
	if (leaders.Num() > 0)
	{
		if (free_soldiers.Num() > 0)
		{
			if (increase_group_arr.Num() > 0)
			{
				FSoldierDataArray new_follower;
				int leader_index = increase_group_arr.Pop(true);
				if ((followers.Num() > leader_index) && (leader_index >= 0))
				{
					if (followers[leader_index].values.Num() < 10)
					{
						followers[leader_index].values.Add(std::move(free_soldiers.Pop(true)));
					}
				}
				else
				{
					new_follower.values.Add(std::move(free_soldiers.Pop(true)));
					followers.Add(std::move(new_follower));
					new_follower.values.Empty();
				}
			}
		}
	}
}

bool ALord::CompareValues(double val1, double val2)
{
	if (val2 >= 0)
	{
		if (val1 < val2) return false;
	}

	return true;
}

void ALord::RL_SaveResults()
{
	//bool a = main_nn->RL_WriteToFile(dt_main_weights);
	//if (a)
	//{
	//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, "MainNN File Writed");
	//}
	//
	//a = attack_nn->RL_WriteToFile(dt_attack_weights);
	//if (a)
	//{
	//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, "AttackNN File Writed");
	//}
	//
	//a = wait_nn->RL_WriteToFile(dt_wait_weights);
	//if (a)
	//{
	//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, "WaitNN File Writed");
	//}
}

int ALord::GetNodeActivated(TArray<double> results)
{
	for (int i = 0; i < results.Num(); i++)
	{
		float rnd = FMath::RandRange(0, 100);
		if (rnd <= (results[i] * 100))
		{
			return i;
		}
	}
	return FMath::RandRange(0, results.Num()-1);
	
}

void ALord::SYS_ResourceManager()
{

			bool isRandom = false;
			float request = 0.0f;
			if (request_slist.Num() > 0)
			{
				request = 1.0f;
			}

			FDataArray resources_nn_data = resources_nn->CData(
				8,
				NUM_Resources(), NUM_TotalSoldiers(), NUM_TotalWorkers(),
				enemy_lord->NUM_TotalSoldiers(), enemy_lord->NUM_TotalWorkers(),
				life, enemy_lord->life, request
			);

			int action = -1;

			switch (train)
			{
			case true:
				nn_result = resources_nn->FeedForwardOutput(resources_nn_data, false);
				isRandom = true;
				action = GetNodeActivated(nn_result);
				break;
			case false:

				nn_result = resources_nn->FeedForwardOutput(resources_nn_data, false);
				action = GetNodeActivated(nn_result);

				break;
			}

			//	action = 0;
			FDataArray output;
			switch (action)
			{
			case 0:
				if (isRandom) {
				
				}	
				if (resources >= 5.0f && (NUM_TotalSoldiers() < (200.0f/1000.0f)) &&
					(free_soldiers.Num() < 20))
				{
				
					float ctr = 0.0f;
				

					if (request > 0.0f)
					{
						ctr += 0.2;
					}

					if (NUM_TotalSoldiers() > enemy_lord->NUM_TotalSoldiers())
					{
				
						ctr += 0.2;
					}

					if (life < enemy_lord->life)
					{
						ctr += 0.4;
					}

					if (resources > enemy_lord->resources)
					{
						ctr += 0.2;
					}
					CreateSoldier();
					output = resources_nn->CData(3, ctr, 0.0, 0.0);
					resources_nn->RL_BackPropagation(resources_nn_data, output);
				}
				break;
			case 1:
				if (isRandom) {

				}
				if (resources > 3.0f && (workers.Num() < MAX_workers))
				{
					float ctr = 0.0f;
				
					if (NUM_TotalWorkers() > enemy_lord->NUM_TotalWorkers())
					{
						ctr += 0.4;
					}

					if (life > enemy_lord->life)
					{
						ctr += 0.2;
					}

					if (resources > enemy_lord->resources)
					{
						ctr += 0.4;
					}
					if (NUM_TotalWorkers() <= NUM_TotalSoldiers())
					{
						CreateWorker();
						output = resources_nn->CData(3, 0.0, ctr, 0.0);
						resources_nn->RL_BackPropagation(resources_nn_data, output);
					}
				}
				break;
			case 2:
				if (isRandom) {
				}
				float ctr = 0.0f;
				if (life > enemy_lord->life)
				{
					if (NUM_TotalSoldiers() > enemy_lord->NUM_TotalSoldiers())
					{
						ctr += 0.2;
					}

					if (NUM_TotalWorkers() > enemy_lord->NUM_TotalWorkers())
					{
						ctr += 0.2;
					}

					if (life > enemy_lord->life)
					{
						ctr += 0.3;
					}

					if (resources > enemy_lord->resources)
					{
						ctr += 0.3;
					}

					output = resources_nn->CData(3, 0.0, 0.0, ctr);
					resources_nn->RL_BackPropagation(resources_nn_data, output);
				}
				break;
			}
}

void ALord::ResetWololo()
{
	CD_Wololo = false;
}

bool ALord::CHECK_EnemiesNearLord()
{

	for (int i = 0; i < enemy_lord->leaders.Num(); i++)
	{
		if (FVector::Dist(enemy_lord->leaders[i]->GetActorLocation(), GetActorLocation()) < 500.0f)
		{
			return true;
		}
	}

	return false;
}