// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstance.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "NNL/NNL_Component.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Soldier.generated.h"



UCLASS()
class NNL_RTS_API ASoldier : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASoldier();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
		UBehaviorTree* BehTree = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
		class ARSTAIController* AIContrn = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Lord")
  class ALord* lord = nullptr;

  UPROPERTY()
  class ALord* enemy_lord = nullptr;

  UPROPERTY()
    UNNL_Component* neural_network = nullptr;

  UPROPERTY()
  	TArray<int> action_index;
  

		TArray<TArray<int>> RL_data_index = { {-1, -1},
																					{-1, -1},
																					{-1, -1}};
  
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
  	bool CanMove = false;
  
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
  	bool Reached = false;
  
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
	FVector target = FVector(-1.0, -1.0, -1.0);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
	ASoldier* enemy_target = nullptr;

  UPROPERTY()
    ASoldier* leader_to_help = nullptr;

  UPROPERTY()
    bool escape = false;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
    float speed = 0.3f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
    float life = 1.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
    float damage = 0.2f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
		bool dead = false;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
    float radius_detection = 1000.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
    float distance_offset = 150.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
    float attack_speed = 1.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
    bool leader = false;

  UPROPERTY()
  TArray<double> nn_result;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
    UStaticMeshComponent* static_mesh = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Soldier")
    FVector offset_position = FVector::ZeroVector;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Particles")
    UParticleSystem* particle_hit = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Particles")
    UParticleSystem* particle_attack = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Materials")
    TArray<UMaterialInstance*> actions_materials;

	UPROPERTY()
	TArray<double> info_data;

	UPROPERTY()
	  float enemy_tower_life = 0.0f;

	UPROPERTY()
		bool CD_atReward = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool AnimAttack = false;

	UPROPERTY()
		int fAction = -1;

	UPROPERTY()
		int sAction = -1;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY()
  double do_damage_counter = 0.0f;

	UPROPERTY()
  bool cd = false;

	UPROPERTY()
  bool inmune = false;

	UPROPERTY()
  FVector normal_scale = FVector::ZeroVector;


public:	
	UPROPERTY()
	int AE_Step = 0;

	UPROPERTY()
	bool fighting = false;

	UPROPERTY()
  double d_time = 0.0f;

	UFUNCTION()
	virtual void BeginDestroy() override;

  UFUNCTION()
  void OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

  UFUNCTION()
    void OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

  UFUNCTION()
  void AttackOnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void AttackTower(int index);

  UFUNCTION()
    void AttackEnemy(int index);

  UFUNCTION()
    void Move(FVector position_to_move);

  UFUNCTION()
    void ReceiveDamage(float damage_received);

  UFUNCTION()
    void DoDamage(ASoldier* enemy);

	UFUNCTION()
		void DoDamageToTower(ALord* enemy);

  UFUNCTION()
    void UpdateMaterial(int action_id);

  UFUNCTION()
    int GetId();

  UFUNCTION()
    void ResetCD();

  UFUNCTION()
    void ReturnToFight();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

  UFUNCTION()
    FDataArray CreateExample(int value1, int value2, int value3);

  UFUNCTION()
    FDataArray CreateExample4(float value1, float value2, float value3, float value4);
	
  UFUNCTION()
    int GetResultGreatestValue();

	UFUNCTION()
	ASoldier* GetRandomObjective(int leader_index);

  UFUNCTION()
    ASoldier* GetClosestEnemy();

	UFUNCTION()
		void SetMaterial(UMaterialInstance* material);


};
