// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Engine/Classes/Components/TextRenderComponent.h"
#include "GameFramework/Actor.h"
#include "NNL/NNL_Component.h"
#include "NNL/FileData.h"
#include "Worker.h"
#include "Soldier.h"
#include "NNL_RTS/Core/Resources/Resource.h"

#include "Lord.generated.h"

USTRUCT()
struct FSoldierDataArray
{
	GENERATED_BODY()

public: 

		TArray<ASoldier*>values;
};

UCLASS()
class NNL_RTS_API ALord : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALord();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		UMaterialInstance* faction_material = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
	UDataTable* dt_main_weights = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		UDataTable* dt_attack_weights = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		UDataTable* dt_wait_weights = nullptr;
  //UPROPERTY()
  //  UNNL_Component* neural_network = nullptr;


  // 0 - Media Life (AVG)
  // 1 - Media Speed
  // 2 - Num Soldiers

  // 3 - Enemy Media Life
  // 4 - Enemy Media Speed
  // 5 - Enemy Num Soldiers

  // 0 - Attack
  // 1 - Move

  UPROPERTY()
    UNNL_Component* nn_soldiers = nullptr;

  // 0 - Life
  // 1 - Speed
  // 2 - Enemy Life
  // 3 - Enemy Speed
  // 4 - Resource in range

  // 0 - Work
  // 1 - Move

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
    UNNL_Component* nn_workers = nullptr;

	//#######################################
	//				MAIN NEURAL NETWORK
	//#######################################

	// 0 - AVG_Life
	// 1 - AVG_Speed
	// 2 - NUM_Soldiers
	// 8 - NUM_TotalSoldiers
	// 3 - NUM_AlliesNear
	// 4 - NUM_EnemySoldiers
	// 5 - TowerLife
	// 6 - EnemyTowerLife
	// 7 - NearTower

	// 0 - Attack#
	// 1 - Move
	// 2 - Wait#

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		UNNL_Component* main_nn = nullptr;

	//#######################################
	//				ATTACK NEURAL NETWORK
	//#######################################

	// 0 - EnemyTowerLife
	// 1 - TowerLife
	// 2 - NUM_SoldierNear
	// 3 - Enemies x 50
	//	 - AVG_EnemyLife
	//	 - AVG_Speed
	//	 - NUM_EnemySoldiers
	//	 - EnemiesNear
	//	 - NearTower

	// 0 - AttackTower
	// 1 - AttackEnemie x 50
	// x
	// 50

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		UNNL_Component* attack_nn = nullptr;

	//#######################################
	//				WAIT NEURAL NETWORK
	//#######################################

	// 0 - AVG_Life
	// 1 - AVG_Speed
	// 2 - NUM_Soldiers
	// 3 - NUM_AlliesNear
	// 4 - NUM_SoldiersFree
	// 5 - Resources

	// 0 - NONE
	// 1 - Increase Group
	// 2 - Request Soldier
	// 3 - Update Formation

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		UNNL_Component* wait_nn = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		UNNL_Component* resources_nn = nullptr;

	UPROPERTY()
		UFileData* test_data = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<ASoldier> SoldierType = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<AWorker> WorkerType = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Time_Update = 0.2f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool CD_Decision_Update = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool CD_Wololo = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float CD_Wololo_Timer = 1.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float radioproximity = 1000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float soldiers_rp = 500.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float resources = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		bool useNNLFiles = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		bool EmptyAllData = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float life = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float damage_reduction = 100.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
    int MaxSoldiersInPatrol = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float dist_squad_reach = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		bool train = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		int MAX_workers = 20.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
    TArray<AWorker*> workers;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<AResource*> Resourses;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<ASoldier*> request_help_list;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
    TArray<FVector> formation_position;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		TArray<FVector> attackT_position;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		TArray<ASoldier*> nldr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TMap<ASoldier*, int> request_slist;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UStaticMeshComponent* LordMesh = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    USceneComponent* SpawnPoint = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UTextRenderComponent* ResourcesText = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
	  float max_width = 600.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
	  float max_height = 600.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float formation_radius = 200.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int id = 0;

  UPROPERTY()
    ALord* enemy_lord = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
  TArray<ASoldier*> leaders;

 // TArray<TArray<ASoldier*>> followers;
	UPROPERTY(VisibleAnywhere)
	TArray<FSoldierDataArray> followers;

//	UPROPERTY()
	//TArray<ASoldier*> kills;
  // Inputs: Resources, numOfSoliders, numOfWorkers
  //Actions: Create soldier, Create Farmer, Wait

	UPROPERTY()
		float CD_atReward_Timer = 0.5f;

	UPROPERTY()
		bool CD_spawnSoldier = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float CD_spawnSoldier_Timer = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Particles")
		UParticleSystem* particle_hit = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool loader = true;

	UPROPERTY()
	float counter_soldiers = 0;

	UPROPERTY()
  float counterTime = 0.0f;

	UPROPERTY()
  TArray<double> nn_result;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
	TArray<ASoldier*> free_soldiers;

	UPROPERTY()
	TArray<int> increase_group_arr;



public:	

  UFUNCTION()
    void CreateSoldier();

  UFUNCTION()
    void CreateWorker();

  UFUNCTION()
    void SYS_Farm();

  UFUNCTION()
    void SYS_Farm_Work(AWorker* worker);

  UFUNCTION()
    void SYS_Farm_Move(AWorker* worker);

  UFUNCTION()
    void SYS_Attack();

  UFUNCTION()
    void SYS_Attack_Attack(int index);

  UFUNCTION()
    void SYS_Attack_Move(int index);

	UFUNCTION()
		void SYS_Attack_Wait(int index);

  UFUNCTION()
    AResource* GetResource();

  UFUNCTION()
    void ChangeLeader(ASoldier* old, ASoldier* new_leader);

  UFUNCTION()
    ASoldier* GetNearestRequest(ASoldier* leader_ignore);


	UFUNCTION()
		void Update();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

  UFUNCTION()
    int GetResultGreatestValue();

	UFUNCTION()
	int GreatestValueFromArray(TArray<double>& data);

	UFUNCTION()
		void KillSoldiers();

	UFUNCTION()
		float AVG_Life(int leader_index);

	UFUNCTION()
		float AVG_Speed(int leader_index);

	UFUNCTION()
		float NUM_Soldiers(int leader_index);

	UFUNCTION()
		float NUM_TotalSoldiers();

	UFUNCTION()
		float NUM_SoldiersNear(int leader_index);

	UFUNCTION()
		float NUM_EnemiesNear(int leader_index);

	UFUNCTION()
		float NUM_AlliesNearEnemy(int leader_index);

	UFUNCTION()
		float NUM_FreeSoldiers();

	UFUNCTION()
		float NUM_Resources();

	UFUNCTION()
		float NUM_TotalWorkers();

	UFUNCTION()
		float CHECK_NearTower(int leader_index);

	UFUNCTION()
		float CHECK_NearTowerEnemy(int leader_index);

	UFUNCTION()
		bool CHECK_State(int leader_index, float al_val = -1.0f, float as_val = -1.0f, float ns_val = -1.0f, float tl_val = -1.0f);

	UFUNCTION()
		bool CHECK_EnemySquadDead(int enemy_leader_index);

	UFUNCTION()
		TArray<int> GetLeadersInRange(FVector position, float radius);

	UFUNCTION()
		void ResetCDecision();

	UFUNCTION()
		void IncreaseGroup(int leader_index);

	UFUNCTION()
		void RequestSoldier(int leader_index);
	
	UFUNCTION()
		void UpdateFormation(int leader_index);

	UFUNCTION()
		void AttackTower(int leader_index);

	UFUNCTION()
		void AttackSoldier(int leader_index, int enemy_index);

	UFUNCTION()
		int GetRandomObjective(int leader_index);

	UFUNCTION()
		void ReceiveDamage(float damage);

	UFUNCTION()
		void RL_SendData(int leader_index, int action, int data_index, UNNL_Component* nn_component, FDataArray& input, FDataArray& output);

	// 0 - main_nn
// 1 - attack_nn
// 2 - wait_nn
//  UPROPERTY()
	/*TArray<TArray<int>> RL_data_index = { {-1, -1},
																				{-1, -1},
																				{-1, -1} };*/
	UFUNCTION()
		void RemoveDecision(int leader_index, int index_data_1, int index_data_2 = -1, UNNL_Component* nn_1 = nullptr, UNNL_Component* nn_2 = nullptr);

	UFUNCTION()
		int GetActionIndex(int leader_index, int index);

	UFUNCTION()
		void ResetData(int leader_index);

	UFUNCTION()
		bool CheckSquadReached(int leader_index, bool useDistances = false);

	UFUNCTION()
		bool CheckSquadReachedDist(int leader_index, bool useDistances = false);

	UFUNCTION()
		int GetIndexFartherFollower(int leader_index);

	UFUNCTION()
		void ResetCdTowerReward(int leader_index);

	UFUNCTION()
		void SpawnSystem();

	UFUNCTION()
		void Recruit();

	UFUNCTION()
		void AddLeader(ASoldier* new_leader);

	UFUNCTION()
		void LeadersWololo();

	UFUNCTION()
		void SetNull(ASoldier* sld);

	UFUNCTION()
		void AnswerRequest();

	UFUNCTION()
		bool CompareValues(double val1, double val2);

	UFUNCTION()
		void RL_SaveResults();

	UFUNCTION()
		int GetNodeActivated(TArray<double> results);

	UFUNCTION()
		void SYS_ResourceManager();

	UFUNCTION()
		void ResetWololo();

	UFUNCTION()
		void UpdateDecision();

	UFUNCTION()
		void UpdateAttackDecision(int leader_index);

	UFUNCTION()
		void UpdateWaitDecision(int leader_index);

	UFUNCTION()
		bool CHECK_EnemiesNearLord();
};
