// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "NNL_RTS/Core/Characters/Soldier.h"
#include "NNL_RTS/Core/Resources/Resource.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstance.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "BehaviorTree/BehaviorTree.h"
#include "NNL/NNL_Component.h"
#include "Worker.generated.h"

UCLASS()
class NNL_RTS_API AWorker : public ACharacter
{
	GENERATED_BODY()

public:
  
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
		UBehaviorTree* BehTree = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
		class ARSTAIController* AIContrn = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
		bool CanMove = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
		bool Reached = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
		FVector target = FVector(-1.0, -1.0, -1.0);

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Lord")
    class ALord* lord = nullptr;

  UPROPERTY()
    UNNL_Component* neural_network = nullptr;

  UPROPERTY()
    TArray<ASoldier*> enemy_soldier_in_range;

  UPROPERTY()
    AResource* resource_target = nullptr;

  UPROPERTY()
    ASoldier* soldier_target = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
    float speed = 0.3f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
    float life = 1.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
    float damage = 0.2f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
    float radius_detection = 500.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
    float distance_offset = 150.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
    float attack_speed = 1.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
    float q_extraction = 2.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
    float resource_load = 0.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
    float MAX_resource_load = 10.0f;

  UPROPERTY()
    TArray<double> nn_result;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
    UBoxComponent* attack_box = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Worker")
    UStaticMeshComponent* static_mesh = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Particles")
    UParticleSystem* particle_hit = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data | Materials")
    TArray<UMaterialInstance*> actions_materials;

public:
	// Sets default values for this character's properties
	AWorker();

	UPROPERTY()
  double recollecting_counter = 0.0f;

	UPROPERTY()
  double d_time = 0.0f;

	UPROPERTY()
  bool cd = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
  
  UFUNCTION()
    void OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

  UFUNCTION()
    void OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

  UFUNCTION()
    void AttackOnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

  UFUNCTION()
    void Mind();

  UFUNCTION()
    void Action();

  UFUNCTION()
    void Work();

  UFUNCTION()
    void Flee();

  UFUNCTION()
    void Move(FVector position_to_move);

  UFUNCTION()
    void ReceiveDamage(float damage_received);

  UFUNCTION()
    void DoWork();

  UFUNCTION()
    void UpdateMaterial(int action_id);

  UFUNCTION()
    void CheckEnemiesInRadius();

  UFUNCTION()
    int GetId();

  UFUNCTION()
    void ResetCD();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

  UFUNCTION()
    FDataArray CreateExample(int value1, int value2, int value3);

  UFUNCTION()
    FDataArray CreateExample5(float value1, float value2, float value3, float value4, float value5);

  UFUNCTION()
    int GetResultGreatestValue();

  UFUNCTION()
    ASoldier* GetClosestEnemy();

	UFUNCTION()
		void SetMaterial(UMaterialInstance* material);


};
