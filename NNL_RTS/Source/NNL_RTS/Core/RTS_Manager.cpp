// Fill out your copyright notice in the Description page of Project Settings.

#include "RTS_Manager.h"


ARTS_Manager* ARTS_Manager::Instance = nullptr;

// Sets default values
ARTS_Manager::ARTS_Manager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Instance = this;
}

// Called when the game starts or when spawned
void ARTS_Manager::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ARTS_Manager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	for (int i = 0; i < lords.Num(); i++)
	{
		for (auto lord : lords)
		{
			lord->Update();
			lord->KillSoldiers();
		}
	}
}

ARTS_Manager* ARTS_Manager::GetInstance()
{
	return Instance;
}

void ARTS_Manager::AddTarget(ASoldier* attacker, ASoldier* enemy)
{
	if (!attacker || !enemy) return;

	if (!targets_map.Find(attacker))
	{
		targets_map.Add(attacker, enemy);
	}
	else {
		targets_map.Remove(attacker);
		targets_map.Add(attacker, enemy);
	}
}

void ARTS_Manager::DeleteTarget(ASoldier* sld)
{
	targets_map.Remove(sld);
	//TMap<ASoldier*, ASoldier*> aux;
	TArray<ASoldier*> delete_list;
	for (auto& element : targets_map)
	{
		if (element.Value == sld)
		{
			delete_list.Add(element.Key);
		}
	}

	for (int i = 0; i < delete_list.Num(); i++)
	{
		targets_map.Remove(delete_list[i]);
	}
}

ASoldier* ARTS_Manager::GetTarget(ASoldier* attacker)
{
	//ASoldier* aux = *targets_map.Find(attacker);
	//
	//if (aux) return aux;
	//
	//if (!aux) return nullptr;
	for (auto& element : targets_map)
	{
		if (element.Key == attacker)
		{
			return element.Value;
		}
	}

	return nullptr;
}