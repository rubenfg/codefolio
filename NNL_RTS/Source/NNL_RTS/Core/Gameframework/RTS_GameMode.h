// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "RTS_GameMode.generated.h"

/**
 * 
 */
UCLASS()
class NNL_RTS_API ARTS_GameMode : public AGameMode
{
	GENERATED_BODY()
	
		APawn* SpawnDefaultPawnFor(AController* NewPlayer, AActor* StartSpot);
	
	
};
