// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "NNL_RTS/Core/Characters/Lord.h"
#include "RTS_Manager.generated.h"

UCLASS()
class NNL_RTS_API ARTS_Manager : public AActor
{
	GENERATED_BODY()
public:

		static ARTS_Manager* Instance;

public:	
	// Sets default values for this actor's properties
	ARTS_Manager();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		TArray<ALord*> lords;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		TMap<ASoldier*, ASoldier*> targets_map;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	static ARTS_Manager* GetInstance();

	UFUNCTION()
		void AddTarget(ASoldier* attacker, ASoldier* enemy);

	UFUNCTION()
		void DeleteTarget(ASoldier* sld);

	UFUNCTION()
		ASoldier* GetTarget(ASoldier* attacker);
};
