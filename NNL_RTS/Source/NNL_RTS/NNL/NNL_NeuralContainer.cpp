// Fill out your copyright notice in the Description page of Project Settings.

#include "NNL_NeuralContainer.h"


// Sets default values
ANNL_NeuralContainer::ANNL_NeuralContainer()
{
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;

  neural_network = CreateDefaultSubobject<UNNL_Component>(TEXT("NeuralComponent"));

  //for (int i = 0; i < 9; i++) 
  //{
  //  grid_nodes.Add(CreateDefaultSubobject<USceneComponent>(TEXT("Grid" + i)));
  //}
}

// Called when the game starts or when spawned
void ANNL_NeuralContainer::BeginPlay()
{
  Super::BeginPlay();
  TArray<int> layers = { 9, 81, 81, 81, 9 };
  TArray<FDataArray> trainer1;// = { { 0.0 }, { 0.5 }, { 1.0 } };
  FDataArray ex1;
  ex1.values.Add(0); ex1.values.Add(0); ex1.values.Add(0);
  ex1.values.Add(0); ex1.values.Add(1); ex1.values.Add(0);
  ex1.values.Add(0); ex1.values.Add(0); ex1.values.Add(0);

  FDataArray ex2;
  ex2.values.Add(0); ex2.values.Add(0); ex2.values.Add(0);
  ex2.values.Add(1); ex2.values.Add(1); ex2.values.Add(1);
  ex2.values.Add(0); ex2.values.Add(0); ex2.values.Add(0);

  FDataArray ex3;
  ex3.values.Add(1); ex3.values.Add(0); ex3.values.Add(1);
  ex3.values.Add(1); ex3.values.Add(1); ex3.values.Add(1);
  ex3.values.Add(0); ex3.values.Add(0); ex3.values.Add(0);

  trainer1.Add(ex1);  trainer1.Add(ex2);  trainer1.Add(ex3);

  TArray<FDataArray> trainer2; //= { { 0.5f}, {1.0f}, {0.5f} };
  FDataArray ej1;
  ej1.values.Add(0); ej1.values.Add(0); ej1.values.Add(0);
  ej1.values.Add(0); ej1.values.Add(1); ej1.values.Add(0);
  ej1.values.Add(0); ej1.values.Add(0); ej1.values.Add(0);

  FDataArray ej2;
  ej2.values.Add(0); ej2.values.Add(0); ej2.values.Add(0);
  ej2.values.Add(1); ej2.values.Add(1); ej2.values.Add(1);
  ej2.values.Add(0); ej2.values.Add(0); ej2.values.Add(0);

  FDataArray ej3;
  ej3.values.Add(1); ej3.values.Add(0); ej3.values.Add(1);
  ej3.values.Add(1); ej3.values.Add(1); ej3.values.Add(1);
  ej3.values.Add(1); ej3.values.Add(1); ej3.values.Add(1);

  trainer2.Add(ej1);  trainer2.Add(ej2);  trainer2.Add(ej3);

  neural_network->Init(layers);

  //neural_network->AddInput(trainer1);
  //neural_network->AddOutput(trainer2);

  neural_network->BackPropagation(trainer1, trainer2);

  FString aa = FString::SanitizeFloat((float)trainer1[0].values[0]);
 // GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, aa);
  //neural_network->FeedForwardOutput(trainer1[0], true);

  FString bb = FString::SanitizeFloat((float)trainer1[1].values[0]);
 // GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, bb);
  //neural_network->FeedForwardOutput(trainer1[1], true);

  FString cc = FString::SanitizeFloat((float)trainer1[2].values[0]);
 // GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, cc);
  //neural_network->FeedForwardOutput(trainer1[2], true);

  FDataArray test;
  test.values.Add(1); test.values.Add(0); test.values.Add(0);
  test.values.Add(0); test.values.Add(1); test.values.Add(0);
  test.values.Add(0); test.values.Add(0); test.values.Add(1);

  TArray<double> result = neural_network->FeedForwardOutput(test, true);


  for (int i = -1; i <= 1; i++)
  {
    for (int j = -1; j <= 1; j++)
    {
      FVector newLocation = {(float)((i * 100) + GetActorLocation().X), (float)((j * 100) +GetActorLocation().Y), 0.0f};
      GridPositions.Add(std::move(newLocation));
    }
  }


  for (int i = 0; i < 9; i++) 
  {
    //ASoldier* soldier = GetWorld()->SpawnActor<ASoldier>(SoldierType, GridPositions[i], FRotator::ZeroRotator);
    
    if (result[i] > 0.5f) 
    {
      Soldiers.Add(std::move(GetWorld()->SpawnActor<ASoldier>(SoldierType, GridPositions[i], FRotator::ZeroRotator)));
    }
  }


}

// Called every frame
void ANNL_NeuralContainer::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);

}

