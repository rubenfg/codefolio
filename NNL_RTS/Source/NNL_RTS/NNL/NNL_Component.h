// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FileData.h"
#include <stdarg.h>
#include "Engine/DataTable.h"
#include "NNL_Component.generated.h"

USTRUCT()
struct FDataArray
{
  GENERATED_BODY()

    TArray<double>values;

		FDataArray() {}

		//bool operator==(const FDataArray& dataArray) const;
		//
		//bool operator<<(const FDataArray& dataArray) const;

	FORCEINLINE FDataArray& operator =(const FDataArray& data)
	{
		values = data.values;
		return *this;
	}
};

FORCEINLINE FArchive& operator<<(FArchive& Ar, FDataArray& dataArr)
{
	Ar << dataArr.values;

	return Ar;
}

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NNL_RTS_API UNNL_Component : public UActorComponent
{
	GENERATED_BODY()

  struct Node
  {
    double input = 0.0f;
    double output = 0.0f;
    double delta = 0.0f;
  };

private:

  UPROPERTY()
    int size = 0;

	UPROPERTY()
		UFileData* file_weights_data = nullptr;

	UPROPERTY()
		UFileData* RL_file_input = nullptr;

	UPROPERTY()
		UFileData* RL_file_output = nullptr;

  TArray<TArray<Node>> neural_network;

  UFUNCTION()
    double sigmoide(double z);

  UFUNCTION()
    double sigmoide_p(double x);

  UFUNCTION()
    double normalize(double v, double min, double max);

  UFUNCTION()
    double denormalize(double v, double min, double max);

public:	

	UPROPERTY()
		TArray<int> NN_layers;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		UDataTable* dt_weights = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		UDataTable* dt_RL_input = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		UDataTable* dt_RL_output = nullptr;

	TArray<TArray<TArray<double>>> weights;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		FString w_filename = "";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		FString RL_input_filename = "";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		FString RL_output_filename = "";

	// Sets default values for this component's properties
	UNNL_Component();
  UNNL_Component(TArray<int>& layers);
	UNNL_Component(int n_args, ...);


  TArray<TArray<double>> nnl_inputs;
  TArray<TArray<double>> nnl_outputs;

	UPROPERTY()
	TArray<FDataArray> RL_data_input;

	UPROPERTY()
	TArray<FDataArray> RL_data_output;

	UPROPERTY()
	TArray<FDataArray> RL_valid_input;

	UPROPERTY()
	TArray<FDataArray> RL_valid_output;

//	UPROPERTY()
	TArray<TArray<double>> RL_valid_input_raw;

//	UPROPERTY()
	TArray<TArray<double>> RL_valid_output_raw;

  UPROPERTY()
    double offset_error = 0.001;

  UPROPERTY()
    int maxIterations = 10000;

  //INFO VARs
  UPROPERTY()
    double final_error = 0;

  UPROPERTY()
    int final_iterations = 0;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	

	FDataArray CData(int n_args, ...);

	UFUNCTION()
	TArray<int> RL_AddData(FDataArray& input_values, FDataArray& output_values);

	UFUNCTION()
		bool RL_LoadFromFile(UDataTable* table);

	UFUNCTION()
		bool RL_WriteToFile(UDataTable* table);

	UFUNCTION()
	int RL_RemoveData(int index_0, int index_1);

	UFUNCTION()
	void RL_SaveData(int index_0, int index_1);

  UFUNCTION()
    void Init(TArray<int> layers);

//	UFUNCTION()
		void Init_(int n_args, ...);

  UFUNCTION()
	bool LoadWeightsData();

  //UFUNCTION(BlueprintCallable, Category = "NNL")
  UFUNCTION()
    void BackPropagation(TArray<FDataArray>& inputs, TArray<FDataArray>& outputs, bool use_file = false);

  //UFUNCTION(BlueprintCallable, Category = "NNL")
  UFUNCTION()
    TArray<double> FeedForwardOutput(FDataArray& input_values, bool printOutput = false);

	UFUNCTION()
		void RL_BackPropagation(FDataArray& input, FDataArray& output);

  //UFUNCTION(BlueprintCallable, Category = "NNL")
  UFUNCTION()
    void AddInput(TArray<double> input, bool clear = false);

  //UFUNCTION(BlueprintCallable, Category = "NNL")
  UFUNCTION()
    void AddOutput(TArray<double> output, bool clear = false);

	UFUNCTION()
		int GetNodeActivated(TArray<double> result);

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
