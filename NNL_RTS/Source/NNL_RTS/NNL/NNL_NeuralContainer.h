  // Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "NNL/NNL_Component.h"
#include "NNL_RTS/Core/Characters/Soldier.h"
#include "NNL_NeuralContainer.generated.h"

UCLASS()
class NNL_RTS_API ANNL_NeuralContainer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ANNL_NeuralContainer();

  UPROPERTY()
    UNNL_Component* neural_network = nullptr;

  //TArray<USceneComponent*> grid_nodes;
  
  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<ASoldier> SoldierType;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
   TArray<ASoldier*> Soldiers;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FVector> GridPositions;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
