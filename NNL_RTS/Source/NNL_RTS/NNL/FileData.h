// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
//#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "Runtime/Core/Public/Misc/FileHelper.h"
#include "Runtime/Core/Public/Serialization/MemoryReader.h"
#include "Runtime/Core/Public/Serialization/BufferArchive.h"
#include "Runtime/Core/Public/Serialization/Archive.h"
#include "Engine/DataTable.h"
#include "FileData.generated.h"

#define SAVEDATAFILENAME "SampleSavedData"


USTRUCT(BlueprintType)
struct FItemTableRow : public FTableRowBase
{
	GENERATED_BODY()

public:
	//UPROPERTY()
		//TArray<TArray<TArray<double>>> weights;

	//	TArray<TArray<double>> data;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Item Basic Info")
		int i;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Item Basic Info")
		int j;

	//UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Item Basic Info")
	//	FString value;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Item Basic Info")
		FString str_value;

	//UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Item Basic Info")
		double value;

	FItemTableRow()
	{
		// Constructor Code...
	}



};

USTRUCT(BlueprintType)
struct FItemTableRowWeights : public FTableRowBase
{
	GENERATED_BODY()

public:
	//UPROPERTY()
		//TArray<TArray<TArray<double>>> weights;

	//	TArray<TArray<double>> data;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Item Basic Info")
		int i;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Item Basic Info")
		int j;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Item Basic Info")
		int k;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Item Basic Info")
		FString str_value;

	//UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Item Basic Info")
	double value;

	FItemTableRowWeights()
	{
		// Constructor Code...
	}



};


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class NNL_RTS_API UFileData : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UFileData();

	//friend FArchive& operator<<(FArchive& Ar, TArray<int>& arr);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


public:

	//FItemTableRowWeights weights_row_ptr;
	//
	//FItemTableRow row_2D_ptr;

	//UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<TArray<TArray<double>>> values;

	TArray<TArray<double>> values2D;

	//UPROPERTY(EditAnywhere)
	//UDataTable* data_table = nullptr;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//template<typename T>
	//bool FORCEINLINE Create(FString filename, T& data)
	//{
	//	data_table =
	//}
	/*
	//UFUNCTION()
	template<typename T>
	void FORCEINLINE SaveLoadData(FArchive& Ar, T& data)
	{
		Ar << data;
	}

	//UFUNCTION()
	template<typename T>
	bool FORCEINLINE SaveData(FString filename, T& data)
	{
		//Save the data to binary
		FBufferArchive ToBinary;
		SaveLoadData(ToBinary, data);

		//No data were saved
		if (ToBinary.Num() <= 0) return false;

		//Save binaries to disk
		bool result = FFileHelper::SaveArrayToFile(ToBinary, (TCHAR*)filename.Chr);

		//Empty the buffer's contents
		ToBinary.FlushCache();
		ToBinary.Empty();

		return result;
	}

	//UFUNCTION()
	template<typename T>
	bool FORCEINLINE LoadData(FString filename, T& data)
	{
		TArray<uint8> BinaryArray;

		//load disk data to binary array
		if (!FFileHelper::LoadFileToArray(BinaryArray, (TCHAR*)filename.Chr)) return false;

		if (BinaryArray.Num() <= 0) return false;

		//Memory reader is the archive that we're going to use in order to read the loaded data
		FMemoryReader FromBinary = FMemoryReader(BinaryArray, true);
		FromBinary.Seek(0);

		SaveLoadData(FromBinary, values2D);

		//Empty the buffer's contents
		FromBinary.FlushCache();
		BinaryArray.Empty();
		//Close the stream
		FromBinary.Close();

		return true;

	}
	*/
	//template<typename T>
	//void FORCEINLINE SaveLoadData(UDataTable& table, T& data)
	//{
	////	table.EmptyTable();
	//}

	bool LoadDataItemTableRowWeights(UDataTable* table, TArray<TArray<TArray<double>>>& data, TArray<int> nn_layers);
	bool SaveDataItemTableRowWeights(UDataTable* table, TArray<TArray<TArray<double>>>& data);

	bool SaveDataItemTableRow(UDataTable* table, TArray<TArray<double>>& data);
	bool LoadDataItemTableRow(UDataTable* table, TArray<TArray<double>>& data);

	//template<typename T>
	//bool FORCEINLINE SaveDataItemTableRowWeights(UDataTable* table, T& data)
/*	{
		//FItemTableRow new_row;
		//new_row.data = data;
		FItemTableRowWeights row;
		int c = 0;
		for (int i = 0; i < data.Num(); i++)
		{
			for (int j = 0; j < data[i].Num(); j++)
			{
				for (int k = 0; k < data[i][j].Num(); k++)
				{
					row.i = i;
					row.j = j;
					row.k = k;
					row.value = FString::SanitizeFloat(data[i][j][k]);


					FString row_str;
					row_str.AppendInt(c);
					//row_str.AppendInt(j);
					//row_str.AppendInt(k);
					//row_str.AppendInt(i);
					table->AddRow(FName(*row_str), row);
					c++;
				}
			}
		}
		return true;
	}*/

	
	//template<typename T>
	//bool FORCEINLINE LoadDataItemTableRowWeights(UDataTable* table, T& data, TArray<int> nn_layers)


	/*template<typename T>
	bool FORCEINLINE SaveDataItemTableRow(UDataTable* table, T& data)
	{
		//FItemTableRow new_row;
		//new_row.data = data;

		for (int i = 0; i < data.Num(); i++)
		{
			for (int j = 0; j < data[i].Num(); j++)
			{
				row.i = i;
				row.j = j;
				row.value = FString::SanitizeFloat(data[i][j]);

				FString row_str = "";
				row_str.AppendInt(i);
				row_str.AppendInt(j);

				table->AddRow(FName(*row_str), row);
			}
		}
		return true;
	}*/


/*	template<typename T>
	bool FORCEINLINE LoadDataItemTableRow(UDataTable* table, T& data)
	{
		//TArray<TArray<FString>> aux_table;
		//aux_table = table->GetTableData();


		FString ContextString;
		TArray<FName> RowNames;
		TArray<FItemTableRowWeights> table_data;

		RowNames = table->GetRowNames();

		for (auto& name : RowNames)
		{
			FType* row = table->FindRow<FType>(name, ContextString);
			if (row)
			{
				table_data.Add(*row);
			}
		}

		//for (int i = 0; i < table_data.Num(); i++)
		//{
		//	table_data[i].i
		//		table_data[i].j
		//		table_data[i].k
		//		table_data[i].value;
		//}


		return true;
	}*/

	bool FORCEINLINE EmptyData(UDataTable* table)
	{
		table->EmptyTable();

		return true;
	}

};