// Fill out your copyright notice in the Description page of Project Settings.

#include "NNL_Component.h"

// Sets default values for this component's properties
UNNL_Component::UNNL_Component()
{
  // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
  // off to improve performance if you don't need them.
  PrimaryComponentTick.bCanEverTick = true;
	file_weights_data = CreateDefaultSubobject<UFileData>(TEXT("FileData"));
	RL_file_input = CreateDefaultSubobject<UFileData>(TEXT("RLFileDataInput"));
	RL_file_output = CreateDefaultSubobject<UFileData>(TEXT("RLFileDataOutput"));

}

UNNL_Component::UNNL_Component(TArray<int>& layers)
{
  PrimaryComponentTick.bCanEverTick = true;
	file_weights_data = CreateDefaultSubobject<UFileData>(TEXT("FileData"));

	NN_layers = layers;
  size = layers.Num();

  double r;
  TArray<Node*> aux;

  for (int i = 0; i < size - 1; i++)
  {
    weights.Add({});
    for (int j = 0; j < layers[i]; j++)
    {

      weights[i].Add({});

      for (int k = 0; k < layers[i + 1]; k++)
      {
        r = rand() % 200 - 100;
        r = r / 100;
        weights[i][j].Add(r);
      }
    }
  }

  TArray<Node> nodeVector;
  for (int i = 0; i < size; i++)
  {
    nodeVector.Empty();
    for (int j = 0; j < layers[i]; j++)
    {
      Node newNode;
      newNode.input = j;
      newNode.output = j;
      newNode.delta = j;
      nodeVector.Add(std::move(newNode));
    }
    neural_network.Add(nodeVector);
  }
}

UNNL_Component::UNNL_Component(int n_args, ...)
{
  PrimaryComponentTick.bCanEverTick = true;
	file_weights_data = CreateDefaultSubobject<UFileData>(TEXT("FileData"));

	va_list vl;

	va_start(vl, n_args);
	TArray<int> layers;
	for (int i = 1; i <= n_args; ++i) {
		layers.Add(va_arg(vl, int));
	}
	va_end(vl);


	NN_layers = layers;
  size = layers.Num();

  double r;
  TArray<Node*> aux;

  for (int i = 0; i < size - 1; i++)
  {
    weights.Add({});
    for (int j = 0; j < layers[i]; j++)
    {

      weights[i].Add({});

      for (int k = 0; k < layers[i + 1]; k++)
      {
        r = rand() % 200 - 100;
        r = r / 100;
        weights[i][j].Add(r);
      }
    }
  }

  TArray<Node> nodeVector;
  for (int i = 0; i < size; i++)
  {
    nodeVector.Empty();
    for (int j = 0; j < layers[i]; j++)
    {
      Node newNode;
      newNode.input = j;
      newNode.output = j;
      newNode.delta = j;
      nodeVector.Add(std::move(newNode));
    }
    neural_network.Add(nodeVector);
  }
}

// Called when the game starts
void UNNL_Component::BeginPlay()
{
  Super::BeginPlay();

  // ...

}

FDataArray UNNL_Component::CData(int n_args, ...) {
	va_list vl;

	va_start(vl, n_args);
	FDataArray data;
	for (int i = 1; i <= n_args; ++i) {
		data.values.Add(va_arg(vl, double));
	}
	va_end(vl);

	return data;
}

TArray<int> UNNL_Component::RL_AddData(FDataArray& input_values, FDataArray& output_values)
{
	TArray<int> r_value;
	r_value.Add(RL_data_input.Add(std::move(input_values)));
	r_value.Add(RL_data_output.Add(std::move(output_values)));

	return r_value;
}

bool UNNL_Component::RL_LoadFromFile(UDataTable* table)
{

	return true;
}

bool UNNL_Component::RL_WriteToFile(UDataTable* table)
{

	return true;

}

int UNNL_Component::RL_RemoveData(int index_0, int index_1)
{
	if ((RL_data_input.Num() > 0) && (RL_data_output.Num() > 0)) 
	{
		RL_data_input.RemoveAt(index_0);
		RL_data_output.RemoveAt(index_1);
	}
	return -1;
}

void UNNL_Component::RL_SaveData(int index_0, int index_1)
{
	if ((index_0 < 0) || (RL_data_input.Num() < index_0)) return;
	if ((index_1 < 0) || (RL_data_input.Num() < index_1)) return;

	RL_BackPropagation(RL_data_input[index_0], RL_data_output[index_1]);
}

void UNNL_Component::Init(TArray<int> layers)
{
  PrimaryComponentTick.bCanEverTick = true;
  NN_layers = layers;
  size = layers.Num();

  double r;
  TArray<Node*> aux;
  weights.Empty();
  for (int i = 0; i < size - 1; i++)
  {
    weights.Add({});
    for (int j = 0; j < layers[i]; j++)
    {

      weights[i].Add({});

      for (int k = 0; k < layers[i + 1]; k++)
      {
        r = rand() % 200 - 100;
        r = r / 100;
        weights[i][j].Add(r);
      }
    }
  }
  neural_network.Empty();
  TArray<Node> nodeVector;
  for (int i = 0; i < size; i++)
  {
    nodeVector.Empty();
    for (int j = 0; j < layers[i]; j++)
    {
      Node newNode;
      newNode.input = j;
      newNode.output = j;
      newNode.delta = j;
      nodeVector.Add(std::move(newNode));
    }
    neural_network.Add(nodeVector);
  }
}

void UNNL_Component::Init_(int n_args, ...)
{
	PrimaryComponentTick.bCanEverTick = true;


	va_list vl;

	va_start(vl, n_args);
	TArray<int> layers;
	for (int i = 1; i <= n_args; ++i) {
		layers.Add(va_arg(vl, int));
	}
	va_end(vl);

	NN_layers = layers;
	size = layers.Num();

	double r;
	TArray<Node*> aux;
	weights.Empty();
	for (int i = 0; i < size - 1; i++)
	{
		weights.Add({});
		for (int j = 0; j < layers[i]; j++)
		{

			weights[i].Add({});

			for (int k = 0; k < layers[i + 1]; k++)
			{
				r = rand() % 200 - 100;
				r = r / 100;
				weights[i][j].Add(r);
			}
		}
	}
	neural_network.Empty();
	TArray<Node> nodeVector;
	for (int i = 0; i < size; i++)
	{
		nodeVector.Empty();
		for (int j = 0; j < layers[i]; j++)
		{
			Node newNode;
			newNode.input = j;
			newNode.output = j;
			newNode.delta = j;
			nodeVector.Add(std::move(newNode));
		}
		neural_network.Add(nodeVector);
	}
}

TArray<double> UNNL_Component::FeedForwardOutput(FDataArray& input_values, bool printOutput)
{
  TArray<double> outputs;

	if (input_values.values.Num() <= 0)
	{
		return outputs;
	}

  for (int i = 0; i < neural_network[0].Num(); i++)
  {
    neural_network[0][i].output = input_values.values[i];
  }

  for (int i = 1; i < neural_network.Num(); i++)
  {
    for (int j = 0; j < neural_network[i].Num(); j++)
    {
      neural_network[i][j].input = 0;
      for (int k = 0; k < neural_network[i - 1].Num(); k++)
      {
        neural_network[i][j].input += weights[i - 1][k][j] * neural_network[i - 1][k].output;
      }

      neural_network[i][j].output = sigmoide(neural_network[i][j].input);
    }
  }

  for (int i = 0; i < neural_network[neural_network.Num() - 1].Num(); i++)
  {
    outputs.Add(neural_network[neural_network.Num() - 1][i].output);
  }
  return outputs;
}

bool UNNL_Component::LoadWeightsData()
{
	bool result = false;
	if (dt_weights) {
		 result = file_weights_data->LoadDataItemTableRowWeights(dt_weights, weights, NN_layers);
	}

	return result;

}

void UNNL_Component::BackPropagation(TArray<FDataArray>& inputs, TArray<FDataArray>& outputs, bool use_file)
{
  double suma;
  int iteration = 0;
  double ERROR = 200;
  double error;

	TArray<TArray<TArray<double>>> aux;

		while (ERROR > offset_error && iteration < maxIterations)
		{
			ERROR = 0;
			for (int e = 0; e < inputs.Num(); e++)
			{

				FeedForwardOutput(inputs[e]);

				for (int i = 0; i < neural_network[size - 1].Num(); i++)
				{
					error = (outputs[e].values[i] - neural_network[neural_network.Num() - 1][i].output);
					neural_network[neural_network.Num() - 1][i].delta = sigmoide_p(neural_network[neural_network.Num() - 1][i].input) * error;
					ERROR += error * error;
				}

				for (int i = size - 2; i >= 0; i--)
				{
					for (int j = 0; j < neural_network[i].Num(); j++)
					{
						suma = 0;
						for (int k = 0; k < weights[i][j].Num(); k++)
						{
							suma = suma + weights[i][j][k] * neural_network[i + 1][k].delta;
						}
						neural_network[i][j].delta = sigmoide_p(neural_network[i][j].input) * suma;
						for (int k = 0; k < neural_network[i + 1].Num(); k++)
						{
							weights[i][j][k] = weights[i][j][k] + 0.5 * neural_network[i][j].output * neural_network[i + 1][k].delta;
						}
					}
				}
			}
			ERROR *= 0.5 * (1.0 / inputs.Num());
			iteration++;
		}
		final_error = ERROR;
		final_iterations = iteration;
		
		if (file_weights_data && dt_weights)
		{
			file_weights_data->SaveDataItemTableRowWeights(dt_weights, weights);
		}

}

void UNNL_Component::RL_BackPropagation(FDataArray& input, FDataArray& output)
{
	double suma = 0;
	double ERROR = 0;
	double error = 0;

	TArray<TArray<TArray<double>>> aux;

	//ERROR = 0;
	FeedForwardOutput(input);
	
	for (int i = 0; i < neural_network[size - 1].Num(); i++)
	{
		error = (output.values[i] - neural_network[neural_network.Num() - 1][i].output);
		neural_network[neural_network.Num() - 1][i].delta = sigmoide_p(neural_network[neural_network.Num() - 1][i].input) * error;
		ERROR += error * error;
	}
	
	for (int i = size - 2; i >= 0; i--)
	{
		for (int j = 0; j < neural_network[i].Num(); j++)
		{
			suma = 0;
			for (int k = 0; k < weights[i][j].Num(); k++)
			{
				suma = suma + weights[i][j][k] * neural_network[i + 1][k].delta;
			}
			neural_network[i][j].delta = sigmoide_p(neural_network[i][j].input) * suma;
			for (int k = 0; k < neural_network[i + 1].Num(); k++)
			{
				weights[i][j][k] = weights[i][j][k] + 0.5 * neural_network[i][j].output * neural_network[i + 1][k].delta;
			}
		}
	}

	ERROR *= 0.5 * (1.0 / input.values.Num());

	if (file_weights_data && dt_weights)
	{
		file_weights_data->SaveDataItemTableRowWeights(dt_weights, weights);
	}
}

void UNNL_Component::AddInput(TArray<double> input, bool clear)
{
  if (clear) nnl_inputs.Empty();

  nnl_inputs.Add(std::move(input));
}

void UNNL_Component::AddOutput(TArray<double> output, bool clear)
{
  if (clear) nnl_outputs.Empty();

  nnl_outputs.Add(std::move(output));
}

int UNNL_Component::GetNodeActivated(TArray<double> result)
{
	TArray<double> aux_result;

	aux_result = result;

	int size = aux_result.Num() - 1;
	double aux = 0.0f;
	for (int i = 0; i < size; i++)
	{
		if (aux_result[i] < aux_result[i + 1])
		{
			aux = aux_result[i + 1];
			aux_result[i + 1] = aux_result[i];
			aux_result[i] = aux;
		}
	}
	float rnd = 0.0f;
	for (int i = 0; i < (size + 1); i++)
	{
		rnd = FMath::RandRange(0, 100);
		if (rnd <= (aux_result[i] * 100.0f))
		{
			return i;
		}
	}

	int max_tries = 100;
	int tries = 0;
	int index_rnd = 0;
	while (tries < max_tries)
	{
		index_rnd = FMath::RandRange(0, size);
		rnd = FMath::RandRange(0, 100);
		if (rnd <= (aux_result[index_rnd] * 100.0f))
		{
			return index_rnd;
		}
		tries++;
	}

	return -1;
}


// Called every frame
void UNNL_Component::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
  Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

  // ...
}

double UNNL_Component::sigmoide(double x)
{

  return (1.0 / (1 + exp(-1 * x)));

}

double UNNL_Component::sigmoide_p(double x)
{

  return (exp(x) / ((exp(x) + 1)*(exp(x) + 1)));

}

double UNNL_Component::normalize(double v, double min, double max)
{
  return (v - min) / (max - min);
}

double UNNL_Component::denormalize(double v, double min, double max)
{
  return (v * (max - min) + min);
}