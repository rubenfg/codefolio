// Fill out your copyright notice in the Description page of Project Settings.

#include "FileData.h"


// Sets default values for this component's properties
UFileData::UFileData()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	//data_table = CreateDefaultSubobject<UDataTable>(TEXT("DataTableTest"));
	// ...
}


// Called when the game starts
void UFileData::BeginPlay()
{
	Super::BeginPlay();

	// ...

}
/*
void UFileData::SaveLoadData3D(FArchive& Ar, TArray<TArray<TArray<double>>>& data)
{
	Ar << data;

	//Save or load values
	//for (int i = 0; i < data.Num(); i++)
	//{
	//	Ar << data[i];
	//}
	//Ar << data;
	//Ar << CurrentAmmoToSaveOrLoad;

	//Ar << PlayerLocationToSaveOrLoad;

//
}

bool UFileData::SaveData3D(TArray<TArray<TArray<double>>>& data, FString filename)
{
	//Save the data to binary
	FBufferArchive ToBinary;
	SaveLoadData3D(ToBinary, data);

	//No data were saved
	if (ToBinary.Num() <= 0) return false;

	//Save binaries to disk
	bool result = FFileHelper::SaveArrayToFile(ToBinary, (TCHAR*)filename.Chr);

	//Empty the buffer's contents
	ToBinary.FlushCache();
	ToBinary.Empty();

	return result;

}

bool UFileData::LoadData3D(FString filename)
{
	TArray<uint8> BinaryArray;

	//load disk data to binary array
	if (!FFileHelper::LoadFileToArray(BinaryArray, (TCHAR*)filename.Chr)) return false;

	if (BinaryArray.Num() <= 0) return false;

	//Memory reader is the archive that we're going to use in order to read the loaded data
	FMemoryReader FromBinary = FMemoryReader(BinaryArray, true);
	FromBinary.Seek(0);

	SaveLoadData3D(FromBinary, values);

	//Empty the buffer's contents
	FromBinary.FlushCache();
	BinaryArray.Empty();
	//Close the stream
	FromBinary.Close();

	return true;
}

void UFileData::SaveLoadData2D(FArchive& Ar, TArray<TArray<double>>& data)
{
	Ar << data;
}

bool UFileData::SaveData2D(TArray<TArray<double>>& data, FString filename)
{
	//Save the data to binary
	FBufferArchive ToBinary;
	SaveLoadData2D(ToBinary, data);

	//No data were saved
	if (ToBinary.Num() <= 0) return false;

	//Save binaries to disk
	bool result = FFileHelper::SaveArrayToFile(ToBinary, (TCHAR*)filename.Chr);

	//Empty the buffer's contents
	ToBinary.FlushCache();
	ToBinary.Empty();

	return result;

}

bool UFileData::LoadData2D(FString filename)
{
	TArray<uint8> BinaryArray;

	//load disk data to binary array
	if (!FFileHelper::LoadFileToArray(BinaryArray, (TCHAR*)filename.Chr)) return false;

	if (BinaryArray.Num() <= 0) return false;

	//Memory reader is the archive that we're going to use in order to read the loaded data
	FMemoryReader FromBinary = FMemoryReader(BinaryArray, true);
	FromBinary.Seek(0);

	SaveLoadData2D(FromBinary, values2D);

	//Empty the buffer's contents
	FromBinary.FlushCache();
	BinaryArray.Empty();
	//Close the stream
	FromBinary.Close();

	return true;
}
*/
// Called every frame
void UFileData::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UFileData::SaveDataItemTableRowWeights(UDataTable* table, TArray<TArray<TArray<double>>>& data)
{
	//FItemTableRow new_row;
	//new_row.data = data;
	FItemTableRowWeights row;
	int c = 0;
	for (int i = 0; i < data.Num(); i++)
	{
		for (int j = 0; j < data[i].Num(); j++)
		{
			for (int k = 0; k < data[i][j].Num(); k++)
			{
				row.i = i;
				row.j = j;
				row.k = k;
				row.str_value = FString::SanitizeFloat(data[i][j][k]);
				row.value = data[i][j][k];

				FString row_str;
				row_str.AppendInt(c);
				//row_str.AppendInt(j);
				//row_str.AppendInt(k);
				//row_str.AppendInt(i);
				table->AddRow(FName(*row_str), row);
				c++;
			}
		}
	}
	return true;
}

bool UFileData::LoadDataItemTableRowWeights(UDataTable* table, TArray<TArray<TArray<double>>>& data, TArray<int> nn_layers)
{

	FString ContextString;
	TArray<FName> RowNames;
	TArray<FItemTableRowWeights*> table_data;

	//RowNames = table->GetRowNames();
	//
	//for (auto& name : RowNames)
	//{
	//	FItemTableRowWeights* row = table->FindRow<FItemTableRowWeights>(name, ContextString);
	//	if (row)
	//	{
	//		table_data.Add(*row);
	//	}
	//}
	table->GetAllRows<FItemTableRowWeights>(ContextString, table_data);

	if (table_data.Num() <= 0)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, "No File");
		return false;
	}
	data.Empty();
	int counterino = 0;
	for (int i = 0; i < (nn_layers.Num() - 1); i++)
	{
		data.Add({});
		for (int j = 0; j < nn_layers[i]; j++)
		{

			data[i].Add({});

			for (int k = 0; k < nn_layers[i + 1]; k++)
			{
				data[i][j].Add(table_data[counterino]->value);
				counterino++;
			}
		}
	}

	if (data.Num() > 0)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, "File Loaded");
		return true;
	}
	return false;
}

bool UFileData::SaveDataItemTableRow(UDataTable* table, TArray<TArray<double>>& data)
{
	//FItemTableRow new_row;
	//new_row.data = data;

/*	for (int i = 0; i < data.Num(); i++)
	{
		for (int j = 0; j < data[i].Num(); j++)
		{
			row.i = i;
			row.j = j;
			row.value = FString::SanitizeFloat(data[i][j]);

			FString row_str = "";
			row_str.AppendInt(i);
			row_str.AppendInt(j);

			table->AddRow(FName(*row_str), row);
		}
	}
	return true;*/

	FItemTableRow row;
	TArray<FItemTableRow*> table_data;
	FString ContextString;

	table->GetAllRows<FItemTableRow>(ContextString, table_data);
	int c = 0;
	c = table_data.Num();
	for (int i = 0; i < data.Num(); i++)
	{
		for (int j = 0; j < data[i].Num(); j++)
		{
			
			row.i = i;
			row.j = j;
			row.value = data[i][j];
			row.str_value = FString::SanitizeFloat(data[i][j]);
			FString row_str;
			row_str.AppendInt(c);
			//row_str.AppendInt(j);
			//row_str.AppendInt(k);
			//row_str.AppendInt(i);
			table->AddRow(FName(*row_str), row);
			c++;
		}
	}
	return true;
}

bool UFileData::LoadDataItemTableRow(UDataTable* table, TArray<TArray<double>>&data)
{
	//TArray<TArray<FString>> aux_table;
			//aux_table = table->GetTableData();


	FString ContextString;
	TArray<FName> RowNames;
	TArray<FItemTableRow*> table_data;

	//RowNames = table->GetRowNames();
	//
	//for (auto& name : RowNames)
	//{
	//	FItemTableRow* row = table->FindRow<FItemTableRow>(name, ContextString);
	//	if (row)
	//	{
	//		UE_LOG(LogTemp, Warning, TEXT("Retrieving data"))
	//	//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, "Retrieving data");
	//		table_data.Add(*row);
	//	}
	//}
//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, "Before GetAllRows");
	table->GetAllRows<FItemTableRow>(ContextString, table_data);

	if (table_data.Num() <= 0)
	{
	//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, "Data = 0");
		return false;
	}
	data.Empty();
	uint64 counterino = 0;
	//
	////FString msg = FString::SanitizeFloat(table_data.Last().j);
	////GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, msg);

	//for (int i = 0; i < (table_data.Last()->i + 1); i++)
	TArray<TArray<FString>> dataString;
	int val2 = (table_data.Last()->i + 1);
	int val = (table_data.Last()->j + 1);
	for (int i = 0; i < val2; i++)
	{
		data.Add({});
		//dataString.Add({});
		for (int j = 0; j < val; j++)
		{
			data[i].Add(std::move(table_data[counterino]->value));
			
			counterino++;
		}
		//if (counterino > 1000)
		//{
		//	break;
		//}
	}
	//for (int i = 0; i < val2; i++)
	//{
	//	data.Add({});
	//	for (int j = 0; j < val; j++)
	//	{
	//		//double a = std::move(FCString::Atod(*dataString[i][j]));
	//		//data[i].Add(std::move(a));
	//		data[i].Add(FCString::Atod(*dataString[i][j]));
	//	}
	//}


//	for (int i = 0; i < data.Num(); i++)
//	{
//		for (int j = 0; j < data[i].Num(); j++)
//		{
//			//FString masage = FString::SanitizeFloat(data[i][j]);
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, masage);
//		}
//	}
	if (data.Num() > 0)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, "File Loaded");
		return true;
	}
	return false;
}