rmdir /s /q Binaries
rmdir /s /q Plugins\PoolMod\Binaries
rmdir /s /q Intermediate
rmdir /s /q Plugins\PoolMod\Intermediate
rmdir /s /q Saved
rmdir /s /q DerivedDataCache
rmdir /s /q Build
del **.opensdf
del **.sdf
del **.sln
del **.suo
del **.xcodeproj

pause