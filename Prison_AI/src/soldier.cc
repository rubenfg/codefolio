#include <stdio.h>
#include <stdlib.h>
#include <soldier.h>
#include <prisoner.h>
#include <app.h>

Soldier::Soldier(){
}

Soldier::~Soldier(){
  
}

bool Soldier::prisionersFree() {
  for (int i = 0; i < 10; i++) {
    if (!app::instance().prisonerTest[i]->freePrisoner()) {
      return false;
    }
  }
  return true;
}

void Soldier::init(const char* spritePath, ESAT::Vec3 position, float speed) {
  data_ = new AgentData();
  data_->velocity_ = new ESAT::Vec3();
  data_->position_ = new ESAT::Vec3();
  data_->target_ = new ESAT::Vec3();
  data_->path_ = new Path();
  set_speed(speed);
  set_sprite(spritePath);
  mainMindState_ = MindState::kmindState_Init;
  set_position(&position);
  set_target(&position);

}

void Soldier::update(float deltaTime) {
  mind(deltaTime);
  body(deltaTime);
}

void Soldier::FSM_init(float d_time) {
  changeToGoA();
}

void Soldier::FSM_GoA(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (bGoToFirstPoint) {
    if (path()->length() == 0) {
      path()->addPoint(ESAT::Vec3{96.0f, 672.0f, 0.0f}, sizeof(ESAT::Vec3));
      path()->set_action(Path::kActionStraight);
      set_targetReached(false);
    }

    if (target_reached()) {
      bGoToFirstPoint = false;
      path()->reset();
    }
  }else {
    if (path()->length() == 0) {
      std::vector<glm::vec2> aux_vector = app::instance().map->go_to_a_path_;
      for (int i = 0; i < aux_vector.size(); i++) {
        path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
      }
      path()->set_action(Path::kActionStraight);
      set_targetReached(false);
      direction_ = kdirection_A;
    }
    if (target_reached()) {
      changeToGoB();
    }
  }
}

void Soldier::FSM_GoB(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    if (!app::instance().map->door_a_->open) {
      std::vector<glm::vec2> aux_vector = app::instance().map->gtb_p_c_;
      for (int i = 0; i < aux_vector.size(); i++) {
        path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
      }
    }else {
      std::vector<glm::vec2> aux_vector = app::instance().map->gtb_p_o_;
      for (int i = 0; i < aux_vector.size(); i++) {
        path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
      }
    }
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
    direction_ = kdirection_B;
  }
  if (target_reached()) {
    app::instance().map->openDoor(app::instance().map->door_b_);
    changeToGoOpenA();
  }
}

void Soldier::FSM_GoOpenA(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
     std::vector<glm::vec2> aux_vector = app::instance().map->gta_p_o_;
     for (int i = 0; i < aux_vector.size(); i++) {
        path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
     }

    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
    direction_ = kdirection_A;
  }
  
  if (target_reached()) {
    app::instance().map->openDoor(app::instance().map->door_a_);    
    if (app::instance().bAlarm && prisionersFree()) {
      changeToEscape();
      if (!app::instance().map->door_a_->open) app::instance().map->openDoor(app::instance().map->door_a_);
    }else {
      changeToDoorToDoor();
      direction_ = kdirection_B;
    }
  }
}

void Soldier::FSM_doorToDoor(float d_time) {
  movement_ = kMovementType_Deterministic;
    if (path()->length() == 0) {
      std::vector<glm::vec2> aux_vector;
      switch (direction_) {
      case kdirection_B:
          aux_vector = app::instance().map->dtd_g_b_;
          for (int i = 0; i < aux_vector.size(); i++) {
            path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
          }
          path()->set_action(Path::kActionStraight);
          set_targetReached(false);
          direction_ = kdirection_B;
      break;
      case kdirection_A:
          aux_vector = app::instance().map->dtd_g_b_;
          for (int i = aux_vector.size() - 1; i >= 0 ; i--) {
            path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
          }
          path()->set_action(Path::kActionStraight);
          set_targetReached(false);
          direction_ = kdirection_A;
     break;
      }
    }
    if (target_reached()) {
      path()->reset();
      set_targetReached(false);
      if (app::instance().bAlarm && prisionersFree()) {
        changeToEscape();
        switch (direction_) {
        case kdirection_B:
          if (!app::instance().map->door_b_->open) {
            app::instance().map->openDoor(app::instance().map->door_b_);
          }
          break;
        case kdirection_A:
          if (!app::instance().map->door_a_->open) {
            app::instance().map->openDoor(app::instance().map->door_a_);
          }
          break;
        }
      }else {
        switch (direction_) {
        case kdirection_B:
          if (!app::instance().map->door_b_->open) {
            app::instance().map->openDoor(app::instance().map->door_b_);
          }
          direction_ = kdirection_A;
          break;
        case kdirection_A:
          if (!app::instance().map->door_a_->open) {
            app::instance().map->openDoor(app::instance().map->door_a_);
          }
          direction_ = kdirection_B;
          break;
        }
      }
    }
}

void Soldier::FSM_escape(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    bFinish = true;
    std::vector<glm::vec2> aux_vector;
    switch (direction_) {
    case kdirection_B:
      aux_vector = app::instance().map->b_go_baseb_;
        for (int i = 0; i < aux_vector.size(); i++) {
          path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
        }
        path()->set_action(Path::kActionStraight);
        set_targetReached(false);
    break;
    case kdirection_A:
        aux_vector = app::instance().map->a_go_basea_;
        for (int i = 0; i < aux_vector.size(); i++) {
          path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
        }
        path()->set_action(Path::kActionStraight);
        set_targetReached(false);
    break;
    }
  }
  if (target_reached()) {
    set_target(position());
  }
}

void Soldier::FSM_End(float d_time) {
  changeToEnd();
}

void Soldier::mind(float d_time) {
  switch (mainMindState_) {
  case kmindState_Init:
    FSM_init(d_time);
    break;
  case kmindState_GoA:
    FSM_GoA(d_time);
    break;
  case kmindState_GoB:
    FSM_GoB(d_time);
    break;
  case kmindState_GoOpenA:
    FSM_GoOpenA(d_time);
    break;
  case kmindState_DoorToDoor:
    FSM_doorToDoor(d_time);
    break;
  case kmindState_Escape:
    FSM_escape(d_time);
    break;
  case kmindState_End:
    FSM_End(d_time);
    break;
  };
}

void Soldier::body(float d_time) {
  ESAT::Vec3* vel = new ESAT::Vec3();

  vel->x = target()->x - position()->x;
  vel->y = target()->y - position()->y;
  vel->z = target()->z - position()->z;

  *vel = app::instance().Normalize(vel);
  set_velocity(vel->x * data_->speed_, vel->y * data_->speed_, vel->z * data_->speed_);

  switch (movement_) {
  case kMovementType_Deterministic:
    MOV_deterministic(d_time);
    break;
  }
  if (!target_reached()) move(d_time);
}

void Soldier::move(float d_time) {
  position()->x = position()->x + velocity()->x * d_time / 1000.0f;
  position()->y = position()->y + velocity()->y * d_time / 1000.0f;
}

void Soldier::MOV_deterministic(float d_time) {
  if (path()->length() > 0) {
    if (!target_reached()) {
      set_target(&path()->actualPoint());
    }
    if (app::instance().Distance(*target(), *position()) <= data_->distance_offset_) {
      if (path()->isLast()) {
        set_targetReached(true);
      }else {
        path()->nextPoint();
      }
    }
  }else {
    set_target(position());
  }
}

void Soldier::draw() {
  ESAT::DrawSprite(data_->sprite_, data_->position_->x, data_->position_->y);
}

//###############################################################################
//                                  STATE CHANGES
//###############################################################################

void Soldier::changeToEscape() {
  path()->reset();
  mainMindState_ = kmindState_Escape;
}

void Soldier::changeToGoA() {
  path()->reset();
  mainMindState_ = kmindState_GoA;
  direction_ = kdirection_A;
}

void Soldier::changeToGoB() {
  path()->reset();
  mainMindState_ = kmindState_GoB;
  direction_ = kdirection_B;
}
void Soldier::changeToGoOpenA() {
  path()->reset();
  mainMindState_ = kmindState_GoOpenA;
  direction_ = kdirection_A;
}
void Soldier::changeToDoorToDoor() {
  path()->reset();
  mainMindState_ = kmindState_DoorToDoor;
}

void Soldier::changeToEnd() {
  path()->reset();
  mainMindState_ = kmindState_End;
}

//###############################################################################
//                                  SETTERS
//###############################################################################

void Soldier::set_target(ESAT::Vec3* target) {
  data_->target_->x = target->x;
  data_->target_->y = target->y;
  data_->target_->z = target->z;
}

void Soldier::set_path(Path* path) {
  data_->path_ = path;
}

void Soldier::set_position(ESAT::Vec3* position) {
  data_->position_->x = position->x;
  data_->position_->y = position->y;
  data_->position_->z = position->z;
}

void Soldier::set_targetReached(bool target_reached) {
  data_->target_reached_ = target_reached;
}

void Soldier::set_velocity(float x, float y, float z) {
  data_->velocity_->x = x;
  data_->velocity_->y = y;
  data_->velocity_->z = z;
}

void Soldier::set_sprite(const char* filename) {
  data_->sprite_ = ESAT::SpriteFromFile(filename);
}

void Soldier::set_speed(float speed) {
  data_->speed_ = speed;
}

//###############################################################################
//                                  GETTERS
//###############################################################################

ESAT::Vec3* Soldier::target() {
  return data_->target_;
}

Path* Soldier::path() {
  return data_->path_;
}

ESAT::Vec3* Soldier::position() {
  return data_->position_;
}

bool Soldier::target_reached() {
  return data_->target_reached_;
}

ESAT::Vec3* Soldier::velocity() {
  return data_->velocity_;
}

float Soldier::speed() {
  return data_->speed_;
}