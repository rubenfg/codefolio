#include "stdio.h"
#include "grid.h"
#include "app.h"
#include "math.h"
#include "stb_image.h"
#include "common_def.h"
#include <string>
#include <iostream>     // std::cout
#include <fstream> 

Grid::Grid() {
}

Grid::~Grid() {

}

void Grid::init() {
  box_manager_ = new BoxManager();
  Box* box1 = new Box();
  box1->initial_position_.x = 416.0f;
  box1->initial_position_.y = 240.0f;
  box1->initial_position_.z = 0.0f;
  box1->final_position_.x = 624.0f;
  box1->final_position_.y = 240.0f;
  box1->final_position_.z = 0.0f;
  box1->bOcuped_ = false;
  Box* box2 = new Box();
  box2->initial_position_.x = 416.0f;
  box2->initial_position_.y = 256.0f;
  box2->initial_position_.z = 0.0f;
  box2->final_position_.x = 624.0f;
  box2->final_position_.y = 256.0f;
  box2->final_position_.z = 0.0f;
  box2->bOcuped_ = false;
  Box* box3 = new Box();
  box3->initial_position_.x = 416.0f;
  box3->initial_position_.y = 272.0f;
  box3->initial_position_.z = 0.0f;
  box3->final_position_.x = 624.0f;
  box3->final_position_.y = 272.0f;
  box3->final_position_.z = 0.0f;
  box3->bOcuped_ = false;
  Box* box4 = new Box();
  box4->initial_position_.x = 416.0f;
  box4->initial_position_.y = 288.0f;
  box4->initial_position_.z = 0.0f;
  box4->final_position_.x = 624.0f;
  box4->final_position_.y = 288.0f;
  box4->final_position_.z = 0.0f;
  box4->bOcuped_ = false;
  Box* box5 = new Box();
  box5->initial_position_.x = 416.0f;
  box5->initial_position_.y = 304.0f;
  box5->initial_position_.z = 0.0f;
  box5->final_position_.x = 624.0f;
  box5->final_position_.y = 304.0f;
  box5->final_position_.z = 0.0f;
  box5->bOcuped_ = false;
  box_manager_->boxes_.push_back(box1);
  box_manager_->boxes_.push_back(box2);
  box_manager_->boxes_.push_back(box3);
  box_manager_->boxes_.push_back(box4);
  box_manager_->boxes_.push_back(box5);
  box_sprite_ = ESAT::SpriteFromFile("../img/chunk_box.png");
  point_sprite_ = ESAT::SpriteFromFile("../img/chunk_points.png");
  chunk_minpath = ESAT::SpriteFromFile("../img/chunk_minipath.png");

  NodePoint* point1 = new NodePoint();
  point1->id_ = 0;
  point1->position_.x = 208.0f;
  point1->position_.y = 80.0f;
  prison_points_.push_back(point1);

  NodePoint* point2 = new NodePoint();
  point2->id_ = 1;
  point2->position_.x = 152.0f;
  point2->position_.y = 256.0f;
  prison_points_.push_back(point2);

  NodePoint* point3 = new NodePoint();
  point3->id_ = 2;
  point3->position_.x = 264.0f;
  point3->position_.y = 424.0f;
  prison_points_.push_back(point3);

  NodePoint* point4 = new NodePoint();
  point4->id_ = 3;
  point4->position_.x = 304.0f;
  point4->position_.y = 224.0f;
  prison_points_.push_back(point4);

  NodePoint* point5 = new NodePoint();
  point5->id_ = 4;
  point5->position_.x = 368.0f;
  point5->position_.y = 152.0f;
  prison_points_.push_back(point5);

  NodePoint* point6 = new NodePoint();
  point6->id_ = 5;
  point6->position_.x = 312.0f;
  point6->position_.y = 328.0f;
  prison_points_.push_back(point6);

  NodePoint* point7 = new NodePoint();
  point7->id_ = 6;
  point7->position_.x = 352.0f;
  point7->position_.y = 392.0f;
  prison_points_.push_back(point7);

  NodePoint* point8 = new NodePoint();
  point8->id_ = 7;
  point8->position_.x = 376.0f;
  point8->position_.y = 440.0f;
  prison_points_.push_back(point8);

  NodePoint* point9 = new NodePoint();
  point9->id_ = 8;
  point9->position_.x = 528.0f;
  point9->position_.y = 424.0f;
  prison_points_.push_back(point9);

  NodePoint* point10 = new NodePoint();
  point10->id_ = 9;
  point10->position_.x = 528.0f;
  point10->position_.y = 168.0f;
  prison_points_.push_back(point10);

  NodePoint* point11 = new NodePoint();
  point11->id_ = 10;
  point11->position_.x = 528.0f;
  point11->position_.y = 112.0f;
  prison_points_.push_back(point11);

  NodePoint* point12 = new NodePoint();
  point12->id_ = 11;
  point12->position_.x = 304.0f;
  point12->position_.y = 80.0f;
  prison_points_.push_back(point12);

  NodePoint* point13 = new NodePoint();
  point13->id_ = 12;
  point13->position_.x = 728.0f;
  point13->position_.y = 88.0f;
  prison_points_.push_back(point13);

  NodePoint* point14 = new NodePoint();
  point14->id_ = 13;
  point14->position_.x = 752.0f;
  point14->position_.y = 192.0f;
  prison_points_.push_back(point14);

  NodePoint* point15 = new NodePoint();
  point15->id_ = 14;
  point15->position_.x = 752.0f;
  point15->position_.y = 424.0f;
  prison_points_.push_back(point15);

  NodePoint* point16 = new NodePoint();
  point16->id_ = 15;
  point16->position_.x = 808.0f;
  point16->position_.y = 336.0f;
  prison_points_.push_back(point16);

  NodePoint* point17 = new NodePoint();
  point17->id_ = 16;
  point17->position_.x = 808.0f;
  point17->position_.y = 216.0f;
  prison_points_.push_back(point17);

  NodePoint* point18 = new NodePoint();
  point18->id_ = 17;
  point18->position_.x = 368.0f;
  point18->position_.y = 320.0f;
  prison_points_.push_back(point18);

  NodePoint* point19 = new NodePoint();
  point19->id_ = 18;
  point19->position_.x = 368.0f;
  point19->position_.y = 224.0f;
  prison_points_.push_back(point19);

  NodePoint* point20 = new NodePoint();
  point20->id_ = 19;
  point20->position_.x = 688.0f;
  point20->position_.y = 224.0f;
  prison_points_.push_back(point20);

  NodePoint* point21 = new NodePoint();
  point21->id_ = 20;
  point21->position_.x = 688.0f;
  point21->position_.y = 320.0f;
  prison_points_.push_back(point21);

  //point1
  point1->nears_.push_back(point12);

  //point2
  point2->nears_.push_back(point12);
  point2->nears_.push_back(point3);

  point3->nears_.push_back(point2);
  point3->nears_.push_back(point12);

  point4->nears_.push_back(point5);
  point4->nears_.push_back(point19);
  point4->nears_.push_back(point18);

  point5->nears_.push_back(point4);
  point5->nears_.push_back(point19);

  point6->nears_.push_back(point18);
  point6->nears_.push_back(point7);

  point7->nears_.push_back(point6);
  point7->nears_.push_back(point8);

  point8->nears_.push_back(point7);
  point8->nears_.push_back(point9);

  point9->nears_.push_back(point8);
  point9->nears_.push_back(point18);
  point9->nears_.push_back(point21);

  point10->nears_.push_back(point20);
  point10->nears_.push_back(point11);
  point10->nears_.push_back(point19);

  point11->nears_.push_back(point10);

  point12->nears_.push_back(point1);
  point12->nears_.push_back(point2);
  point12->nears_.push_back(point3);
  point12->nears_.push_back(point13);

  point13->nears_.push_back(point12);
  point13->nears_.push_back(point14);

  point14->nears_.push_back(point13);
  point14->nears_.push_back(point17);
  point14->nears_.push_back(point20);

  point15->nears_.push_back(point21);
  point15->nears_.push_back(point20);
  point15->nears_.push_back(point16);

  point16->nears_.push_back(point15);
  point16->nears_.push_back(point21);
  point16->nears_.push_back(point20);

  point17->nears_.push_back(point14);

  point18->nears_.push_back(point6);
  point18->nears_.push_back(point9);
  point18->nears_.push_back(point21);
  point18->nears_.push_back(point19);
  point18->nears_.push_back(point4);

  point19->nears_.push_back(point18);
  point19->nears_.push_back(point4);
  point19->nears_.push_back(point5);
  point19->nears_.push_back(point10);
  point19->nears_.push_back(point20);

  point20->nears_.push_back(point14);
  point20->nears_.push_back(point10);
  point20->nears_.push_back(point19);
  point20->nears_.push_back(point21);
  point20->nears_.push_back(point15);
  point20->nears_.push_back(point16);

  point21->nears_.push_back(point20);
  point21->nears_.push_back(point18);
  point21->nears_.push_back(point9);
  point21->nears_.push_back(point15);
  point21->nears_.push_back(point16);
}

void Grid::generateGeneralPaths() {

  door_a_ = new Door();
  door_a_->door_position_.push_back(glm::vec2({ 144.0f / 8, 216.0f / 8 }));
  door_a_->door_position_.push_back(glm::vec2({ 152.0f / 8, 216.0f / 8 }));
  door_a_->door_position_.push_back(glm::vec2({ 160.0f / 8, 216.0f / 8 }));

  door_b_ = new Door();
  door_b_->door_position_.push_back(glm::vec2({ 816.0f / 8, 256.0f / 8 }));
  door_b_->door_position_.push_back(glm::vec2({ 816.0f / 8, 264.0f / 8 }));
  door_b_->door_position_.push_back(glm::vec2({ 816.0f / 8, 272.0f / 8 }));
  door_b_->door_position_.push_back(glm::vec2({ 816.0f / 8, 280.0f / 8 }));

  closeDoor(door_a_);
  closeDoor(door_b_);

  Path* aux_path = new Path();

  aux_path->calculatePath(glm::vec2{ 96.0f / 8, 672.0f / 8 }, glm::vec2({ 144.0f / 8 , 200.0f / 8 }));
  go_to_a_path_ = std::move(aux_path->path_);

  aux_path->calculatePath(glm::vec2{ 144.0f / 8, 200.0f / 8 }, glm::vec2({ 832.0f / 8 , 272.0f / 8 }));
  gtb_p_c_ = std::move(aux_path->path_);

  openDoor(door_a_);
  aux_path->calculatePath(glm::vec2{ 144.0f / 8, 200.0f / 8 }, glm::vec2({ 800.0f / 8 , 272.0f / 8 }));
  gtb_p_o_ = std::move(aux_path->path_);
  closeDoor(door_a_);

  openDoor(door_b_);
  aux_path->calculatePath(glm::vec2{ 832.0f / 8, 272.0f / 8 }, glm::vec2({ 152.0f / 8 , 232.0f / 8 }));
  gta_p_o_ = std::move(aux_path->path_);
  closeDoor(door_b_);

  aux_path->calculatePath(glm::vec2{ 800.0f / 8, 272.0f / 8 }, glm::vec2({ 152.0f / 8 , 232.0f / 8 }));
  dtd_g_a_ = std::move(aux_path->path_);

  aux_path->calculatePath(glm::vec2{ 152.0f / 8, 232.0f / 8 }, glm::vec2({ 800.0f / 8 , 272.0f / 8 }));
  dtd_g_b_ = std::move(aux_path->path_);

  openDoor(door_a_);
  aux_path->calculatePath(glm::vec2{ 152.0f / 8, 232.0f / 8 }, glm::vec2({ 48.0f / 8 , 672.0f / 8 }));
  a_go_basea_ = std::move(aux_path->path_);
  closeDoor(door_a_);

  openDoor(door_b_);
  aux_path->calculatePath(glm::vec2{ 800.0f / 8, 272.0f / 8 }, glm::vec2({ 896.0f / 8 , 664.0f / 8 }));
  b_go_baseb_ = std::move(aux_path->path_);
  closeDoor(door_b_);


  aux_path->calculatePath(glm::vec2{800.0f / 8, 272.0f / 8}, glm::vec2{568.0f / 8, 504.0f / 8});
  a_to_rest_ = std::move(aux_path->path_);
  delete(aux_path);
}

void Grid::openDoor(Door* door) {
  for (int i = 0; i < door->door_position_.size(); i++) {
    glm::vec2 aux = door->door_position_[i];
    elements_[(aux.y * size_x) + aux.x].value_ = 0;
  }
  door->open = true;
}

void Grid::closeDoor(Door* door) {
  for (int i = 0; i < door->door_position_.size(); i++) {
    glm::vec2 aux = door->door_position_[i];
    elements_[(aux.y * size_x) + aux.x].value_ = 2;
  }
  door->open = false;
}

bool Grid::doorsOpen() {
  return (door_a_->open | door_b_->open);
}

void Grid::generateNodePointsPaths() {
  
  for (int j = 0; j < prison_points_.size(); j++) {
    for (unsigned int i = 0; i < prison_points_[j]->nears_.size(); i++) {
      Path* path = new Path();
      glm::vec2 p1 = prison_points_[j]->position_;
      p1.x = p1.x / 8;
      p1.y = p1.y / 8;
      glm::vec2 p2 = prison_points_[j]->nears_[i]->position_;
      p2.x = p2.x / 8;
      p2.y = p2.y / 8;
      path->calculatePath(p1, p2);
      prison_points_[j]->paths_.push_back(std::move(path->path_));

      delete(path);
    }
  }
  
}
void Grid::drawNodePointsPaths() {
  for (int j = 0; j < prison_points_.size(); j++) {
    for (unsigned int i = 0; i < prison_points_[j]->paths_.size(); i++) {
      for (auto element : prison_points_[j]->paths_[i]) {
        ESAT::DrawSprite(chunk_minpath, element.x * 8, element.y * 8);
      }
    }
  }
}

Grid::Box* Grid::getFreeBox() {
  for (unsigned int i = 0; i < box_manager_->boxes_.size(); i++) {
    if (!box_manager_->boxes_[i]->bOcuped_){
      box_manager_->boxes_[i]->bOcuped_ = true;
      return box_manager_->boxes_[i];
    }
  }
  return nullptr;
}

bool Grid::CheckFreeBox() {
  for (unsigned int i = 0; i < box_manager_->boxes_.size(); i++) {
    if (!box_manager_->boxes_[i]->bOcuped_) {
      return true;
    }
  }
  return false;
}

void Grid::drawBoxes() {
  for (unsigned int i = 0; i < box_manager_->boxes_.size(); i++) {
      ESAT::DrawSprite(box_sprite_, box_manager_->boxes_[i]->initial_position_.x, box_manager_->boxes_[i]->initial_position_.y);
  }
}

void Grid::drawPrisonPoints() {
  for (unsigned int i = 0; i < prison_points_.size(); i++) {
    ESAT::DrawSprite(point_sprite_, prison_points_[i]->position_.x, prison_points_[i]->position_.y);
  }
}

void Grid::drawPrisonPointRute(int id) {
  for (int jaja = 0; jaja < prison_points_.size(); jaja++) {
    if (prison_points_[jaja]->id_ == id) {
      
      for (unsigned int i = 0; i < prison_points_[jaja]->paths_.size(); i++) {
        for (auto element : prison_points_[jaja]->paths_[i]) {
          ESAT::DrawSprite(chunk_minpath, element.x * 8, element.y * 8);
        }
      }
      ESAT::DrawSprite(point_sprite_, prison_points_[jaja]->position_.x * 8, prison_points_[jaja]->position_.y * 8);
    }
  }
}

Grid::NodePoint* Grid::GetNodePoint(int id) {
  for (unsigned int i = 0; i < prison_points_.size(); i++) {
    if (prison_points_[i]->id_ == id) {
      return prison_points_[i];
    }
  }
  return nullptr;
}

bool Grid::fillGridByFile(const char* filename) {
  std::ifstream in(filename);

  if (!in.eof() && in.fail()){
    return false;
  }
  if (in.is_open()) {
    while (in.good()) {
      std::string aux;
      std::getline(in, aux);
      for (int i = 0; i < aux.size(); i++) {
        Node* auxNode = new Node();
        auxNode->value_ = aux[i];
        elements_.push_back(*auxNode);
      }
    }
  }
  in.close();

  for (int i = 0; i < size_y; i++) {
    for (int j = 0; j < size_x; j++) {

      elements_[(i * size_x) + j].position_.x = j * 8;
      elements_[(i * size_x) + j].position_.y = i * 8;
    }
  }
  return true;
}

bool Grid::fillGridByImage(const char* filename) {
  int width;
  int height;
  int number_of_channels;

  unsigned char* texture = stbi_load(filename, &width, &height, &number_of_channels, 0);

  size_x = width;
  size_y = height;
  for (int y = 0; y < size_y; y++) {
    for (int x = 0; x < size_x; x++) {

      unsigned bytePerPixel = number_of_channels;
      unsigned char* pixelOffset = texture + ((y * size_x) + x) * bytePerPixel;

      unsigned char r = pixelOffset[0];
      unsigned char g = pixelOffset[1];
      unsigned char b = pixelOffset[2];
     
      unsigned char a = number_of_channels >= 4 ? pixelOffset[3] : 0xff;

      Node* auxnode = new Node();

      if (r == 255) {
        auxnode->value_ = 0;
      }else {
       
        auxnode->value_ = 1;
      }

      elements_.push_back(*auxnode);
      delete(auxnode);
    }
  }
  for (int i = 0; i < size_y; i++) {
    for (int j = 0; j < size_x; j++) {
      elements_[(i * size_x) + j].position_.x = j * 8;
      elements_[(i * size_x) + j].position_.y = i * 8;
    }
  }

  chunk_valid = ESAT::SpriteFromFile("../img/chunk_good.png");
  chunk_not_valid = ESAT::SpriteFromFile("../img/chunk_bad.png");
  chunk_selected = ESAT::SpriteFromFile("../img/chunk_selected.png");
  return false;
}

void Grid::convertGridValues(const char def_value, const char new_value) {
  for (int i = 0; i < elements_.size(); i++) {
    if (elements_[i].value_ == def_value) {
      elements_[i].value_ = new_value;
    }
  }
}

void Grid::convertGridValue(int index, const char def_value, const char new_value) {
  if (elements_[index].value_ == def_value) {
    elements_[index].value_ = new_value;
  }
}

char Grid::getElement(int index) {
  return elements_[index].value_;
}

int Grid::calculateSpecificHeuristic(Node* origin, Node* destiny) {
  return (app::instance().absolute((destiny->position_.x - origin->position_.x)) + app::instance().absolute(((destiny->position_.y) - origin->position_.y)));
}

bool Grid::valueInGrid(float valueX, float valueY) {
  if (((valueX >= 0) && (valueX < size_x)) && ((valueY >= 0) && (valueY < size_y))) {
    if (elements_[(valueY * size_x) + valueX].value_ == 0) {
      return true;
    }
  }
  return false;
}

glm::vec2 Grid::grid_position(float x, float y) {
  glm::vec2 aux;
  aux.x = elements_[((int)(y/8) * size_x) + (int)(x/8)].position_.x;
  aux.y = elements_[((int)(y / 8) * size_x) + (int)(x / 8)].position_.y;
  return aux;
}

void Grid::print() {
  fopen_s(&file, "grid.txt", "w");
  if (file != NULL) {
    for (int i = 0; i < elements_.size(); i++) {
      char aux[1024];
      itoa(elements_[i].value_, aux, 10);    
      if (i != 0 && (i % size_x) == 0) {
        fputs("\n", file);
      }
      fputs(aux, file);
      fputs(" ", file);

    }   
  }
  fclose(file);
}

void Grid::save_position(float x, float y) {
  /*fopen_s(&file, "positions.txt", "w");
  if (file != NULL) {
    for (int i = 0; i < elements_.size(); i++) {
      char aux[1024];
      itoa(elements_[i].value_, aux, 10);
      if (i != 0 && (i % size_x) == 0) {
        fputs("\n", file);
      }
      fputs(aux, file);
      fputs(" ", file);

    }
  }
  fclose(file);*/
  //glm::vec2 aux2 = grid_cells(x, y);
  //pos_grid_saved_.push_back(aux2.x);
  //pos_grid_saved_.push_back(aux2.y);
  glm::vec2 aux = grid_position(x, y);
  position_saved_.push_back(aux.x);
  position_saved_.push_back(aux.y);

}

void Grid::write_positions() {
  fopen_s(&file, "positions.txt", "w");
  if (file != NULL) {
    for (int i = 0; i < position_saved_.size(); i++) {
      char aux[1024];
      char aux2[1024];
      itoa(position_saved_[i], aux, 10);
      itoa(pos_grid_saved_[i], aux2, 10);
      if (i != 0 && (i % 2) == 0) {
       fputs("\n", file);
      }
      fputs(aux, file);
      fputs("[", file);
      fputs(aux2, file);
      fputs("]", file);
      fputs(",", file);
    }
  }
  fclose(file);
}

void Grid::draw() {
  for (int i = 0; i < size_y; i++) {
    for (int j = 0; j < size_x; j++) {
      switch(elements_[(i * size_x) + j].value_) {
        case 0:
          ESAT::DrawSprite(chunk_valid, elements_[(i * size_x) + j].position_.x, elements_[(i * size_x) + j].position_.y);
          break;
        case 1:
          ESAT::DrawSprite(chunk_not_valid, elements_[(i * size_x) + j].position_.x, elements_[(i * size_x) + j].position_.y);
          break;
      }
    }
  }
}

void Grid::print_selected_cells() {
  for (int i = 0; i < position_saved_.size(); i+=2) {
    ESAT::DrawSprite(chunk_selected, position_saved_[i], position_saved_[i+1]);
  }
}