
/*
LARGE_INTEGER lp_end_performance_count;
LARGE_INTEGER frequency;

//get ticks per second
QueryPerformanceFrequency(&Frequency);

//start timer
QueryPerformanceCounter(&lp_end_performance_count);

//do something

//stop timer
QueryPerformanceCounter(&lp_end_performance_count);

elapsed_time = (lp_end_performance_count.QuadPart - &lp_end_performance_count)
milliseconds = elapsed_time * 1000.0 / frequency.QuadPart;

DWORD start_time = GetTickCount();
DWORD elapsed;

//do something

elapsed = GetTickCount() - star_time;

printf("%d.%3d seconds \n", elapsed / 1000, elapsed - elapsed / 1000);


*/

#include <stdio.h>
#include "app.h"
#include "time.h"
#include <ESAT/input.h>
#include <ESAT/window.h>
#include <ESAT/math.h>
#include <ESAT/draw.h>


int ESAT::main(int argc, char **argv)
{
  srand(time(NULL));
	app::instance().init();
	app::instance().start();
	app::instance().shutDown();
	return 0;
}
