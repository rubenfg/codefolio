#include <stdio.h>
#include <stdlib.h>
#include <guard.h>
#include <app.h>
#include <ESAT/draw.h>

Guard::Guard(){
}

Guard::~Guard(){
  
}

Soldier* Guard::soldierInRange(float perception_d) {
  for (int i = 0; i < 10; i++) {
    if (app::instance().Distance(*app::instance().soldierTest[i]->position(), *position()) < perception_d) {
      return app::instance().soldierTest[i];
    }
  }
  return nullptr;
}

Prisoner* Guard::prisonerInRange(float perception_d) {
  for (int i = 0; i < 10; i++) {
    if (app::instance().Distance(*app::instance().soldierTest[i]->position(), *position()) < perception_d) {
      return app::instance().prisonerTest[i];
    }
  }
  return nullptr;
}

Guard::DoorType Guard::doorInRange(bool checkOpen) {
    if (app::instance().Distance(ESAT::Vec3({ app::instance().map->door_a_point_.x,  app::instance().map->door_a_point_.y, 0.0f }),
      *position()) < door_perception_distance_) {
      return kDoorType_A;
    }
    if (app::instance().Distance(ESAT::Vec3({ app::instance().map->door_b_point_.x,  app::instance().map->door_b_point_.y, 0.0f }),
      *position()) < door_perception_distance_) {
      return kDoorType_B;
    }
  return kDoorType_None;
}

void Guard::init(const char* spritePath, ESAT::Vec3 position, float speed) {
  data_ = new AgentData();
  data_->velocity_ = new ESAT::Vec3();
  data_->position_ = new ESAT::Vec3();
  data_->target_ = new ESAT::Vec3();
  data_->path_ = new Path();
  set_speed(speed);
  set_sprite(spritePath);
  mainMindState_ = MindState::kmindState_Init;
  node_ = new Grid::NodePoint();
  node_ = app::instance().map->prison_points_[(rand() % app::instance().map->prison_points_.size())];
  ESAT::Vec3* a = new ESAT::Vec3();
  a->x = node_->position_.x;
  a->y = node_->position_.y;
  set_position(a);
  set_target(a);
}

void Guard::update(float deltaTime) {
  mind(deltaTime);
  body(deltaTime);
}

void Guard::FSM_init(float d_time) {
  changeToWork();
}

void Guard::FSM_work(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    int randm = rand() % app::instance().map->prison_points_[node_->id_]->nears_.size();
    std::vector<glm::vec2> aux_vector = app::instance().map->prison_points_[node_->id_]->paths_[randm];
    node_ = app::instance().map->prison_points_[node_->id_]->nears_[randm];
    for (int i = 0; i < aux_vector.size(); i++) {
      ESAT::Vec3* newPos = new ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f };
      path()->addPoint(*newPos, sizeof(ESAT::Vec3));
      delete(newPos);
    }
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }
    if (target_reached()) {
      if (path()->isLast()) {
        counter_stamina--;
     
      }
      path()->reset();
      if (counter_stamina <= 0) {
        changeToRest();
      }
    }
    soldier_target_ = soldierInRange(perception_distance_);
    if (soldier_target_ !=  nullptr) {
      changeToSuspicion();
    }
    if (app::instance().bAlarm) {
      changeToAlarm();
    }

    if (app::instance().map->doorsOpen()) {
      switch(doorInRange()){
      case kDoorType_A:
        if (app::instance().map->door_a_->open) {
          changeToCloseDoorA();
        }
        break;
      case kDoorType_B:
        if (app::instance().map->door_b_->open) {
          changeToCloseDoorB();
        }
        break;
      };
    }
}

void Guard::FSM_rest(float d_time) {
  movement_ = kMovementType_Resting;
  counter_resting_ -= d_time;
  if (counter_resting_ <= 0.0f) {
    changeToWork();
  }
  soldier_target_ = soldierInRange(perception_distance_);
  if (soldier_target_ != nullptr) {
    changeToSuspicion();
  }
}

void Guard::FSM_suspicion(float d_time) {
  movement_ = kMovementType_Deterministic;
  counter_update_suspicion_ -= d_time;
  if (path()->length() == 0) {
    counter_update_suspicion_ = time_update_suspicion_;
    for (int i = 0; i < soldier_target_->path()->points().size(); i++) {
      path()->addPoint(soldier_target_->path()->points()[i], sizeof(ESAT::Vec3));
    }
    if(bTakeCurrent_) path()->current_ = soldier_target_->path()->current_ - 2;
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }
  if ((app::instance().Distance(*soldier_target_->position(), *position()) <= 20.0f) && (counter_update_suspicion_ < 0.0f)) {
    counter_update_suspicion_ = time_update_suspicion_;
    path()->reset();
    bTakeCurrent_ = true;
    set_targetReached(false);
  }
  if (target_reached()) {
    path()->reset();
    bTakeCurrent_ = false;
  }
  if (app::instance().map->doorsOpen()) {
    switch (doorInRange()) {
    case kDoorType_None:
      break;
    case kDoorType_A:
      changeToCloseDoorA();
      break;
    case kDoorType_B:
      changeToCloseDoorB();
      break;
    };
  }
}

void Guard::FSM_closeDoorA(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    Path* aux = new Path();
    std::vector<glm::vec2> aux_vector;
    float pos_x = ((int)position()->x) / 8;
    float pos_y = ((int)position()->y) / 8;
    aux->calculatePath(glm::vec2({pos_x, pos_y}), glm::vec2({ 152.0f / 8, 232.0f / 8 }));
    aux_vector = std::move(aux->path_);
    for (unsigned int i = 0; i < aux_vector.size(); i++) {
      ESAT::Vec3* newpoint = new ESAT::Vec3();
      newpoint->x = aux_vector[i].x * 8;
      newpoint->y = aux_vector[i].y * 8;
      newpoint->z = 0.0f;
      path()->addPoint(*newpoint, sizeof(ESAT::Vec3));
    }
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }
  if (target_reached()) {
    app::instance().map->closeDoor(app::instance().map->door_a_);
    path()->reset();
    if (app::instance().bAlarm) {
      direction_ = kdirection_B;
      changeToAlarm();
    }else {
      app::instance().bAlarm = true;
      changeToGoToWork();
    }
  }
}

void Guard::FSM_closeDoorB(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    Path* aux = new Path();
    std::vector<glm::vec2> aux_vector;
    float pos_x = ((int)position()->x) / 8;
    float pos_y = ((int)position()->y) / 8;
    aux->calculatePath(glm::vec2({pos_x + 4.0f, pos_y + 4.0f}), glm::vec2({ 800.0f / 8, 272.0f / 8 }));
    aux_vector = std::move(aux->path_);
    for (unsigned int i = 0; i < aux_vector.size(); i++) {
      ESAT::Vec3* newpoint = new ESAT::Vec3();
      newpoint->x = aux_vector[i].x * 8;
      newpoint->y = aux_vector[i].y * 8;
      newpoint->z = 0.0f;
      path()->addPoint(*newpoint, sizeof(ESAT::Vec3));
    }
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }
  if (target_reached()) {
    app::instance().map->closeDoor(app::instance().map->door_b_);
    path()->reset();
    if (app::instance().bAlarm) {
      direction_ = kdirection_A;
      changeToAlarm();
    }else{
      app::instance().bAlarm = true;
      changeToGoToWork();
    }
  }
}

void Guard::FSM_goToWork(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    Path* aux = new Path();
    std::vector<glm::vec2> aux_vector;
    float pos_x = (int)position()->x / 8;
    float pos_y = (int)position()->y / 8;
    switch (doorInRange()) {
    case kDoorType_A:
      node_ = app::instance().map->GetNodePoint(1);
      aux->calculatePath(glm::vec2({ pos_x, pos_y }), glm::vec2({ node_->position_.x / 8, node_->position_.y / 8 }));
      aux_vector = std::move(aux->path_);
      for (unsigned int i = 0; i < aux_vector.size(); i++) {
        ESAT::Vec3* newpoint = new ESAT::Vec3();
        newpoint->x = aux_vector[i].x * 8;
        newpoint->y = aux_vector[i].y * 8;
        newpoint->z = 0.0f;
        path()->addPoint(*newpoint, sizeof(ESAT::Vec3));
      }
      break;
    case kDoorType_B:
      node_ = app::instance().map->GetNodePoint(20);
      aux->calculatePath(glm::vec2({ pos_x, pos_y }), glm::vec2({ node_->position_.x / 8, node_->position_.y / 8 }));
      aux_vector = std::move(aux->path_);
      for (unsigned int i = 0; i < aux_vector.size(); i++) {
        ESAT::Vec3* newpoint = new ESAT::Vec3();
        newpoint->x = aux_vector[i].x * 8;
        newpoint->y = aux_vector[i].y * 8;
        newpoint->z = 0.0f;
        path()->addPoint(*newpoint, sizeof(ESAT::Vec3));
      }
      break;
    };
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }
  if (target_reached()) {
    changeToInit();
  }
}

void Guard::FSM_alarm(float d_time) {
  movement_ = kMovementType_Deterministic;

  switch (alarmMindState_) {
  case kmindState_Init:
    FSM_alarmInit(d_time);
    break;
  case kmindState_GoA:
    FSM_goA(d_time);
    break;
  case kmindState_GoB:
    FSM_goB(d_time);
    break;
  case kmindState_DoorToDoor:
    FSM_doorToDoor(d_time);
    break;
  case kmindState_Track:
    FSM_Track(d_time);
    break;
  };
}

void Guard::FSM_alarmInit(float d_time) {
  app::instance().counter_alarm = app::instance().time_alarm_;
  int rdn = rand() % 2;
  switch (rdn) {
  case 0:
    alarmMindState_ = kmindState_GoA;
    break;
  case 1:
    alarmMindState_ = kmindState_GoB;
    break;
  }

}

void Guard::FSM_goA(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    Path* aux = new Path();
    std::vector<glm::vec2> aux_vector;
    float pos_x = (int)position()->x / 8;
    float pos_y = (int)position()->y / 8;
    aux->calculatePath(glm::vec2({ pos_x, pos_y}), glm::vec2({ 152.0f / 8 , 232.0f / 8 }));
    aux_vector = std::move(aux->path_);
    for (unsigned int i = 0; i < aux_vector.size(); i++) {
      ESAT::Vec3* newpoint = new ESAT::Vec3();
      newpoint->x = aux_vector[i].x * 8;
      newpoint->y = aux_vector[i].y * 8;
      newpoint->z = 0.0f;
      path()->addPoint(*newpoint, sizeof(ESAT::Vec3));
    }
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }
  if (soldier_target_ = soldierInRange(perception_distance_)) {
    changeToAlarmTrack();
  }
  if (target_reached()) {
    app::instance().map->closeDoor(app::instance().map->door_a_);
    changeToAlarmDoorToDoor();
    direction_ = kdirection_B;
  }
}

void Guard::FSM_goB(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    Path* aux = new Path();
    std::vector<glm::vec2> aux_vector;
    float pos_x = (int)position()->x / 8;
    float pos_y = (int)position()->y / 8;
    aux->calculatePath(glm::vec2({ pos_x, pos_y}), glm::vec2({ 800.0f / 8 , 272.0f / 8 }));
    aux_vector = std::move(aux->path_);
    for (unsigned int i = 0; i < aux_vector.size(); i++) {
      ESAT::Vec3* newpoint = new ESAT::Vec3();
      newpoint->x = aux_vector[i].x * 8;
      newpoint->y = aux_vector[i].y * 8;
      newpoint->z = 0.0f;
      path()->addPoint(*newpoint, sizeof(ESAT::Vec3));
    }
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }
  if (soldier_target_ = soldierInRange(perception_distance_)) {
    changeToAlarmTrack();
  }
  if (target_reached()) {
    app::instance().map->closeDoor(app::instance().map->door_b_);
    changeToAlarmDoorToDoor();
    direction_ = kdirection_A;
  }
}

void Guard::FSM_doorToDoor(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    std::vector<glm::vec2> aux_vector;
    switch (direction_) {
    case kdirection_B:
      aux_vector = app::instance().map->dtd_g_b_;
      for (int i = 0; i < aux_vector.size(); i++) {
        path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
      }

      path()->set_action(Path::kActionStraight);
      set_targetReached(false);
      direction_ = kdirection_B;

      break;
    case kdirection_A:
      aux_vector = app::instance().map->dtd_g_b_;
      for (int i = aux_vector.size() - 1; i >=0 ; i--) {
        path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
      }

      path()->set_action(Path::kActionStraight);
      set_targetReached(false);
      direction_ = kdirection_A;
      break;
    }
  };
  if ((soldier_target_ = soldierInRange(perception_distance_)) && !soldier_target_->bFinish) {
    changeToAlarmTrack();
  }

  if (target_reached()) {
    path()->reset();
    switch (direction_) {
    case kdirection_B:
      app::instance().map->closeDoor(app::instance().map->door_b_);
      direction_ = kdirection_A;
      break;
    case kdirection_A:
      app::instance().map->closeDoor(app::instance().map->door_a_);
      direction_ = kdirection_B;
      break;
    }

    if ((app::instance().counter_alarm <= 0.0f) && (soldierInRange(perception_distance_) == nullptr)) {
      changeToGoToWork();
      app::instance().bAlarm = false;
    }
  }
}

void Guard::FSM_Track(float d_time) {
  movement_ = kMovementType_Deterministic;
  counter_update_suspicion_ -= d_time;
  if (path()->length() == 0) {
    counter_update_suspicion_ = time_update_suspicion_;
    for (int i = 0; i < soldier_target_->path()->points().size(); i++) {
      path()->addPoint(soldier_target_->path()->points()[i], sizeof(ESAT::Vec3));
    }
    if (bTakeCurrent_) path()->current_ = soldier_target_->path()->current_ - 1;
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }
  if ((app::instance().Distance(*soldier_target_->position(), *position()) <= 20.0f) && (counter_update_suspicion_ < 0.0f)) {
    counter_update_suspicion_ = time_update_suspicion_;
    path()->reset();
    bTakeCurrent_ = true;
    set_targetReached(false);
  }
  if (target_reached()) {
    path()->reset();
    bTakeCurrent_ = false;
  }
  if (app::instance().map->doorsOpen()) {
    switch (doorInRange()) {
    case kDoorType_None:
      break;
    case kDoorType_A:
      changeToCloseDoorA();
      break;
    case kDoorType_B:
      changeToCloseDoorB();
      break;
    };
  }
}

void Guard::FSM_End(float d_time) {
  set_target(position());
}

void Guard::mind(float d_time) {
  switch (mainMindState_) {
  case kmindState_Init:
    FSM_init(d_time);
    break;
  case kmindState_Work:
    FSM_work(d_time);
    break;
  case kmindState_Rest:
    FSM_rest(d_time);
    break;
  case kmindState_Suspicion:
    FSM_suspicion(d_time);
    break;
  case kmindState_closeDoorA:
    FSM_closeDoorA(d_time);
    break;
  case kmindState_closeDoorB:
    FSM_closeDoorB(d_time);
    break;
  case kmindState_goToWork:
    FSM_goToWork(d_time);
    break;
  case kmindState_alarm:
    FSM_alarm(d_time);
    break;
  };
}

void Guard::body(float d_time) {
  ESAT::Vec3* vel = new ESAT::Vec3();

  vel->x = target()->x - position()->x;
  vel->y = target()->y - position()->y;
  vel->z = target()->z - position()->z;

  *vel = app::instance().Normalize(vel);
  set_velocity(vel->x * data_->speed_, vel->y * data_->speed_, vel->z * data_->speed_);

  switch (movement_) {
  case kMovementType_Deterministic:
    MOV_deterministic(d_time);
    break;
  case kMovementType_Resting:
    MOV_resting(d_time);
    break;
  case kMovementType_Tracker:
    MOV_tracker(d_time);
    break;
  }
  if(!target_reached()) move(d_time);
}

void Guard::move(float d_time) {
  position()->x = position()->x + velocity()->x * d_time / 1000.0f;
  position()->y = position()->y + velocity()->y * d_time / 1000.0f;
}

void Guard::MOV_deterministic(float d_time) {
  if (path()->length() > 0) {
    if (!target_reached()) {
      set_target(&path()->actualPoint());
    }
    if (app::instance().Distance(*target(), *position()) <= data_->distance_offset_) {
      if (path()->isLast()) {
        set_targetReached(true);
      }
      else {
        path()->nextPoint();
      }
    }
  }else {
    set_target(position());
  }
}

void Guard::MOV_resting(float d_time) {
  set_target(position());
  set_targetReached(true);
}

void Guard::MOV_tracker(float d_time) {
  set_target(soldier_target_->target());
}

void Guard::draw() {
  ESAT::DrawSprite(data_->sprite_, data_->position_->x, data_->position_->y);
  if (0) {
    ESAT::DrawSetStrokeColor(255.0f, 255.0f, 255.0f, 255.0f);
    ESAT::DrawLine(position()->x, position()->y, position()->x + perception_distance_, position()->y);
    ESAT::DrawLine(position()->x, position()->y, position()->x - perception_distance_, position()->y);
    ESAT::DrawLine(position()->x, position()->y, position()->x, position()->y + perception_distance_);
    ESAT::DrawLine(position()->x, position()->y, position()->x, position()->y - perception_distance_);
  }
}

//###############################################################################
//                                  STATE CHANGES
//###############################################################################

void Guard::changeToWork() {
  mainMindState_ = kmindState_Work;
  counter_stamina = max_stamina_;
}

void Guard::changeToRest() {
  mainMindState_ = kmindState_Rest;
  path()->reset();
  counter_stamina = max_stamina_;
  counter_resting_ = time_resting_;
}

void Guard::changeToSuspicion() {
  path()->reset();
  mainMindState_ = kmindState_Suspicion;
}

void Guard::changeToAlarm() {
  mainMindState_ = kmindState_alarm;
  alarmMindState_ = kmindState_Init;
  path()->reset();
}

void Guard::changeToCloseDoorA() {
  mainMindState_ = kmindState_closeDoorA;
  path()->reset();
}

void Guard::changeToCloseDoorB() {
  mainMindState_ = kmindState_closeDoorB;
  path()->reset();
}

void Guard::changeToAlarmDoorToDoor() {
  path()->reset();
  alarmMindState_ = kmindState_DoorToDoor;
}

void Guard::changeToAlarmTrack() {
  path()->reset();
  alarmMindState_ = kmindState_Track;
}

void Guard::changeToAlarmInit() {
  alarmMindState_ = kmindState_Init;
}

void Guard::changeToGoToWork() {
  path()->reset();
  mainMindState_ = kmindState_goToWork;
}

void Guard::changeToInit() {
  path()->reset();
  mainMindState_ = kmindState_Init;
}

//###############################################################################
//                                  SETTERS
//###############################################################################

void Guard::set_target(ESAT::Vec3* target) {
  data_->target_->x = target->x;
  data_->target_->y = target->y;
  data_->target_->z = target->z;
}

void Guard::set_path(Path* path) {
  data_->path_ = path;
}

void Guard::set_position(ESAT::Vec3* position) {
  data_->position_->x = position->x;
  data_->position_->y = position->y;
  data_->position_->z = position->z;
}

void Guard::set_targetReached(bool target_reached) {
  data_->target_reached_ = target_reached;
}

void Guard::set_velocity(float x, float y, float z) {
  data_->velocity_->x = x;
  data_->velocity_->y = y;
  data_->velocity_->z = z;
}

void Guard::set_sprite(const char* filename) {
  data_->sprite_ = ESAT::SpriteFromFile(filename);
}

void Guard::set_speed(float speed) {
  data_->speed_ = speed;
}

//###############################################################################
//                                  GETTERS
//###############################################################################

ESAT::Vec3* Guard::target() {
  return data_->target_;
}

Path* Guard::path() {
  return data_->path_;
}

ESAT::Vec3* Guard::position() {
  return data_->position_;
}

bool Guard::target_reached() {
  return data_->target_reached_;
}

ESAT::Vec3* Guard::velocity() {
  return data_->velocity_;
}

float Guard::speed() {
  return data_->speed_;
}
