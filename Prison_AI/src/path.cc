#include "stdio.h"
#include "path.h"
#include "grid.h"
#include "math.h"
#include "app.h"
#include "common_def.h"
#include <string>
#include <iostream>     // std::cout
#include <fstream> 
#include <chrono>
#include <ctime>

Path::Path() {
  current_ = 0;
  current_loop_ = 0;
  num_loops_ = 0;
  action_ = kActionNone;
  direction_ = kForward;
  total_ = 0;
  offset_ = 0;
}

Path::~Path() {

}

int absolute(int value) {
  if (value < 0) {
    return (value * (-1));
  }
  return value;
}


void Path::reset() {
  current_ = 0;
  current_loop_ = 0;
  direction_ = kForward;
  action_ = kActionNone;
  total_ = 0;
  offset_ = 0;
  points_.clear();
}
void Path::addPoint(ESAT::Vec3 point, u16 size) {
  points_.push_back(point);
  total_++;
}

bool Path::isLast() {
  if (current_ == 0 && direction_ == kBackward) return true;
  if (total_ == current_ + 1 && direction_ == kForward) return true;
  return false;
}

ESAT::Vec3 Path::actualPoint() {
  return points_.at(current_);
}

int Path::direction() {
  return direction_;
}

ESAT::Vec3 Path::reestartPath() {
  current_ = 0;
  return points_.at(current_);
}

void Path::actionStraight() {
  if (!isLast()) current_ = current_ + direction_;
}

void Path::actionLoopNTimes() {
  if (isLast() && direction_ == kBackward) {
    if (num_loops_ > current_loop_) current_loop_++;
    direction_ = kForward;
  }
  if (isLast() && direction_ == kForward) direction_ = kBackward;
  if (current_loop_ < num_loops_) current_ = current_ + direction_;
}

void Path::actionLoopInfinite() {
  if (isLast() && direction_ == kBackward) direction_ = kForward;
  if (isLast() && direction_ == kForward) direction_ = kBackward;
  current_ = current_ + direction_;
}

ESAT::Vec3 Path::nextPoint() {
  switch (action_) {
  case kActionNone:
    break;
  case kActionStraight:
    actionStraight();
    break;
  case kActionLoopNTimes:
    actionLoopNTimes();
    break;
  case kActionLoopInfinite:
    actionLoopInfinite();
    break;
  }
  return points_.at(current_);
}
void Path::set_direction(Direction direction) {
  direction_ = direction;
}
int Path::length() {
  return total_;
}

void Path::set_action(Action action) {
  action_ = action;
}

void Path::set_action(Action action, u16 loops) {
  action_ = action;
  num_loops_ = loops;
}

void Path::print() {
  ESAT::Vec3 auxVec;
  for (int i = 0; i < total_; i++) {
    auxVec = points_.at(i);
    printf("\nNODE %d", i);
    printf("\nvecx: %f", auxVec.x);
    printf("\nvecy: %f", auxVec.y);
  }
}

int Path::calculateHeuristic(Node* origin, Node* destiny, bool type) {
  if (type) {
    glm::vec2 direction;
    direction.x = destiny->position_.x - origin->position_.x;
    direction.y = destiny->position_.y - origin->position_.y;
    return sqrt((direction.x * direction.x) + (direction.y * direction.y));
  }else {
    return (int)(absolute((destiny->position_.x - origin->position_.x)) + absolute(((destiny->position_.y) - origin->position_.y)));
  }
  return 0;
}

std::vector<ESAT::Vec3> Path::points() {
  return points_;
}

void Path::calculatePath(glm::vec2 origin, glm::vec2 destiny) {

  std::unordered_map<glm::vec2, Node*> open_list_;
  std::unordered_map<glm::vec2, Node*> closed_list_;

  Node* node_goal = new Node();
  node_goal->position_.x = destiny.x;
  node_goal->position_.y = destiny.y;
  Node* node_start = new Node();
  node_start->position_.x = origin.x;
  node_start->position_.y = origin.y;
  node_start->g = 0;
  node_start->h = 0;
  node_start->f = node_start->g + node_start->h;

  std::pair<glm::vec2, Node*> first_node(node_start->position_, node_start);
  open_list_.insert(first_node);
  Node* node_current = nullptr;

  while (!open_list_.empty()) {

    node_current = open_list_.begin()->second;
    for (auto element : open_list_) {
      if (node_current->f >= element.second->f) {
        node_current = element.second;
      }
    }
    open_list_.erase(node_current->position_);
    if(node_current->position_ == node_goal->position_){
        Node* aux = node_current;
        while (aux->parent_ != nullptr) {
          path_.insert(path_.begin(), aux->position_);
          aux = aux->parent_;
        }
         path_.insert(path_.begin(), aux->position_);
        return;
      }
    
      for (int y = -1; y <= 1; y++) {
        for (int x = -1; x <= 1; x++) {
         
          if (x == 0 && y == 0) {
            continue;
          }
          if (!app::instance().map->valueInGrid(node_current->position_.x + x, node_current->position_.y + y)) {
            continue;
          }
          Node* aux_node = new Node();
          if ((y * x) != 0) {
            aux_node->g = 14;
          }else {
            aux_node->g = 10;
          }
          aux_node->g = aux_node->g + node_current->g;
          aux_node->position_.x = node_current->position_.x + x;
          aux_node->position_.y = node_current->position_.y + y;

          if (open_list_.count(aux_node->position_)) {
            if (open_list_[aux_node->position_]->g <= aux_node->g) {
              delete(aux_node);
              continue;
            }else {
              open_list_.erase(aux_node->position_);
            }
          }
          if (closed_list_.count(aux_node->position_)) {
            if (closed_list_[aux_node->position_]->g <= aux_node->g) {
              delete(aux_node);
              continue;
            }
            else {
              closed_list_.erase(aux_node->position_);
            }
          }

          aux_node->parent_ = node_current;
          aux_node->h = calculateHeuristic(aux_node, node_goal, 1) * 10;
          aux_node->f = aux_node->g + aux_node->h;
          std::pair<glm::vec2, Node*> sucessor(aux_node->position_, aux_node);
          open_list_.insert(sucessor);
        }
      }
      std::pair<glm::vec2, Node*> current(node_current->position_, node_current);
      closed_list_.insert(current);
  }
}
