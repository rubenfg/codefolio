#include "stdio.h"
#include "math.h"
#include "app.h"
#include "agent.h"
#include "prisoner.h"
#include "guard.h"
#include "common_def.h"
#include <ESAT/time.h>
#include <ESAT/input.h>
#include <ESAT/window.h>
#include <ESAT/math.h>
#include <ESAT/draw.h>
#include <ESAT/sprite.h>
#include <grid.h>
#include <path.h>

app::app(){
}

bool app::init(){
	ESAT::WindowInit(kMax_width, kMax_height);
  map = new Grid();
 
  wallpaper = ESAT::SpriteFromFile("../resources/map_tex.bmp");
  alert_wp = ESAT::SpriteFromFile("../img/sapoalarma.png");
  chunk = ESAT::SpriteFromFile("../img/chunk.png");
  chunk_mouse = ESAT::SpriteFromFile("../img/cross.png");
  chunk_bad = ESAT::SpriteFromFile("../img/chunk_bad.png");
  chunk_good = ESAT::SpriteFromFile("../img/chunk_good.png");
 
  map->init();
  map->fillGridByImage("../resources/cost120.png");
  map->generateGeneralPaths();
  map->generateNodePointsPaths();

  for (int i = 0; i < 10; i++) {
    guardTest[i] = new Guard();
    guardTest[i]->init("../img/toad_green.png", ESAT::Vec3{ 200.0f, 200.0f, 0.0f }, 175.0f);
  }

  for (int i = 0; i < 10; i++) {
    prisonerTest[i] = new Prisoner();
    prisonerTest[i]->init("../img/toad_red.png", ESAT::Vec3{ 200.0f, 200.0f, 0.0f }, 250.0f);
    if (i >= 5) {
      prisonerTest[i]->set_startWorking(true);
    }
    soldierTest[i] = new Soldier();
    soldierTest[i]->init("../img/toad_blue.png", ESAT::Vec3{ (float)(rand() % (64 - 24 + 1) + 24), (float)(rand() % (688 - 648 + 1) + 648) , 0.0f }, 250.0f);
  }
	return true;
}

void app::shutDown(){
	ESAT::WindowDestroy();
}

void app::start(){
	float current_time = ESAT::Time();//SDL_GetTicks();
	float dt = 0;
	float time_step_ = 16;
	while ( false == quit_game_ ) {
		inputService();
		float accum_time = ESAT::Time() - current_time;;//SDL_GetTicks() – current_time;
			while ( accum_time >= time_step_ ) {
				update(time_step_);
				current_time += time_step_;
				accum_time = ESAT::Time() - current_time;//SDL_GetTicks() – current_time;
			}
		draw();
	}
}

void app::update(float time){
  for (int i = 0; i < 10; i++) {
    prisonerTest[i]->update(time);
    guardTest[i]->update(time);
    if (!bStopSoldiers) {
      soldierTest[i]->update(time);
    }
  }
  if (bAlarm) {
    counter_alarm -= time;
  }
  
}

void app::draw(){
	ESAT::DrawBegin();
	ESAT::WindowSetMouseVisibility(1);
	ESAT::DrawClear(0,0,0);//Borrado y color de fondo rgb.
  ESAT::DrawSprite(wallpaper, 0.0f, 0.0f);
  
  map->print_selected_cells();
  if ((mouse_position.x >= 0 && mouse_position.x < 960) && (mouse_position.y >= 0 && mouse_position.y < 704)) {
    glm::vec2 aux = map->grid_position(mouse_position.x, mouse_position.y);
    ESAT::DrawSprite(chunk_mouse, aux.x, aux.y);
  }

  for (int i = 0; i < map->door_a_->door_position_.size(); i++) {
    if (map->door_a_->open) {
      ESAT::DrawSprite(chunk_good, map->door_a_->door_position_[i].x * 8, map->door_a_->door_position_[i].y * 8);
    }else {
      ESAT::DrawSprite(chunk_bad, map->door_a_->door_position_[i].x * 8, map->door_a_->door_position_[i].y * 8);
    }
  }

  for (int i = 0; i < map->door_b_->door_position_.size(); i++) {
    if (map->door_b_->open) {
      ESAT::DrawSprite(chunk_good, map->door_b_->door_position_[i].x * 8, map->door_b_->door_position_[i].y * 8);
    }else {
      ESAT::DrawSprite(chunk_bad, map->door_b_->door_position_[i].x * 8, map->door_b_->door_position_[i].y * 8);
    }
  }

  for (int i = 0; i < 10; i++) {
    prisonerTest[i]->draw();
  }
  for (int i = 0; i < 10; i++) {
    guardTest[i]->draw();
    soldierTest[i]->draw();
  }

  if (bAlarm) {
    ESAT::DrawSprite(alert_wp, 0.0f, 0.0f);
  }

	ESAT::DrawEnd();
	ESAT::WindowFrame();
}

void app::inputService(){
	mouse_position.x = ESAT::MousePositionX();
	mouse_position.y = ESAT::MousePositionY();
	mouse_position.z = 0.0f;
  if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_F1)) {
    map->save_position(mouse_position.x, mouse_position.y);
  }
  if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_F2)) {
    map->write_positions();
  }
  if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_F3)) {
    glm::vec2 aux = map->grid_position(mouse_position.x, mouse_position.y);
    printf("\n[%f, %f]", aux.x, aux.y);
  }
  if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_F4)) {
    bAlarm = true;
  }
  if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_F5)) {
    bAlarm = false;
  }

  if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_F6)) {
    if (!bStopSoldiers) {
      bStopSoldiers = true;
    }else {
      bStopSoldiers = false;
    }

  }
}

int app::absolute(int value) {
  if (value < 0) {
    value = value * (-1);
  }
  return value;
}

float Length(ESAT::Vec3 vector) {
  return sqrt(vector.x*vector.x + vector.y*vector.y + vector.z*vector.z);
}

ESAT::Vec3 app::Normalize(ESAT::Vec3* vector) {
  ESAT::Vec3 newVector;
  newVector.x = 0.0f;
  newVector.y = 0.0f;
  newVector.z = 0.0f;

  float length = Length(*vector);

  if (length != 0) {
    newVector.x = vector->x / length;
    newVector.y = vector->y / length;
    newVector.z = vector->z / length;
  }
  return newVector;
}

float app::Distance(ESAT::Vec3 pos1, ESAT::Vec3 pos2) {
  ESAT::Vec3 distance;
  distance.x = pos2.x - pos1.x;
  distance.y = pos2.y - pos1.y;
  distance.z = pos2.z - pos1.z;
  return sqrt((distance.x * distance.x) + (distance.y * distance.y) + (distance.z * distance.z));
}