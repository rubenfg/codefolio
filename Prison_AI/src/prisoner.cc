#include <stdio.h>
#include <stdlib.h>
#include <prisoner.h>
#include <app.h>


Prisoner::Prisoner(){
 
  
}

Prisoner::~Prisoner(){
  
}

void Prisoner::init(const char* spritePath, ESAT::Vec3 position, float speed) {
  data_ = new AgentData();
  data_->velocity_ = new ESAT::Vec3();
  data_->position_ = new ESAT::Vec3();
  data_->target_ = new ESAT::Vec3();
  data_->path_ = new Path();
  set_speed(speed);
  set_sprite(spritePath);
  mainMindState_ = MindState::kmindState_Init;
  set_position(new ESAT::Vec3{ (float)(rand() % (776 - 536 + 1) + 536), (float)(rand() % (544 - 472 + 1) + 472), 0.0f});
  counter_rest_time_ = rest_time_;
  loading_speed_ = speed / 2;
  default_speed_ = speed;
}

void Prisoner::update(float deltaTime) {
  mind(deltaTime);
  body(deltaTime);
}

void Prisoner::FSM_init(float d_time) {
  if (bStart_working_) {
    changeToWork();
  }else {
    changeToRest();
  }
}

void Prisoner::FSM_rest(float d_time) {
  state_ = kResting;
  movement_ = kMovementType_Random;
  counter_rest_time_ -= d_time;
  if (path()->length() == 0) {
    glm::vec2 aux = app::instance().map->grid_position((rand() % (776 - 536 + 1) + 536), (rand() % (544 - 472 + 1) + 472));
    ESAT::Vec3* newPos = new ESAT::Vec3{ aux.x, aux.y, 0.0f };
    path()->addPoint(*newPos, sizeof(ESAT::Vec3));
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }
  if (target_reached()) {
    glm::vec2 aux = app::instance().map->grid_position((rand() % (776 - 536 + 1) + 536), (rand() % (544 - 472 + 1) + 472));
    ESAT::Vec3* newPos = new ESAT::Vec3{ aux.x, aux.y, 0.0f };
    path()->addPoint(*newPos, sizeof(ESAT::Vec3));
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }
  if (counter_rest_time_ <= 0.0f && app::instance().map->CheckFreeBox()) {
    changeToWork();
    counter_rest_time_ = rest_time_;
  }
  if (app::instance().bAlarm) {
    changeToEscape();
  }
}

void Prisoner::FSM_work(float d_time) {
  state_ = kWorking;
  counter_work_time_ -= d_time;
  if (app::instance().bAlarm) {
    box_->bOcuped_ = false;
    
    changeToEscape();
  }
  switch (workMindState_) {
    case kmindState_Init:
      FSM_workInit(d_time);
      break;
    case kmindState_goToWork:
      FSM_goToWork(d_time);
      break;
    case kmindState_Load:
      FSM_Load(d_time);
      break;
    case kmindState_GotoUnload:
      FSM_GoToUnload(d_time);
      break;
    case kmindState_Unload:
      FSM_Unload(d_time);
      break;
    case kmindState_GotoLoad:
      FSM_GoToLoad(d_time);
      break;
    case kmindState_GoToRest:
      FSM_goToRestFromWork(d_time);
      break;
    };
}

void Prisoner::FSM_workInit(float d_time) {
  changeToWorkGoToWork();
}

void Prisoner::FSM_goToWork(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    path()->addPoint(ESAT::Vec3({560.0f, 480.0f, 0.0f}), sizeof(ESAT::Vec3));
    path()->addPoint(ESAT::Vec3({560.0f, 464.0f, 0.0f}), sizeof(ESAT::Vec3));
    bool rnd = rand() % 2;
    if (rnd) {
      path()->addPoint(ESAT::Vec3({608.0f, 392.0f, 0.0f}), sizeof(ESAT::Vec3));
    }else {
      path()->addPoint(ESAT::Vec3({464.0f, 392.0f, 0.0f}), sizeof(ESAT::Vec3));
    }
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }
  if (target_reached()) {
    changeToWorkLoad();
  }
}

void Prisoner::FSM_goToRestFromWork(float d_time) {
  movement_ = kMovementType_Deterministic;
  if(path()->length() == 0){
    bool rnd = rand() % 2;
    if (rnd) {
      path()->addPoint(ESAT::Vec3({608.0f, 392.0f, 0.0f}), sizeof(ESAT::Vec3));
    }else {
      path()->addPoint(ESAT::Vec3({ 464.0f, 392.0f, 0.0f }), sizeof(ESAT::Vec3));
    }
    path()->addPoint(ESAT::Vec3({ 560.0f, 464.0f, 0.0f }), sizeof(ESAT::Vec3));
    path()->addPoint(ESAT::Vec3({ 560.0f, 480.0f, 0.0f }), sizeof(ESAT::Vec3));
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }

  if (target_reached()) {
    box_->bOcuped_ = false;
    changeToRest();
  }
}

void Prisoner::FSM_Load(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    path()->addPoint(box_->initial_position_, sizeof(ESAT::Vec3));
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }

  if(target_reached()){
    changeToWorkGoToUnload();
  }
}

void Prisoner::FSM_GoToUnload(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    set_speed(loading_speed_);
    path()->addPoint(box_->final_position_, sizeof(ESAT::Vec3));
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }

  if(target_reached()){
    changeToWorkUnload();
  }
}

void Prisoner::FSM_Unload(float d_time) {
  movement_ = kMovementType_Resting;
  counter_time_unloading_ -= d_time;
  set_targetReached(true);
  if (counter_time_unloading_ < 0.0f) {
    changeToWorkGoToLoad();
    if (counter_work_time_ <= 0.0f) {
      changeToWorkGoToRest();
    }
  }
  
}

void Prisoner::FSM_GoToLoad(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    set_speed(default_speed_);
    path()->addPoint(box_->initial_position_, sizeof(ESAT::Vec3));
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }
  if(target_reached()){
    changeToWorkLoad();
  }
}

void Prisoner::FSM_escape(float d_time) {
  switch (escapeMindState_) {
  case kmindState_Init:
    FSM_escapeInit(d_time);
    break;
  case kmindState_GoB:
    FSM_goB(d_time);
    break;
  case kmindState_DoorToDoor:
    FSM_doorToDoor(d_time);
    break;
  case kmindState_goBase:
    FSM_goBase(d_time);
    break;
  };
}

void Prisoner::FSM_escapeInit(float d_time) {
  changeToEscapeGoB();
}

void Prisoner::FSM_goB(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    switch (state_) {
    case kResting:
      path()->reset();
      path()->addPoint(ESAT::Vec3({ 560.0f, 480.0f, 0.0f }), sizeof(ESAT::Vec3));
      path()->addPoint(ESAT::Vec3({ 560.0f, 464.0f, 0.0f }), sizeof(ESAT::Vec3));
      path()->addPoint(ESAT::Vec3({ 608.0f, 392.0f, 0.0f }), sizeof(ESAT::Vec3));
      path()->addPoint(ESAT::Vec3({ 696.0f, 264.0f, 0.0f }), sizeof(ESAT::Vec3));
      path()->addPoint(ESAT::Vec3({ 800.0f, 272.0f, 0.0f }), sizeof(ESAT::Vec3));
      path()->set_action(Path::kActionStraight);
      set_targetReached(false);
      break;
    case kWorking:
      path()->reset();
      path()->addPoint(ESAT::Vec3({ 696.0f, 264.0f, 0.0f }), sizeof(ESAT::Vec3));
      path()->addPoint(ESAT::Vec3({ 800.0f, 272.0f, 0.0f }), sizeof(ESAT::Vec3));
      path()->set_action(Path::kActionStraight);
      set_targetReached(false);
      break;
    };
  }
  if (target_reached()) {
    changeToEscapeDoorToDoor();
    direction_ = kdirection_A;
  }

}

void Prisoner::FSM_doorToDoor(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    std::vector<glm::vec2> aux_vector;
    switch (direction_) {
    case kdirection_B:
      aux_vector = app::instance().map->dtd_g_b_;
      for (int i = 0; i < aux_vector.size(); i++) {
        path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
      }

      path()->set_action(Path::kActionStraight);
      set_targetReached(false);
      direction_ = kdirection_B;
      if (app::instance().map->door_a_->open) {
        changeToEscapeGoBase();
        direction_ = kdirection_A;
      }
      break;
    case kdirection_A:
      aux_vector = app::instance().map->dtd_g_a_;
      for (int i = 0; i < aux_vector.size(); i++) {
        path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
      }

      path()->set_action(Path::kActionStraight);
      set_targetReached(false);
      direction_ = kdirection_A;
      if (app::instance().map->door_b_->open) {
        changeToEscapeGoBase();
        direction_ = kdirection_B;
      }
      break;
    }
  }
  if (target_reached()) {
    path()->reset();
    set_targetReached(false);
    switch (direction_){
    case kdirection_A:
      if (app::instance().map->door_a_->open) {
        changeToEscapeGoBase();
      }else {
        direction_ = kdirection_B;
      }
      break;
    case kdirection_B:
      if (app::instance().bAlarm) {
        if (app::instance().map->door_b_->open) {
          changeToEscapeGoBase();
        }
        else {
          direction_ = kdirection_A;
        }
      }else {
        changeToGoToRestFB();
      }
      break;
    }
  }
}

void Prisoner::FSM_goBase(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    std::vector<glm::vec2> aux_vector;
    switch (direction_) {
    case kdirection_B:
      aux_vector = app::instance().map->b_go_baseb_;
      for (int i = 0; i < aux_vector.size(); i++) {
        path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
      }
      path()->set_action(Path::kActionStraight);
      set_targetReached(false);
      break;
    case kdirection_A:
      aux_vector = app::instance().map->a_go_basea_;
      for (int i = 0; i < aux_vector.size(); i++) {
        path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
      }
      path()->set_action(Path::kActionStraight);
      set_targetReached(false);
      break;
    }
  }
  if (target_reached()) {
    bFree_ = true;
    changeToEnd();
  }
}

void Prisoner::FSM_GoRest(float d_time) {
  movement_ = kMovementType_Deterministic;
  if (path()->length() == 0) {
    std::vector<glm::vec2> aux_vector;
    aux_vector = app::instance().map->a_to_rest_;
    for (int i = 0; i < aux_vector.size(); i++) {
      path()->addPoint(ESAT::Vec3{ aux_vector[i].x * 8, aux_vector[i].y * 8, 0.0f }, sizeof(ESAT::Vec3));
    }
    path()->set_action(Path::kActionStraight);
    set_targetReached(false);
  }

  if (target_reached()) {
    switch (state_) {
    case kResting:
      changeToRest();
      break;
    case kWorking:
      changeToWork();
      break;
    }
    path()->reset();
  }
}

void Prisoner::FSM_End(float d_time) {
  set_target(position());
}

void Prisoner::mind(float d_time) {
  switch (mainMindState_) {
  case kmindState_Init:
    FSM_init(d_time);
    break;
  case kmindState_Work:
    FSM_work(d_time);
    break;
  case kmindState_Rest:
    FSM_rest(d_time);
    break;
  case kmindState_Escape:
    FSM_escape(d_time);
    break;
  case kmindState_GoToRestFB:
    FSM_GoRest(d_time);
    break;
  case kmindState_End:
    FSM_End(d_time);
    break;
  };
}

void Prisoner::body(float d_time) {
  ESAT::Vec3* vel = new ESAT::Vec3();

  vel->x = target()->x - position()->x;
  vel->y = target()->y - position()->y;
  vel->z = target()->z - position()->z;
  
  *vel = app::instance().Normalize(vel);
  set_velocity(vel->x * data_->speed_, vel->y * data_->speed_, vel->z * data_->speed_);

  switch (movement_) {
  case kMovementType_Random:
    MOV_random(d_time);
    break;
  case kMovementType_Deterministic:
    MOV_deterministic(d_time);
    break;
  case kMovementType_Resting:
    MOV_resting(d_time);
    break;
  }
  if(!target_reached()) move(d_time);
}

void Prisoner::move(float d_time) {
  position()->x = position()->x + velocity()->x * d_time / 1000.0f;
  position()->y = position()->y + velocity()->y * d_time / 1000.0f;
}

void Prisoner::MOV_random(float d_time) {
  if (path()->length() > 0) {
    if (!target_reached()) {
      ESAT::Vec3* point = new ESAT::Vec3;
      *point = path()->actualPoint();
      set_target(point);
    }
    if (app::instance().Distance(*target(), *position()) <= data_->distance_offset_) {
      set_targetReached(true);
      path()->nextPoint();
    }
  }
}

void Prisoner::MOV_deterministic(float d_time) {
  if (path()->length() > 0) {
    if (!target_reached()) {
      ESAT::Vec3* point = new ESAT::Vec3;
      *point = path()->actualPoint();
      set_target(point);
    }
    if (app::instance().Distance(*target(), *position()) <= data_->distance_offset_) {
      if (path()->isLast()) {
        set_targetReached(true);
      }
      path()->nextPoint();
    }
  }
}

void Prisoner::MOV_resting(float d_time) {
  set_target(position());
}

void Prisoner::draw() {
  ESAT::DrawSprite(data_->sprite_, data_->position_->x, data_->position_->y);
}

//###############################################################################
//                                  STATE CHANGES
//###############################################################################

void Prisoner::changeToWork() {
  bResting = false;
  mainMindState_ = kmindState_Work;
  workMindState_ = kmindState_Init;
  box_ = app::instance().map->getFreeBox();
  box_->bOcuped_ = true;
  path()->reset();
}

void Prisoner::changeToRest() {
  path()->reset();
  mainMindState_ = kmindState_Rest;
  workMindState_ = kmindState_Init;
  counter_rest_time_ = rest_time_;
  bResting = true;
}

void Prisoner::changeToEscape() {
  path()->reset();
  mainMindState_ = kmindState_Escape;
  workMindState_ = kmindState_Init;
  escapeMindState_ = kmindState_Init;
  set_speed(default_speed_);
}

void Prisoner::changeToWorkGoToWork() {
  workMindState_ = kmindState_goToWork;
  counter_work_time_ = work_time_;
  path()->reset();
  state_ = kWorking;
}

void Prisoner::changeToWorkLoad() {
  path()->reset();
  workMindState_ = kmindState_Load;
}

void Prisoner::changeToWorkGoToUnload() {
  path()->reset();
  workMindState_ = kmindState_GotoUnload;
}

void Prisoner::changeToWorkUnload() {
  path()->reset();
  workMindState_ = kmindState_Unload;
  counter_time_unloading_ = time_unloading_;
}

void Prisoner::changeToWorkGoToLoad() {
  counter_time_unloading_ = time_unloading_;
  workMindState_ = kmindState_GotoLoad;
  box_->bOcuped_ = false;
  box_ = app::instance().map->getFreeBox();
}

void Prisoner::changeToWorkGoToRest() {
  path()->reset();
  box_->bOcuped_ = false;
  set_speed(default_speed_);
  workMindState_ = kmindState_GoToRest;
}

void Prisoner::changeToEscapeGoB() {
  escapeMindState_ = kmindState_GoB;
  path()->reset();
}

void Prisoner::changeToEscapeDoorToDoor() {
  path()->reset();
  escapeMindState_ = kmindState_DoorToDoor;
}

void Prisoner::changeToEscapeGoBase() {
  path()->reset();
  escapeMindState_ = kmindState_goBase;
}

void Prisoner::changeToGoToRestFB() {
  path()->reset();
  escapeMindState_ = kmindState_Init;
  mainMindState_ = kmindState_GoToRestFB;
}

void Prisoner::changeToEnd() {
  path()->reset();
  mainMindState_ = kmindState_End;
}


//###############################################################################
//                                  SETTERS
//###############################################################################

void Prisoner::set_target(ESAT::Vec3* target) {
  data_->target_->x = target->x;
  data_->target_->y = target->y;
  data_->target_->z = target->z;
}

void Prisoner::set_path(Path* path) {
  data_->path_ = path;
}

void Prisoner::set_position(ESAT::Vec3* position) {
  data_->position_->x = position->x;
  data_->position_->y = position->y;
  data_->position_->z = position->z;
}

void Prisoner::set_targetReached(bool target_reached) {
  data_->target_reached_ = target_reached;
}

void Prisoner::set_velocity(float x, float y, float z) {
  data_->velocity_->x = x;
  data_->velocity_->y = y;
  data_->velocity_->z = z;
}

void Prisoner::set_sprite(const char* filename) {
  data_->sprite_ = ESAT::SpriteFromFile(filename);
}

void Prisoner::set_speed(float speed) {
  data_->speed_ = speed;
}

void Prisoner::set_startWorking(bool state) {
  bStart_working_ = state;
}

//###############################################################################
//                                  GETTERS
//###############################################################################

ESAT::Vec3* Prisoner::target() {
  return data_->target_;
}

Path* Prisoner::path() {
  return data_->path_;
}

ESAT::Vec3* Prisoner::position() {
  return data_->position_;
}

bool Prisoner::target_reached() {
  return data_->target_reached_;
}

ESAT::Vec3* Prisoner::velocity() {
  return data_->velocity_;
}

float Prisoner::speed() {
  return data_->speed_;
}

bool Prisoner::freePrisoner() {
  return bFree_;
}