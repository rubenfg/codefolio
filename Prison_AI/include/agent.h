#ifndef __AGENT_H__
#define __AGENT_H__ 1

#include "platform_types.h"
#include "path.h"
#include <ESAT/math.h>
#include <ESAT/sprite.h>

class Agent{
public:
  /////////////////////////////////////////////////////////////////////////////
  /// @enum	MindState
  ///
  /// @brief	Values that represent mind states.
  /////////////////////////////////////////////////////////////////////////////
  enum MindState {
    kmindState_Init,
    kmindState_End,

    //mind states of prisoner
    kmindState_Work,
    kmindState_Rest,
    kmindState_Escape,
    kmindState_GoToRestFB,

    kmindState_Load,
    kmindState_Unload,
    kmindState_GotoLoad,
    kmindState_GotoUnload,
    kmindState_GoToRest,
    kmindState_goBase,

    // mind states of soldier
    kmindState_GoA,
    kmindState_GoB,
    kmindState_GoOpenA,
    kmindState_DoorToDoor,

    // mind states of guard
    kmindState_Suspicion,
    kmindState_Alert,
    kmindState_closeDoorA,
    kmindState_closeDoorB,
    kmindState_goToWork,
    kmindState_alarm,
    kmindState_goNormalState,
    kmindState_Track,

  };
  /////////////////////////////////////////////////////////////////////////////
  /// @enum	Movtype
  ///
  /// @brief	Values that represent movement types.
  /////////////////////////////////////////////////////////////////////////////
  enum Movtype { 
    kMovementType_Random, 
    kMovementType_Deterministic, 
    kMovementType_Tracker, 
    kMovementType_Pattern,
	  kMovementType_Resting 
  };
  /////////////////////////////////////////////////////////////////////////////
  /// @enum	Coorde
  ///
  /// @brief	Values that represent coordes.
  ////////////////////////////  /////////////////////////////////////////////////
  enum Coorde { kMovementUp, kMovementRight, kMovementDown, kMovementLeft };
  /////////////////////////////////////////////////////////////////////////////
  /// @enum	GateDirection
  ///
  /// @brief	Values that represent the gate direction.
  /////////////////////////////////////////////////////////////////////////////
  enum GateDirection {
    kdirection_A = 0,
    kdirection_B,
  };
  /////////////////////////////////////////////////////////////////////////////
  /// @struct	AgentData
  ///
  /// @brief	Generic data of the agents.
  /////////////////////////////////////////////////////////////////////////////
  struct AgentData {
    /// @brief	Agent id
    int id_;
    /// @brief	Agent path
    Path* path_;
    /// @brief	Agent speed
    float speed_;
    /// @brief	Agent distance offset
    float distance_offset_ = 10.0f;
    /// @brief	Agent target reached check
    bool target_reached_;
    /// @brief	Agent velocity
    ESAT::Vec3* velocity_;
    /// @brief	Agent position
    ESAT::Vec3* position_;
    /// @brief	Agent target
    ESAT::Vec3* target_;
    /// @brief	Agent sprite
    ESAT::SpriteHandle sprite_;
  };
  ///////////////////////////////////////////////////////////////////////
  /// @fn	Agent::Agent();
  ///
  /// @brief	Default constructor.
  ///////////////////////////////////////////////////////////////////////
  Agent();
  ///////////////////////////////////////////////////////////////////////
  /// @fn	Agent::~Agent();
  ///
  /// @brief	Default destructor.
  ///////////////////////////////////////////////////////////////////////
  virtual ~Agent();
  ///////////////////////////////////////////////////////////////////////
  /// @fn	virtual void Agent::init( const char* spritePath, 
  ///                       ESAT::Vec3 position, 
  ///                       float speed);
  ///
  /// @brief	Initialize the variables class.
  ///
  /// @param	spritePath	The path for the sprite.
  /// @param	position	The initial position.
  /// @param	speed	The speed of the agent.
  ///////////////////////////////////////////////////////////////////////
  virtual void init(const char* spritePath, ESAT::Vec3 position, float speed) {};
  ///////////////////////////////////////////////////////////////////////
  /// @fn	virtual void Agent::update(float d_time);
  ///
  /// @brief	Updates the agent.
  ///
  /// @param	deltaTime	Delta time.
  ///////////////////////////////////////////////////////////////////////
  virtual void update(float deltaTime) {};
  ///////////////////////////////////////////////////////////////////////
  /// @fn	virtual void Agent::mind(float d_time);
  ///
  /// @brief	Minds.
  ///
  /// @param	deltaTime	Delta time.
  ///////////////////////////////////////////////////////////////////////
  virtual void mind(float d_time) {};
  ///////////////////////////////////////////////////////////////////////
  /// @fn	virtual void Agent::body(float d_time);
  ///
  /// @brief	Body.
  ///
  /// @param	deltaTime	Delta time.
  ///////////////////////////////////////////////////////////////////////
  virtual void body(float d_time) {};
  ///////////////////////////////////////////////////////////////////////
  /// @fn	virtual void Agent::move(float d_time);
  ///
  /// @brief	Moves the agent.
  ///
  /// @param	deltaTime	Delta time.
  ///////////////////////////////////////////////////////////////////////
  virtual void move(float d_time) {};
  ///////////////////////////////////////////////////////////////////////
  /// @fn	virtual void Agent::draw();
  ///
  /// @brief	Draws the agent.
  ///////////////////////////////////////////////////////////////////////
  virtual void draw() {};

protected:
  
private:


};

#endif
