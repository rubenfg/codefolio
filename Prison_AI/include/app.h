#ifndef __APP_H__
#define __APP_H__ 1

#include "platform_types.h"
#include <grid.h>
#include "agent.h"
#include "prisoner.h"
#include "guard.h"
#include "soldier.h"
#include <ESAT/math.h>
#include <ESAT/sprite.h>

class app{
	public:
		static app& instance(){
      //Only executed once
      static app* inst = new app();
      //Executed on every call
      return *inst;
    }
		///////////////////////////////////////////////////////////////////////
		/// @fn	app::app();
		///
		/// @brief	Default constructor.
		///////////////////////////////////////////////////////////////////////

		app();

		///////////////////////////////////////////////////////////////////////
		/// @fn	app::~app()
		///
		/// @brief	Destructor.
		///////////////////////////////////////////////////////////////////////

		~app(){};

		///////////////////////////////////////////////////////////////////////
		/// @fn	bool app::init();
		///
		/// @brief	Initializes this object.
		///
		/// @return	True if it succeeds, false if it fails.
		///////////////////////////////////////////////////////////////////////

		bool init();

		///////////////////////////////////////////////////////////////////////
		/// @fn	void app::ShutDown();
		///
		/// @brief	Shuts down this object and frees any resources it is using.
		///////////////////////////////////////////////////////////////////////

		void shutDown();

		///////////////////////////////////////////////////////////////////////
		/// @fn	void app::Start();
		///
		/// @brief	Starts this object.
		///////////////////////////////////////////////////////////////////////

		void start();

		///////////////////////////////////////////////////////////////////////
		/// @fn	void app::Update(float time);
		///
		/// @brief	Updates the given time.
		///
		/// @param	time	The time.
		///////////////////////////////////////////////////////////////////////

		void update(float time);

		///////////////////////////////////////////////////////////////////////
		/// @fn	void app::Draw();
		///
		/// @brief	Draws this object.
		///////////////////////////////////////////////////////////////////////

		void draw();

		///////////////////////////////////////////////////////////////////////
		/// @fn	void app::InputService();
		///
		/// @brief	Input service.
		///////////////////////////////////////////////////////////////////////

		void inputService();

    ///////////////////////////////////////////////////////////////////////
    /// @fn int app::absolute(int value);
    ///
    /// @brief	Calculate the absolute value.
    ///
    /// @param	value	value.
    ///
    /// @return	Return the absolute value.
    /////////////////////////////////////////////////////////////////////// 
    int absolute(int value);
    ///////////////////////////////////////////////////////////////////////
    /// @fn ESAT::Vec3 app::Normalize(ESAT::Vec3* vector);
    ///
    /// @brief	Normalize the vector.
    ///
    /// @param	vector Vector to normalize.
    ///
    /// @return	Return the vector normalized.
    /////////////////////////////////////////////////////////////////////// 
    ESAT::Vec3 Normalize(ESAT::Vec3* vector);
    ///////////////////////////////////////////////////////////////////////
    /// @fn float app::Distance(ESAT::Vec3 pos1, ESAT::Vec3 pos2);
    ///
    /// @brief	Return the distance between 2 points.
    ///
    /// @param	pos1 First point.
    /// @param	pos2 Second point.
    ///
    /// @return	The distance.
    /////////////////////////////////////////////////////////////////////// 
    float Distance(ESAT::Vec3 pos1, ESAT::Vec3 pos2);

    /// @brief	True to quit game
		bool quit_game_ = false;

		/// @brief	Number of NPCs
		static const int kMax_npcs = 3;

		/// @brief	my array of NPCs


		/// @brief	Mouse position
		ESAT::Vec3 mouse_position;

		/// @brief	Max width of the screen
		int const kMax_width = 960;

		/// @brief	Max height of the screen
		int const kMax_height = 704;

    /// @brief	Grid
    Grid* map;
    /// @brief	Sprite
    ESAT::SpriteHandle wallpaper;
    /// @brief	Sprite
    ESAT::SpriteHandle alert_wp;
    /// @brief	Sprite
    ESAT::SpriteHandle chunk;
    /// @brief	Sprite
    ESAT::SpriteHandle chunk_mouse;
    /// @brief	Sprite
    ESAT::SpriteHandle chunk_bad;
    /// @brief	Sprite
    ESAT::SpriteHandle chunk_good;
    /// @brief	Prisoners
    Prisoner* prisonerTest[10];
    /// @brief	Guards
    Guard* guardTest[10];
    /// @brief	Soldiers
    Soldier* soldierTest[10];
    /// @brief	Stop the soldiers
    bool bStopSoldiers = false;
    /// @brief	Alarm time
    float time_alarm_ = 10000.0f;
    /// @brief	counter alarm
    float counter_alarm;
    /// @brief	Alarm state
    bool bAlarm = false;

};

#endif
