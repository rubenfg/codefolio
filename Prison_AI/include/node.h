#ifndef __NODE_H__
#define __NODE_H__ 1

#include "platform_types.h"
#include <vector>
#include <ESAT/math.h>
#include <glm/vec2.hpp>

class Node{
	public:
    /// @brief	Value of the node
    char value_;
    /// @brief	Node parent
    Node* parent_ = nullptr;
    /// @brief	Node position
    glm::vec2 position_;
    /// @brief	h
    int h = 0;
    /// @brief	f
    int f = 0;
    /// @brief	g
    int g = 0;
    ///////////////////////////////////////////////////////////////////////
    /// @fn	Node::Node();
    ///
    /// @brief	Default constructor.
    ///////////////////////////////////////////////////////////////////////
    Node();
    ///////////////////////////////////////////////////////////////////////
    /// @fn	Node::~Node();
    ///
    /// @brief	Default destructor.
    ///////////////////////////////////////////////////////////////////////
    ~Node();
		
};

#endif
