#ifndef __GRID_H__
#define __GRID_H__ 1

#include "platform_types.h"
#include "node.h"
#include <vector>
#include <list>
#include <ESAT/math.h>
#include <ESAT/sprite.h>
#include <glm/vec2.hpp>
#include <path.h>

class Grid {
public:
  /////////////////////////////////////////////////////////////////////////////
  /// @struct	Box
  ///
  /// @brief	box used for the prisoner work zone.
  /////////////////////////////////////////////////////////////////////////////
  struct Box {
    /// @brief	Box initial position
    ESAT::Vec3 initial_position_;
    /// @brief	Box final position
    ESAT::Vec3 final_position_;
    /// @brief	Box state
    bool bOcuped_;
  };
  /// @brief	Box sprite
  ESAT::SpriteHandle box_sprite_;
  /// @brief	sprite for the points marked
  ESAT::SpriteHandle point_sprite_;
  /////////////////////////////////////////////////////////////////////////////
  /// @struct	BoxManager
  ///
  /// @brief	Storage all the box of the game.
  /////////////////////////////////////////////////////////////////////////////
  struct BoxManager {
    /// @brief	Vector of boxes
    std::vector<Box*> boxes_;
  };
  /////////////////////////////////////////////////////////////////////////////
  /// @struct	NodePoint
  ///
  /// @brief	Points graph for the guards
  /////////////////////////////////////////////////////////////////////////////
  struct NodePoint {
    /// @brief	NodePoint id
    int id_ = 0;
    /// @brief	NodePoint position
    glm::vec2 position_;
    /// @brief	Vector of other NodePoints to access.
    std::vector<NodePoint*> nears_;
    /// @brief	Vector of path for go to communicated NodePoints
    std::vector<std::vector<glm::vec2>> paths_;
  };
  /////////////////////////////////////////////////////////////////////////////
  /// @struct	Door
  ///
  /// @brief	Doors for the map
  /////////////////////////////////////////////////////////////////////////////
  struct Door {
    /// @brief	Door state
    bool open = false;
    /// @brief	Vector of the points that the door fills
    std::vector<glm::vec2> door_position_;
  };
  /// @brief	Door a 
  Door* door_a_;
  /// @brief	Door b
  Door* door_b_;
  /// @brief	Box manager
  BoxManager* box_manager_;
  /// @brief	Grid cell values
  std::vector<Node> elements_;
  /// @brief	Graph for the guards movement
  std::vector<NodePoint*> prison_points_;
  /// @brief	Sprite valid
  ESAT::SpriteHandle chunk_valid;
  /// @brief	Sprite not valid
  ESAT::SpriteHandle chunk_not_valid;
  /// @brief	Sprite selected
  ESAT::SpriteHandle chunk_selected;
  /// @brief	Sprite min path
  ESAT::SpriteHandle chunk_minpath;
  /// @brief	Vector of point to go to a
  std::vector<glm::vec2> go_to_a_path_;
  /// @brief	Vector of point to go to b (door closed)
  std::vector<glm::vec2> gtb_p_c_;
  /// @brief	Vector of point to go to b (door open)
  std::vector<glm::vec2> gtb_p_o_;
  /// @brief	Vector of point to go to a (door open)
  std::vector<glm::vec2> gta_p_o_;
  /// @brief	Vector of point to the state Door to door (go a)
  std::vector<glm::vec2> dtd_g_a_;
  /// @brief	Vector of point to the state Door to door (go b)
  std::vector<glm::vec2> dtd_g_b_;
  /// @brief	Vector of point to go base b from b
  std::vector<glm::vec2> b_go_baseb_;
  /// @brief	Vector of point to go base a from a
  std::vector<glm::vec2> a_go_basea_;
  /// @brief	Vector of point to go to rest from a
  std::vector<glm::vec2> a_to_rest_;
  /// @brief	Point where door a can be opened
  glm::vec2 door_a_point_ = {152.0f, 232.0f};
  /// @brief	Point where door b can be opened
  glm::vec2 door_b_point_ = { 800.0f, 272.0f };
  ///Debug vector, delete it after
  std::vector<float> position_saved_;
  std::vector<int> pos_grid_saved_;
  /// @brief	grid size x
  int size_x = 960/8;
  /// @brief	grid size y
  int size_y = 704/8;
  /// Debug file to storage information
  FILE *file;
  ///////////////////////////////////////////////////////////////////////
  /// @fn	Grid::Grid();
  ///
  /// @brief	Default constructor.
  ///////////////////////////////////////////////////////////////////////
  Grid();
  ///////////////////////////////////////////////////////////////////////
  /// @fn	Grid::~Grid();
  ///
  /// @brief	Default destructor.
  ///////////////////////////////////////////////////////////////////////
  ~Grid();
  ///////////////////////////////////////////////////////////////////////
  /// @fn	Grid::init();
  ///
  /// @brief	Initialize the grid.
  ///////////////////////////////////////////////////////////////////////
  void init();
  ///////////////////////////////////////////////////////////////////////
  /// @fn void Grid::generateNodePointsPaths();
  ///
  /// @brief	Creates the graph paths with the A* algorithm.
  ///////////////////////////////////////////////////////////////////////
  void generateNodePointsPaths();
  ///////////////////////////////////////////////////////////////////////
  /// @fn void Grid::generateGeneralPaths();
  ///
  /// @brief	Creates the pahts used in the game with the algorithm A*.
  ///////////////////////////////////////////////////////////////////////
  void generateGeneralPaths();
  ///////////////////////////////////////////////////////////////////////
  /// @fn void Grid::drawNodePointsPaths();
  ///
  /// @brief	Draw the graph points.
  ///////////////////////////////////////////////////////////////////////
  void drawNodePointsPaths();
  ///////////////////////////////////////////////////////////////////////
  /// @fn void Grid::openDoor(Door* door);
  ///
  /// @brief	Open the indicated door.
  ///
  /// @param	door	Door to open.
  ///////////////////////////////////////////////////////////////////////
  void openDoor(Door* door);
  ///////////////////////////////////////////////////////////////////////
  /// @fn void Grid::closeDoor(Door* door);
  ///
  /// @brief	Close the indicated door.
  ///
  /// @param	door	Door to close.
  ///////////////////////////////////////////////////////////////////////
  void closeDoor(Door* door);
  ///////////////////////////////////////////////////////////////////////
  /// @fn bool Grid::doorsOpen();
  ///
  /// @brief	Check if a door is opened.
  ///
  /// @return	True if it succeeds, false if it fails.
  ///////////////////////////////////////////////////////////////////////
  bool doorsOpen();
  ///////////////////////////////////////////////////////////////////////
  /// @fn	void Grid::drawPrisonPointRute(int id);
  ///
  /// @brief	Draw the paths of a NodePoint.
  ///
  /// @param	id	NodePoint id to draw.
  ///////////////////////////////////////////////////////////////////////
  void drawPrisonPointRute(int id);
  ///////////////////////////////////////////////////////////////////////
  /// @fn Box* Grid::getFreeBox();
  ///
  /// @brief	Find a free box.
  ///
  /// @return	Return the pointer to the first free box.
  ///////////////////////////////////////////////////////////////////////
  Box* getFreeBox();
  ///////////////////////////////////////////////////////////////////////
  /// @fn bool Grid::CheckFreeBox();
  ///
  /// @brief	Checks if there is any free box.
  ///
  /// @return	True if it succeeds, false if it fails.
  ///////////////////////////////////////////////////////////////////////
  bool CheckFreeBox();
  ///////////////////////////////////////////////////////////////////////
  /// @fn void Grid::drawBoxes();
  ///
  /// @brief	Draw the boxes.
  ///////////////////////////////////////////////////////////////////////
  void drawBoxes();
  ///////////////////////////////////////////////////////////////////////
  /// @fn void Grid::drawPrisonPoints();
  ///
  /// @brief	Draw the graph points.
  ///////////////////////////////////////////////////////////////////////
  void drawPrisonPoints();
  ///////////////////////////////////////////////////////////////////////
  /// @fn NodePoint* Grid::GetNodePoint(int id);
  ///
  /// @brief	Find the NodePoint and return it.
  ///
  /// @param	id	NodePoint id to return.
  ///
  /// @return	Return the NodePoint pointer.
  ///////////////////////////////////////////////////////////////////////  
  Grid::NodePoint* GetNodePoint(int id);
  ///////////////////////////////////////////////////////////////////////
  /// @fn bool Grid::fillGridByFile(const char* filename);
  ///
  /// @brief	Fills the values of the grid with the file indicated.
  ///
  /// @param	filename	File to read.
  ///
  /// @return	True if it succeeds, false if it fails.
  ///////////////////////////////////////////////////////////////////////  
  bool fillGridByFile(const char* filename);
  ///////////////////////////////////////////////////////////////////////
  /// @fn bool Grid::fillGridByImage(const char* filename);
  ///
  /// @brief	Fills the values of the grid with the image indicated.
  ///
  /// @param	filename	Image to read.
  ///
  /// @return	True if it succeeds, false if it fails.
  ///////////////////////////////////////////////////////////////////////  
  bool fillGridByImage(const char* filename);
  ///////////////////////////////////////////////////////////////////////
  /// @fn void Grid::convertGridValue(int index, 
  ///                                 const char def_value, 
  ///                                 const char new_value);
  ///
  /// @brief	Convert the specific value of the grid to the value given.
  ///
  /// @param	index	Grid element index.
  /// @param	def_value	Original value to change.
  /// @param	new_value	New value to change.
  ///////////////////////////////////////////////////////////////////////  
  void convertGridValue(int index, const char def_value, const char new_value);
  ///////////////////////////////////////////////////////////////////////
  /// @fn void Grid::convertGridValues(const char def_value, 
  ///                                  const char new_value);
  ///
  /// @brief	Convert the values of the grid to the value given.
  ///
  /// @param	def_value	Original value to change.
  /// @param	new_value	New value to change.
  ///////////////////////////////////////////////////////////////////////  
  void convertGridValues(const char def_value, const char new_value);
  ///////////////////////////////////////////////////////////////////////
  /// @fn char Grid::getElement(int index);
  ///
  /// @brief	Return the value of the cell given.
  ///
  /// @param	index	Index element to get the value.
  ///
  /// @return	The value of the element indicated.
  ///////////////////////////////////////////////////////////////////////  
  char getElement(int index);
  ///////////////////////////////////////////////////////////////////////
  /// @fn int Grid::calculateSpecificHeuristic(Node* origin, Node* destiny);
  ///
  /// @brief	Return the value of the calculated heuristic.
  ///
  /// @param	origin	Node start.
  /// @param	destiny	Node goal.
  ///
  /// @return	The value of the calculated heuristic.
  ///////////////////////////////////////////////////////////////////////  
  int calculateSpecificHeuristic(Node* origin, Node* destiny);
  ///////////////////////////////////////////////////////////////////////
  /// @fn bool Grid::valueInGrid(float valueX, float valueY);
  ///
  /// @brief	Check if the position has a valid value in the grid.
  ///
  /// @param	valueX x.
  /// @param	valueY y.
  ///
  /// @return	True if it succeeds, false if it fails.
  ///////////////////////////////////////////////////////////////////////  
  bool valueInGrid(float valueX, float valueY);
  ///////////////////////////////////////////////////////////////////////
  /// @fn void Grid::print();
  ///
  /// @brief	Print the values of the grid in a file.
  ///////////////////////////////////////////////////////////////////////  
  void print();
  ///////////////////////////////////////////////////////////////////////
  /// @fn void Grid::draw();
  ///
  /// @brief	Draw the grid.
  ///////////////////////////////////////////////////////////////////////  
  void draw();
  /// Used for DEBUG
  void print_selected_cells();
  /// Used for DEBUG
  void save_position(float x, float y);
  /// Used for DEBUG
  void write_positions();
  ///////////////////////////////////////////////////////////////////////
  /// @fn glm::vec2 grid_position(float x, float y);
  ///
  /// @brief	Returns the position of the cell.
  ///
  /// @param	x.
  /// @param	y.
  ///
  /// @return	The position of the cell.
  /////////////////////////////////////////////////////////////////////// 
  glm::vec2 grid_position(float x, float y);
};

#endif