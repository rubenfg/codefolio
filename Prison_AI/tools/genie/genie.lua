solution "3ia_delafuent"
	configurations {
			"Release",
			"Debug"
		}
    defines {
		"WIN32",
		"_CONSOLE",
	}
    platforms {"x32", "x64"}
    location "../../vs"

	project "3ia_delafuent"
		language "C++"
		kind "ConsoleApp"
    links{"opengl32", "ole32", "winmm", "kernel32", "user32", "gdi32", "winspool", "comdlg32", "advapi32", "shell32", "oleaut32","uuid", "odbc32", "odbccp32",
	"ESAT_d", "ESAT_extra_d", "ESAT", "ESAT_Extra"}

    includedirs{
	"../../extern/ESAT_rev183",
	"../../extern/ESAT_rev183/include",
      "../../include",
			"../../extern/stb",
			"../../extern/GLM/glm/detail",
			"../../extern/GLM/glm/ext",
			"../../extern/GLM/glm/gtc",
			"../../extern/GLM/glm/gtx",
			"../../extern/GLM/glm/simd",
			"../../extern/GLM/glm",
			"../../extern/GLM",
	}

	libdirs{
		"../../extern/ESAT_rev183/bin"
	}

    files{
	  "../../img/**.png",
	  "../../extern/ESAT_rev183/include/ESAT/**.h",
      "../../src/**.cc",
      "../../include/**.h",
			"../../extern/stb/**.h",
			"../../extern/GLM/glm/**.hpp",

			"../../extern/GLM/glm/detail/**.hpp",
			"../../extern/GLM/glm/detail/**.cpp",
			"../../extern/GLM/glm/detail/**.inl",

			"../../extern/GLM/glm/ext/**.hpp",
			"../../extern/GLM/glm/ext/**.cpp",
			"../../extern/GLM/glm/ext/**.inl",

			"../../extern/GLM/glm/gtc/**.hpp",
			"../../extern/GLM/glm/gtc/**.cpp",
			"../../extern/GLM/glm/gtc/**.inl",

			"../../extern/GLM/glm/gtx/**.hpp",
			"../../extern/GLM/glm/gtx/**.cpp",
			"../../extern/GLM/glm/gtx/**.inl",

			"../../extern/GLM/glm/simd/**.h",
			"../../extern/GLM/glm/simd/**.cpp",
			"../../extern/GLM/glm/simd/**.inl",
    }
	configuration "Debug"
		flags { "Symbols" }

	configuration "vs2017"
	windowstargetplatformversion "10.0.15063.0"
